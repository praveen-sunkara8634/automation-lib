__author__ = 'Sarvesh.Singh'

import json
from locust import HttpLocust, TaskSequence, seq_task, between
from decurtis import common
from decurtis.common import uncurl_from_curl


class RunCurl(TaskSequence):
    """
    Locust User Behavior File
    """

    def __init__(self, parent):
        """
        Init Function
        :param parent:
        """
        super().__init__(parent)
        self.url = self.parent.host

    @seq_task(1)
    def startcurl(self):
        """
        Run the curl command using sequence
        :return:
        """
        _url = self.url
        _data = json.dumps(self.parent.data)
        headers = []
        headers = self.parent.headers
        method = self.parent.method
        params = self.parent.params
        if params is not None:
            _url = f"{_url}?{params}"
        if method == "GET":
            response = self.client.get(_url, name='TestCurl', data=_data,
                                       headers=headers).json()
        elif method == "POST":
            response = self.client.post(_url, name='TestCurl', data=_data,
                                        headers=headers).json()
        else:
            response = self.client.put(_url, name='TestCurl', data=_data,
                                       headers=headers).json()


class RunCurlLocust(HttpLocust):
    task_set = RunCurl
    f = open("curl_command.txt", "r")
    curl_command = f.read()
    uncurl_command = uncurl_from_curl(curl_command)
    host = uncurl_command['url']
    method = uncurl_command['method']
    data = []
    data = uncurl_command['data']
    params = []
    params = uncurl_command['params']
    headers = []
    headers = uncurl_command['headers']
    params = uncurl_command['params']
    wait_time = between(5, 15)


if __name__ == "__main__":
    RunCurlLocust().run()
