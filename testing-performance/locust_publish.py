__author__ = 'Sarvesh.Singh'

message = """
<style>
table, th, td {
  border: 1px solid black;
}
th, tr {
  text-align: center;
}
</style>
<table>
<th>requests</th>
<th>failures</th>
<th>Median response time</th>
<th>Average response time</th>
<th>Min response time</th>
<th>Max response time</th>
<th>Average Content Size</th>
<th>Requests/s</th>
<th>Requests Failed/s</th>
"""
infile = open("example_stats.csv", "r")
for line in infile:
    row = line.split(",")
    requests = row[2]
    failures = row[3]
    Median_response = row[4]
    Average_response = row[5]
    Min_response = row[6]
    Max_response = row[7]
    Average_Content = row[8]
    Requests = row[9]
    Requests_Failed = row[10]
    messages = f"""
    <tr>
    <td>{requests}</td>
    <td>{failures}</td>
    <td>{Median_response}</td>
    <td>{Average_response}</td>
    <td>{Min_response}</td>
    <td>{Max_response}</td>
    <td>{Average_Content}</td>
    <td>{Requests}</td>
    <td>{Requests_Failed}</td>
    </tr>
    </table>
    """
message_towrite = message + messages
# end the table
f = open("locust_report.html", "a")
f.write(message_towrite)
f.close()
