#!groovy
@Library("JenkinsLib") _

// Global Variables
gitRepo = 'git@bitbucket.org:decurtis/vv-e2e-automation.git'
dockerImage = "gcr.io/eng-genius-224716/python:3.8.1"

countries = ['US', 'UK', 'CA', 'IN']
guestCount = [1, 2, 3, 4]
vipLevel = ['VIP1', 'VIP2', 'VIP3', 'VIP4','VIP5','VIP6']
celebration = ['F', 'BP', 'E', 'EG','A','GR','H','OT','RT','RU']
notifyEmails = lib.getBuildUser()['email']
def embarkDate = null
threads = 1

properties([
    buildDiscarder(logRotator(artifactDaysToKeepStr: '1', artifactNumToKeepStr: '50', daysToKeepStr: '7', numToKeepStr: '50')),
    parameters([
        choice(choices: countries.unique().join('\n'), description: 'Guest Country', name: 'testCountry'),
        string(defaultValue: 'https://application-integration.ship.virginvoyages.com/svc/', description: "Ship Link", name: 'shipLink'),
        string(defaultValue: 'https://integration.virginvoyages.com/svc/', description: "Shore Link", name: 'shoreLink'),
        string(defaultValue: '2020-01-01', description: "Embark Date (YYYY-MM-DD)", name: 'embarkdate'),
        choice(choices: guestCount.unique().sort().join('\n'), description: 'Guest Count', name: 'guests'),
        booleanParam(name: 'extra', defaultValue: false, description: 'Add extra to reservation'),
        choice(choices: vipLevel.unique().sort().join('\n'), description: 'VIP Level', name: 'viplevel'),
        choice(choices: celebration.unique().sort().join('\n'), description: 'celebration', name: 'celebration'),
        string(defaultValue: '1', description: "Reservations to Make", name: 'reservations'),
    ])
])

testCountry = (params.testCountry == null) ? 'US' : params.testCountry.toString().trim().replaceAll('"', '')
shipLink = (params.shipLink == null) ? 'https://application-integration.ship.virginvoyages.com/svc/' : params.shipLink.toString().trim().replaceAll('"', '')
shoreLink = (params.shoreLink == null) ? 'https://integration.virginvoyages.com/svc/' : params.shoreLink.toString().trim().replaceAll('"', '')
guests = (params.guests == null) ? 2 : params.guests.toInteger()
extra = (params.extra == null) ? false : params.extra.toBoolean()
viplevel = (params.viplevel == null) ? 'VIP1' : params.viplevel.toString()
celebration = (params.celebration == null) ? 'F' : params.celebration.toString()
reservations = (params.reservations == null) ? 1 : params.reservations.toInteger()
embarkdate = (params.embarkdate == null) ? '2020-01-01' : params.embarkdate.toString().trim().replaceAll('"', '')

log.info([
    "reservations = ${reservations}",
    "guestsPerReservation = ${guests}",
    "totalGuests = ${reservations * guests}",
    "notifyEmails = ${notifyEmails}",
].unique().sort().join('\n'))

log.info("Total '${reservations}' Reservations")
testBed = lib.getTestBed(shoreLink)

cmdLineParams = ""
markers = "RESERVATION"

cmdLineParams += " -m \"${markers} and not INFRA_CHECK\" --guests ${guests} --embark-date ${embarkdate}"

if(extra) {
    cmdLineParams += " --add_extra --vip_level ${viplevel} --celebration_level ${celebration}"
}

reservationsMade = []

def branches = [:]
for (int _thread = 0; _thread < threads; _thread++) {
    int index = _thread, branch = _thread + 1
    stage ("Create-Reservation") {
        branches["Thread-${branch}"] = {
            toSleep = branch * 5
            sleep(toSleep)
            try {
                log.info("python3 -u -m pytest --color=yes --ship=${shipLink} --shore=${shoreLink} ${cmdLineParams} --countries ${testCountry} test_scripts")
                sh "python3 -u -m pytest --color=yes --ship=${shipLink} --shore=${shoreLink} ${cmdLineParams} --countries ${testCountry} test_scripts"
                log.success("********** ITER: ${iter} Passed !! **********")
            } catch (exc) {
                log.error("********** ITER: ${iter} Failed !! **********")
            } finally {
                def userName = null
                def userPass = "Yellow*99"
                def resNum = null
                if (fileExists('test_data.json')) {
                    def testData = readJSON file: 'test_data.json', pretty: 2
                    embarkDate = testData['sailingEmbarkDate']
                    currentBuild.displayName = "${env.BUILD_NUMBER} - ${embarkDate}"
                    userName = testData['username']
                    resNum = testData['reservationNumber']
                }
                if (fileExists('guest_data.json')) {
                    sh "mv guest_data.json guest_data${iter}.json"
                }
                reservationsMade.add("\"${resNum} ${userName} ${userPass} ${embarkDate}\"\n")
                log.info("Passed | ITER: ${iter} | reservationNumber: ${resNum}")
            }
        }
    }
}

node('master') {
    workspace = pwd()
    stage('Git-Clone') {
        checkout([
            $class: 'GitSCM',
            doGenerateSubmoduleConfigurations: false,
            branches: [[name: 'master']],
            extensions: [[$class: 'CleanBeforeCheckout']],
            userRemoteConfigs: [[credentialsId: 'bitbucket-root', url: gitRepo]]]
        )
    }
    log.info("Running E2E on ${testBed.toUpperCase()}")
    currentBuild.displayName = "${env.BUILD_NUMBER} - ${embarkDate}"
    currentBuild.result = "SUCCESS"
    withDockerContainer(args: '-v $workspace/:/root/:z --network=host', image: dockerImage) {
        withCredentials([string(credentialsId: 'decurtis-e2e-password', variable: 'E2E_PASSWORD')]) {
            for (iter = 1; iter <= reservations; iter++) {
                currentBuild.description = "${testBed.toUpperCase()} | ${iter} of ${reservations} | ${lib.getUserToDisplay()}"
                wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'xterm']) {
                    try {
                        timeout(time: 5, unit: 'MINUTES') {
                            parallel branches
                        }
                    } catch (exc) {
                        unstable(exc.toString())
                        log.info("Parallel Execution Failed !! Continuing Next ...")
                        currentBuild.result = "FAILURE"
                    }
                }
            }
        }
    }
    // Send Final Notification !!
    toSend = "Reservations: ${reservationsMade}"
    subject = "${testBed.toUpperCase()} Reservations"
    message = reservationsMade.unique().sort().join('\n')
    lib.sendMail([notifyEmails], subject, message)
    log.info(toSend)
    slackMessage = [
        "${env.JOB_BASE_NAME} #${env.BUILD_NUMBER} ${testBed.toUpperCase()} Bulk Reservations Done !!",
        "Embark: ${embarkDate} | Iter: ${reservations} | Guests: ${guests}",
        "E-Mail Sent to ${notifyEmails}",
    ].join('\n')
    sendSlackNotification(slackMessage)
}

def sendSlackNotification(String text) {
    Date now = new Date();
    time_now = ((now.getTime())/1000).toInteger()
    message = """@here Job# <${env.BUILD_URL}console|${env.BUILD_DISPLAY_NAME}> on *${testBed.toUpperCase()}*
Updated @ <!date^${time_now}^ {date_short} {time_secs}|MY-Date>
```${text}```"""
    slackSend channel: 'bulk-reservation-data', color: 'danger', message: message, teamDomain: 'decurtis', tokenCredentialId: 'slack-token', iconEmoji: 'deadpool'
}