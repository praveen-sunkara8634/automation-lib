import json
from decurtis.common import urljoin
from decurtis.bitbucket import BitBucketApi

name = 'Pull-Request-Env'

with open("jenkins-data.json", 'r') as _fp:
    _original_data = json.loads(_fp.read())

all_failed = list(filter(lambda x: x['result'] == 'FAILURE', _original_data))
all_passed = list(filter(lambda x: x['result'] == 'SUCCESS', _original_data))
all_running = list(filter(lambda x: x['status'] == 'IN_PROGRESS', _original_data))
all_descriptions = sorted(list({x['description'] for x in _original_data}))

to_rerun = []
to_report = []
for _description in {x['description'] for x in all_failed}:
    for _failed in all_failed:
        if _failed['description'] == _description:
            to_rerun.append(_failed)
            break

if name == "DXP-CI":
    index_to_remove = []
    for _count, _rerun in enumerate(to_rerun):
        for _passed in all_passed:
            if _passed['description'] == _rerun['description']:
                if int(_passed['number']) > int(_rerun['number']):
                    index_to_remove.append(_count)

    to_rerun = [y for x, y in enumerate(to_rerun) if x not in index_to_remove]

if name == "Pull-Request-Env":
    index_to_remove = []
    for _description in {x['description'] for x in all_failed}:
        count = [x for x in all_failed if x['description'] == _description]
        if len(count) > 3:
            for _count, _failed in enumerate(to_rerun):
                if _failed['description'] == _description:
                    to_report.append(_failed)

    # Filter based on Bit-Bucket Status (only PR's)
    to_rerun_bit_bucket_based = []
    for _rerun in to_rerun:
        repo = _rerun['params']['Repository']
        number = int(_rerun['params']['PR'])
        data = BitBucketApi().get_pull_requests_statuses(client='decurtis', repo=repo, number=number)
        for value in data['values']:
            if str(value['key']).strip() == 'Test' and str(value['state']).strip() != 'SUCCESSFUL':
                to_rerun_bit_bucket_based.append(_rerun)

    to_rerun = to_rerun_bit_bucket_based

    # Removed currently running ones!!
    to_rerun = list(filter(lambda x: x['description'] not in [x['description'] for x in all_running], to_rerun))
    to_report = list(filter(lambda x: x['description'] not in [x['description'] for x in all_running], to_report))

print()
