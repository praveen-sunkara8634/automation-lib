from PIL import Image, ImageDraw, ImageFont
from decurtis.common import *


class GeneratePhoto:
    """
    Class to Generate MRZ Images
    """

    def __init__(self, **kwargs):
        """
        Generate Photo with following data
            country_code: str,
            last_name: str,
            first_names: str,
            passport_number: str,
            birth_date: str,
            expiry_date: str,
        :param kwargs:
        """
        self.image_data = kwargs
        self.path = os.path.dirname(os.path.realpath(__file__))
        self.generated_images = os.path.abspath('generated_images')
        if not os.path.isdir(self.generated_images):
            os.makedirs(self.generated_images)

    def generate_photo_file_name(self, extension='.jpeg'):
        """
        Function to generate Random File Name
        :param extension:
        :return:
        """
        name = os.path.join(self.generated_images, f'{generate_random_alpha_numeric_string(length=15)}{extension}')
        return name

    def select_random_image(self):
        """
        Function to find .JPEG files in a folder
        :return:
        """
        _name = os.path.join(self.path, 'ref_images/photo_images')
        files_found = []
        for dir_path, dir_names, file_names in os.walk(_name):
            for file_name in file_names:
                if str(file_name).endswith('.jpeg'):
                    files_found.append(os.path.join(dir_path, file_name))

        return random.choice(files_found)

    def add_text(self):
        """
        Function that will append text to an image
        :return:
        """
        image = Image.open(self.select_random_image())
        width, height = image.size
        font_size = int(height / 25)
        draw = ImageDraw.Draw(image)

        font_file_path = os.path.join(self.path, 'fonts/calibri.ttf')
        font = ImageFont.FreeTypeFont(font_file_path, size=font_size)
        fill = (0, 0, 0)

        # starting position of the message
        line_01 = f"{self.image_data['first_name']} {self.image_data['last_name']}"
        line_02 = self.image_data['birth_date']
        height = height - (font_size * 2)
        draw.text((0, (height + (font_size * 0))), line_01, fill=fill, font=font)
        draw.text((0, (height + (font_size * 1))), line_02, fill=fill, font=font)

        # save the edited image
        _file = self.generate_photo_file_name()
        image.save(_file, compress_level=1)
        # JPEG is s slow format, so we wait for file to be generated and written on disk
        while True:
            if os.path.isfile(_file):
                break
            else:
                time.sleep(1)
                continue
        return _file


if __name__ == "__main__":
    first_name = str(generate_first_name())
    last_name = str(generate_last_name())
    email_id = generate_email_id(from_saved=True, first_name=first_name, last_name=last_name)
    birth_date = str(generate_birth_date().strftime('%y%m%d'))
    final_file = GeneratePhoto(
        first_name=first_name,
        last_name=last_name,
        email_id=email_id,
        birth_date=birth_date
    ).add_text()
