__author__ = 'anshuman.goyal'

import logging
import logging.handlers
import os

loggers = {}


class Logger:
    """
    Class to initialise the Logging
    # "%(asctime)s %(name)3s %(levelname)-8s %(filename)13s: %(lineno)4d :: %(message)s")
    """

    def __init__(self, name='Automation'):
        """
        Init Function for Logging
        :param name: Log-name you want to see in Logger
        """
        self.name = str(name).upper()
        self.mappings = {
            'DEBUG': logging.DEBUG,  # 10
            'INFO': logging.INFO,  # 20
            'WARNING': logging.WARN,  # 30
            'WARN': logging.WARNING,  # 30
            'ERROR': logging.ERROR,  # 40
            'CRITICAL': logging.CRITICAL  # 50
        }

        # Set Log Formatter
        self.formatter = logging.Formatter("{levelname} {name} -> {message}", style='{')

        # Set Logging Level: First from environment else
        self.log_level = self.mappings[os.getenv('LOG_LEVEL', 'INFO')]

        global loggers
        if loggers.get(self.name):
            self.logger = loggers[self.name]
        else:
            # logging.basicConfig(level=self.log_level, format="%(levelname)s %(name)s -> %(message)s")
            self.logger = logging.getLogger(self.name)
            self.logger.setLevel(self.log_level)

            # Normal Handler -> Set Level Based on Environment Variable
            # _handler = logging.Handler()
            # _handler.setFormatter(self.formatter)
            # _handler.setLevel(self.log_level)
            # self.logger.addHandler(_handler)

            # Console Handler -> Don't Display DEBUG Logs on Console, Only INFO, ERROR AND WARNING
            console = logging.StreamHandler()
            console.setFormatter(logging.Formatter("'{message}'", style='{'))
            console.setLevel(logging.INFO)
            self.logger.addHandler(console)

            loggers[self.name] = self.logger

    def get_logger(self):
        """
        Function that will return logger object
        :return:
        """
        return self.logger


def get_logger(name='DXP'):
    """
    This function will call the DXP's return_log_object function and return back to the user
    :param name: Name of the logger
    :param name:
    :return:
    """
    return Logger(name=name).get_logger()
