__author__ = 'anshuman.goyal'

from datetime import datetime, timedelta
from types import SimpleNamespace as Namespace

from decurtis.common import *
from elasticsearch import Elasticsearch
from pytz import timezone


class KibanaLogs:
    """
    Class to fetch Kibana Logs and Kibana URL's
    """

    def __init__(self, platform, env, correlation):
        """
        Init Function
        :param platform: DXP or VIRGIN (as of now)
        :param env: QA1, DC, Developer for DXP and qa, Integration for Virgin Voyages
        :param correlation: E2E Correlation ID
        """
        self.platform = platform
        self.correlation = correlation

        # All Developer environments have a common Kibana Link, setting that
        if env in ['qa1', 'dc', 'dev']:
            self.env = env
        else:
            self.env = 'developer'

        # Read Kibana Links from Json
        _file = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'utils/kibana_links.json')
        with open(_file, 'r') as _fp:
            self.links = json.load(_fp, object_hook=lambda d: Namespace(**d))

        # Read Query data from Json
        _file = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'utils/es_query_data.json')
        with open(_file, 'r') as _fp:
            self.query = json.load(_fp)

        if platform == 'DXP' or platform == "DCL":
            self.links = getattr(self.links.DXP, self.env)

        self.base_url = self.links.base_url
        self.kibana_url = str(self.links.base_url).replace("/elasticsearch/", "")
        self.index_shore = self.links.index_shore
        self.index_ship = self.links.index_ship
        self.query['query']['bool']['filter'][0]['multi_match']['query'] = self.correlation

        self.ship_url = self.get_short_kibana_url(self.index_ship)
        self.shore_url = self.get_short_kibana_url(self.index_shore)

        self.ship_logs = self.get_kibana_logs(self.index_ship)
        self.shore_logs = self.get_kibana_logs(self.index_shore)

    def get_short_kibana_url(self, index):
        """
        Get Kibana Short URL, which will open short URL for Kibana
        :return:
        """
        url = urljoin(self.kibana_url, 'kibana/api/saved_objects/_find?type=index-pattern&fields=title&per_page=10000')
        headers = {'content-type': 'application/json'}
        response = send_get_request(url=url, headers=headers).content
        for _saved in response['saved_objects']:
            if index in _saved['attributes']['title']:
                index_id = _saved['id']
                break
        else:
            raise Exception(f"No Index Found for {index}")

        _now = datetime.strftime(datetime.now(timezone('GMT')), "%Y-%m-%dT%H:00:00.000Z")
        _later = datetime.strftime(datetime.now(timezone('GMT')) + timedelta(hours=1), "%Y-%m-%dT%H:00:00.000Z")
        _query = f"/app/kibana#/discover?_g=(refreshInterval:(pause:!t,value:0),time:(from:'{_now}'," \
                 f"to:'{_later}'))&_a=(columns:!(severity,componentname,messagedetail,'@timestamp')," \
                 f"filters:!(),index:'{index_id}',interval:auto,query:(language:kuery,query:'\"{self.correlation}\"')" \
                 f",sort:!('@timestamp',asc))"
        url = urljoin(self.kibana_url, "kibana/api/shorten_url")
        body = {"url": _query}
        headers = {'kbn-xsrf': 'reporting'}
        _content = send_post_request(url=url, data=body, headers=headers).content
        url_id = _content['urlId']
        return urljoin(self.kibana_url, f"kibana/goto/{url_id}")

    def get_kibana_logs(self, index):
        """
        Get Kibana Logs for given index
        :return:
        """
        es = Elasticsearch(self.base_url, use_ssl=True, verify_certs=True, ssl_show_warn=False, timeout=180)

        # Get Shore Side Logs from Kibana (Elastic Search)
        logs = None
        retries = 3
        while retries > 0:
            logs = es.search(index=index, body=self.query)
            if len(logs['hits']['hits']) > 0 and len(logs['hits']['hits']) > 0:
                break
            else:
                logger.debug("Wait for 10 more seconds for Kibana Logs ...")
                retries -= 1
                time.sleep(10)

        # Sort the logs with time stamps if there are hits, else set this to none
        if len(logs['hits']['hits']) > 0:
            logs = sorted([x for x in logs['hits']['hits']], key=lambda i: i['_source']['info']['timestamp'])

        return logs


if __name__ == "__main__":
    kb = KibanaLogs("DXP", 'anshumangoyal', 'e2e-7688906d-780d-4f70-867a-f3efe488dd4a')
    print(kb.get_kibana_logs())
