__author__ = 'anshuman.goyal'
__maintainer__ = 'vishesh.teng'

import json
import os
import allure
from decurtis.logger import get_logger
from decurtis.database import Database
from decurtis.common import read_creds_file

logger = get_logger(name="GRAFANA")


class GrafanaResult():
    """
    module to add data points to Postgres db for grafana graphs
    """

    def __init__(self):
        """
        Init Function for Grafana Logging
        """
        creds = read_creds_file()
        data = creds.qa_postgres
        try:
            self.db = Database(
                host=data.host,
                username=data.username,
                password=data.password,
                database='automation'
            )
        except Exception as exp:
            logger.debug(
                f"psycopg2.connect(user={data.username}, password={data.password}, host={data.host}, port=5432, "
                f"database='automation')")
            logger.error(f"Failed to connect to db {exp}")

    def add_data_points(self, **kwargs):
        """
        Add Grafana Data Points
        """
        try:
            logger.debug(f"adding grafana data points")
            self.db.insert_comp_status_data(**kwargs)
        except Exception as exp:
            logger.error(f"Grafana Postgres Query Exception {exp}")

        try:
            logger.debug(f"saving query in allure report")
            allure.attach(json.dumps(kwargs, indent=2, sort_keys=True), name="grafana_pg_query.json",
                          attachment_type=allure.attachment_type.JSON)
        except Exception as exp:
            logger.error(f"save query to allure Failed !! {exp}")
