from collections import namedtuple


class Locator(object):
    """
    Define one or more locators for an html element.
    """
    # locator types - these values need to match the constructor parameter names
    CLASS_ATTRIBUTE = "class_attribute"
    CSS_SELECTOR = "css_selector"
    LINK_TEXT = "link_text"
    NAME_ATTRIBUTE = "name_attribute"
    PARTIAL_LINK_TEXT = "partial_link_text"
    ID_ATTRIBUTE = "id_attribute"
    TAG_NAME = "tag_name"
    XPATH = "xpath"
    DATA_RL = "data_rl"

    LOC = namedtuple("LOC", "name value")
    # valid locator types; also used for precedence
    LOCATOR_TYPES = [ID_ATTRIBUTE, NAME_ATTRIBUTE, XPATH, CLASS_ATTRIBUTE, LINK_TEXT, PARTIAL_LINK_TEXT, CSS_SELECTOR,
                     TAG_NAME, DATA_RL]

    def __init__(self,
                 class_attribute=None,
                 css_selector=None,
                 link_text=None,
                 name_attribute=None,
                 partial_link_text=None,
                 id_attribute=None,
                 tag_name=None,
                 xpath=None,
                 data_rl=None,
                 ):
        self.class_attribute = class_attribute
        self.css_selector = css_selector
        self.link_text = link_text
        self.name_attribute = name_attribute
        self.partial_link_text = partial_link_text
        self.id_attribute = id_attribute
        self.tag_name = tag_name
        self.xpath = xpath
        self.data_rl = data_rl

    def __str__(self):
        locators = []
        if self.class_attribute:
            locators.append(f"class_attribute={self.class_attribute}")
        if self.css_selector:
            locators.append(f"css_selector={self.css_selector}")
        if self.link_text:
            locators.append(f"link_text={self.link_text}")
        if self.name_attribute:
            locators.append(f"name_attribute={self.name_attribute}")
        if self.partial_link_text:
            locators.append(f"partial_link_text={self.partial_link_text}")
        if self.id_attribute:
            locators.append(f"id_attribute={self.id_attribute}")
        if self.tag_name:
            locators.append(f"tag_name={self.tag_name}")
        if self.xpath:
            locators.append(f"xpath={self.xpath}")
        if self.data_rl:
            locators.append(f'xpath=//*[@data-rl="{self.data_rl}"]')
        return ", ".join(locators)

    def get_locators(self):
        locators = []
        for locator_id in Locator.LOCATOR_TYPES:
            value = eval(f"self.{locator_id}")
            if value is not None:
                locators.append(Locator.LOC(name=locator_id, value=value))
        return locators
