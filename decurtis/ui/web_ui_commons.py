"""
define parameterised methods that can be directly used within page classes
"""
from decurtis.ui.webdriver_factory import WebDriverFactory
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
import time
import pydash

class WebUICommons():

    def launch_browser(self, browser, grid):
        """
        launch browser either on local system or on a selenium grid
        :param browser: chrome, firefox
        :param grid:
        :return:
        """
        global driver
        driver = WebDriverFactory().get_driver(browser, grid)
        driver.maximize_window()
        driver.implicitly_wait(5)

    def close_browser(self):
        """
        method to close the browser
        :return:
        """
        driver.quit()

    def open_url(self, url):
        """
        takes a string "url" as an argument and opens the associated web page in the browser
        :param url:
        :return:
        """
        driver.get(url)

    def element(self, locators, locator_key):
        """
        takes the xpath locator as an argument and returns the web element object
        :param locator:
        :return: web element
        """
        value = pydash.get(locators, locator_key)
        web_element = driver.find_element_by_xpath(value)
        return web_element

    def elements(self, locators, locator_key):
        """
        takes the xpath locator as an argument and returns a list of web elements
        :param locator:
        :return: list of web elements
        """
        value = pydash.get(locators, locator_key)
        web_elements = driver.find_elements_by_xpath(value)
        return web_elements

    def click(self, locators, locator_key):
        """
        clicks on a web element associated with the xpath locator passed as an argument
        :param locator:
        :return:
        """
        self.element(locators, locator_key).click()

    def clear(self, locators, locator_key):
        """
        clears the text present in the input field associated with the xpath locator passed as an argument
        :param locator:
        :return:
        """
        self.element(locators, locator_key).clear()

    def send_text(self, locators, locator_key, text):
        """
        sends text as input to the input field associated with the xpath locator
        :param locator:
        :param text:
        :return:
        """
        self.element(locators, locator_key).send_keys(text)

    def wait_till_element_present(self, locators, locator_key):
        """
        custom wait for appearance of a web element
        :param locator:
        :return:
        """
        count= 0
        flag = False
        try:
            while count<=5:
                count = count + 1
                flag = len(self.elements(locators, locator_key)) > 0
                if(flag):
                    break
            print(count)
        except:
            print("element not found")

    def is_element_display_on_screen(self, locators, locator_key):
        """
        takes xpath locator as an argument and returns a boolean value based on the web element's appearance
        :param locator:
        :return:
        """
        try:
            appear = self.element(locators, locator_key).is_displayed()
            if(appear):
                return True
            else:
                return False
        except:
            return False

    def is_element_present(self, locators, locator_key):
        """
        takes the xpath locator as an argument and returns a boolean value based on the element's presence
        :param locator:
        :return:
        """
        try:
            size = len(self.elements(locators, locator_key))
            if size > 0:
                return True
            else:
                return False
        except:
            return False

    def scroll_till_visibility_of_element(self, locators, locator_key):
        """
        scrolls untill the web element associated with the xpath locator is visible
        :param locator:
        :return:
        """
        element = self.element(locators, locator_key)
        driver.execute_script("arguments[0].scrollIntoView();", element)

    def scroll_by_pixel(self, pixel):
        """
        scrolls till the number of pixels passed as argument
        :param pixel:
        :return:
        """
        driver.execute_script(f"window.scrollBy(0, {pixel})")

    def scroll_till_last(self):
        """
        scrolls till the end of the web page
        :return:
        """
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight)")

    def clear_with_backspace(self, locators, locator_key):
        """
        clears the value of input fields using backspace
        :param locator:
        :return:
        """
        value_length = len(self.element(locators, locator_key).get_attribute("value"))
        act = ActionChains(driver)
        act.move_to_element(self.element(locators, locator_key)).click().perform()
        act.send_keys(Keys.END).perform()
        for i in range(value_length):
            act.send_keys(Keys.BACK_SPACE).perform()

    #Loader method is not working as of now, need to debug and modify
    def wait_till_loader(self):
        count = 0
        while count<20:
            flag = len(self.elements("//div[@class='app-loader']")) > 0
            if not flag:
                break
            count+=1
        if count >=20:
            print("more than 20 seconds loading!!")

    def scroll_and_click(self, locators, locator_key):
        """
        first scrolls till the element is visible and then performs the click operation once the element is found
        :param locator:
        :return:
        """
        self.scroll_till_visibility_of_element(locators, locator_key)
        if(self.is_element_display_on_screen(locators, locator_key)):
            self.click(locators, locator_key)

    def wait_for(self, time_in_secs):
        """
        hard wait, waits for time in seconds passed as an argument
        :param time_in_secs:
        :return:
        """
        time.sleep(time_in_secs)


    def select_by_index(self, locators, locator_key, index):
        """
        selects the option located by index in a dropdown
        :param locator:
        :param index:
        :return:
        """
        sel = Select(self.element(locators, locator_key))
        sel.select_by_index(index)

    def select_by_text(self, locators, locator_key, text):
        """
        selects the option by visible text in a dropdown
        :param locator:
        :param text:
        :return:
        """
        sel = Select(self.element(locators, locator_key))
        sel.select_by_visible_text(text)

    def tab_key(self):
        """
        performs the operation corresponding to the TAB button on the keyboard
        :return:
        """
        act = ActionChains(driver)
        act.send_keys(Keys.TAB).perform()

    def move_to_element_and_click(self, locators, locator_key):
        """
        corresponds to the mouse movement to the element's location and then performs left click
        :param locator:
        :return:
        """
        act = ActionChains(driver)
        act.move_to_element(self.element(locators, locator_key)).click(locators, locator_key).perform()

    def move_to_element(self, locators, locator_key):
        """
        corresponds to the mouse movement of the cursor to the element's location
        :param locator:
        :return:
        """
        act = ActionChains(driver)
        act.move_to_element(self.element(locators, locator_key)).perform()

    def right_click_on_element(self, locators, locator_key):
        """
        corresponds to mouse right button click
        :param locator:
        :return:
        """
        act = ActionChains()
        act.context_click(self.element(locators, locator_key)).perform()







