import os
import sys
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException


class SeleniumWebClass:
    """
    Selenium Web Class
    """
    grid = ['34.93.10.179', '172.24.0.124', '172.24.0.125']

    def __init__(self, url="", browser='chrome', remote=False, remote_ip=grid[0], remote_port=4444):
        """
        Init Function
        :param url:
        :param browser:
        :param remote_ip:
        :param remote_port:
        """
        remote_server = f"http://{remote_ip}:{remote_port}/wd/hub"
        if browser == 'chrome':
            self.web_driver_options = webdriver.ChromeOptions()
            self.web_driver_options.add_argument("--no-sandbox")
            self.web_driver_options.add_argument("--privileged")
            if remote:
                self.driver = webdriver.Remote(command_executor=remote_server,
                                               desired_capabilities=self.web_driver_options.to_capabilities())
            else:
                self.web_driver_options.add_argument("--ignore-certificate-errors")
                # self.web_driver_options.add_argument("--headless")
                self.driver = webdriver.Chrome(executable_path=self.get_selenium_driver_path(browser=browser),
                                               desired_capabilities=self.web_driver_options.to_capabilities())
        elif browser == 'firefox':
            self.web_driver_options = webdriver.FirefoxOptions()
            if remote:
                self.driver = webdriver.Remote(command_executor=remote_server,
                                               desired_capabilities=self.web_driver_options.to_capabilities())
            else:
                self.driver = webdriver.Firefox(executable_path=self.get_selenium_driver_path(browser=browser))

        self.driver.get(url)
        self.driver.maximize_window()

        self.elements = {
            'CLASS_NAME': By.CLASS_NAME,
            'ID': By.ID,
            'NAME': By.NAME,
            'XPATH': By.XPATH,
            'LINK_TEXT': By.LINK_TEXT,
            'TAG_NAME': By.TAG_NAME,
        }

    def get_selenium_driver_path(self, browser='chrome'):
        """
        Function to get selenium driver path
        Download drivers from here:
            FireFox: https://github.com/mozilla/geckodriver/releases
            Chrome: https://chromedriver.storage.googleapis.com/index.html?path=76.0.3809.126/
        :param browser:
        :return:
        """
        if 'linux' in sys.platform:
            # This is Linux System
            if browser == 'chrome':
                web_driver_path = os.path.join('decurtis_lib/web_drivers/Linux', 'chromedriver')
            else:
                web_driver_path = os.path.join('decurtis_lib/web_drivers/Linux', 'geckodriver')
        elif 'darwin' in sys.platform:
            # This is Mac System
            if browser == 'chrome':
                web_driver_path = os.path.join('decurtis/web_drivers/Darwin', 'chromedriver')
            else:
                web_driver_path = os.path.join('decurtis/web_drivers/Darwin', 'geckodriver')
        elif 'win' in sys.platform:
            # This is Windows System
            if browser == 'chrome':
                web_driver_path = os.path.join('decurtis/web_drivers/Windows', 'chromedriver.exe')
            else:
                web_driver_path = os.path.join('decurtis/web_drivers/Windows', 'geckodriver.exe')
        else:
            raise Exception("ERROR: UI Automation is supported only on Linux and Mac (as of now) !!")

        # Check if Driver File Exists
        web_driver_path = os.path.join(os.environ["ROOT_PATH"], web_driver_path)
        if not os.path.isfile(web_driver_path):
            raise Exception(f"File {web_driver_path} Not Found !!")

        return web_driver_path

    def class_element_click(self, name):
        """
        Send click to a class element
        :param name:
        :return:
        """
        self.driver.find_element_by_class_name(name).click()

    def click_on_link(self, name):
        """
        Click on Link given by id
        :return:
        """
        self.driver.find_element_by_class_name(name).submit()

    def class_element_send_text(self, name, keys):
        """
        Send test to a class element
        :param name:
        :param keys:
        :return:
        """
        self.driver.find_element_by_class_name(name).send_keys(keys)

    def id_send_text(self, name, keys):
        """
        Send text to a text element
        :param name:
        :param keys:
        :return:
        """
        self.driver.find_element_by_id(name).send_keys(keys)

    def take_screen_shot(self, file_name='screen_shot.png'):
        """
        Take Screen Shot and save it with file_name
        :param file_name:
        :return:
        """
        self.driver.save_screenshot(file_name)

    def wait_for_element(self, element_id, timeout=300, element='CLASS_NAME'):
        """
        Wait for Element to Load
        :param timeout:
        :param element:
        :return:
        """
        try:
            element_present = EC.presence_of_element_located((self.elements[element], element_id))
            WebDriverWait(self.driver, timeout).until(element_present)
        except TimeoutException:
            print("Timed out waiting for page to load")

    def __del__(self):
        """
        Destructor Class to close the Driver
        :return:
        """
        self.driver.quit()


if __name__ == "__main__":
    selenium_class = SeleniumWebClass(url='https://www.flipkart.com/', browser='firefox', remote=True)
    selenium_class.class_element_send_text('_2zrpKA', 'anshumangoyal@gmail.com')
    selenium_class.class_element_send_text('_3v41xv', 'FlipKart@32145')
    selenium_class.class_element_click('_1avdGP')
    selenium_class.wait_for_element(element_id='_2aUbKa', element=By.CLASS_NAME)
    selenium_class.take_screen_shot()
    print(selenium_class.driver.title)
