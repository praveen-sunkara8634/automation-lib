__author__ = 'anshuman.goyal'

import psycopg2
from psycopg2.extras import RealDictCursor


class Database:
    """
    DB Connector for DXP
    """

    def __init__(self, host, username, password, database=None, port=None):
        """
        Connect to database and provide it's marker
        :param host:
        :param port:
        :param username:
        :param password:
        :param database:
        """
        self.host = host
        self.port = port
        if self.port is None:
            self.port = 5432
        self.username = username
        self.password = password
        self.database = database

        # variables
        self.connection = None
        self._connect_to_db()

    def _connect_to_db(self):
        """
        Function to connect to db
        :return:
        """
        self.connection = psycopg2.connect(
            user=self.username,
            password=self.password,
            host=self.host,
            port=self.port,
            database=self.database,
            connect_timeout=5
        )

    def run_and_fetch_data(self, query):
        """
        Run db Query and Fetch Data from Database
        :param query:
        :return:
        """
        # Make a new cursor each time for a new query
        cursor = self.connection.cursor(cursor_factory=RealDictCursor)
        cursor.execute(query)
        _records = cursor.fetchall()
        cursor.close()
        return _records

    def insert_comp_status_data(self, query):
        """
        Insert Component Status Data in db
        :param query:
        :return:
        """
        if query['Status'] == 'FAILED':
            query['Status'] = '0'
        else:
            query['Status'] = '1'
        cursor = self.connection.cursor(cursor_factory=RealDictCursor)
        postgres_insert_query = """ INSERT INTO component_report(Component,
                                                    status,
                                                    test_case,
                                                    environment,
                                                    platform,
                                                    shipcode,
                                                    test_link,
                                                    jenkins_build,
                                                    component_order,jenkins_url) 
                                                    VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
        record_to_insert = (
            query['Component'], query['Status'], query['Test-Case'], query['Environment'], query['Platform'],
            query['shipCode'], query['TestPlan'], query['jenkins_build'], query['order'], query['jenkins_url']
        )
        cursor.execute(postgres_insert_query, record_to_insert)
        self.connection.commit()
        count = cursor.rowcount
        return count

    def insert_test_cases_data(self, query):
        """
        Insert Test-Cases Data from Test Rails
        :param query:
        :return:
        """
        cursor = self.connection.cursor(cursor_factory=RealDictCursor)
        _insert_query = """ INSERT INTO testcase(component,
                                                    api_smoke,
                                                    api_regression,
                                                    ui_regression,
                                                    ui_smoke,
                                                    test_plan) 
                                                    VALUES (%s,%s,%s,%s,%s,%s)"""
        _to_insert = (
            query['component'], query['api_smoke'], query['api_regression'], query['ui_regression'], query['ui_smoke'],
            query['test_plan'])
        cursor.execute(_insert_query, _to_insert)
        self.connection.commit()
        count = cursor.rowcount
        return count

    def insert_vv_shore_side_data(self, query):
        """
        Insert Test-Cases Data from Test Rails
        :param query:
        :return:
        """
        cursor = self.connection.cursor(cursor_factory=RealDictCursor)
        _insert_query = """ INSERT INTO e2eshipdata(reservationid,
                                                    reservationguestid,
                                                    embarkdate,
                                                    debarkdate,
                                                    voyageid,
                                                    emailid,
                                                    guestid,
                                                    firstname,
                                                    lastname,
                                                    embarkationstatus) 
                                                    VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
        _to_insert = (
            query['reservationid'], query['reservationguestid'], query['embarkdate'], query['debarkdate'],
            query['voyageid'], query['emailid'], query['guestid'], query['firstname'], query['lastname'],
            query['embarkationstatus'])
        cursor.execute(_insert_query, _to_insert)
        self.connection.commit()
        count = cursor.rowcount
        return count

    def truncate_data(self, table):
        """
        Insert Test-Cases Data from Test Rails Priority Wise Table
        :param table:
        :return:
        """

        cursor = self.connection.cursor(cursor_factory=RealDictCursor)
        _query = f"truncate table {table} "
        cursor.execute(_query)
        self.connection.commit()
        count = cursor.rowcount
        return count

    def insert_test_rail_data(self, query):
        """
        Insert Test-Cases Data from Test Rails Priority Wise Table
        :param query:
        :return:
        """
        cursor = self.connection.cursor(cursor_factory=RealDictCursor)
        _insert_query = """INSERT INTO testraildata (suite,
                                                    in_dxp,
                                                    created_on,
                                                    priority,
                                                    case_id, 
                                                    section,automated,in_ci,ci_date,suite_name,automation_date,in_vv)   
                                                    VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
        _to_insert = (
            query['suite'], query['in_dxp'], query['created'], query['priority'], query['case_id'], query['section'],
            query['automated'], query['in_ci'], query['ci_date'], query['suite_name'], query['automation_date'],
            query['in_vv']
        )
        cursor.execute(_insert_query, _to_insert)
        self.connection.commit()
        count = cursor.rowcount
        return count

    def delete_data_query(self, table, condition):
        """
        Function to run query to delete data from database
        :param table:
        :param condition:
        :return:
        """
        cursor = self.connection.cursor(cursor_factory=RealDictCursor)
        _query = f"DELETE FROM {table} {condition}"
        cursor.execute(_query)
        self.connection.commit()
        count = cursor.rowcount
        return count

    def insert_columns(self, query):  # TODO: Create one method for all commit executions
        """
        Function to insert new columns in db
        :param query:
        :return:
        """
        cursor = self.connection.cursor(cursor_factory=RealDictCursor)
        cursor.execute(query)
        self.connection.commit()
        count = cursor.rowcount
        return count
