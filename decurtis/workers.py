import re
import json
import collections
import allure
import os
import yaml
from collections import namedtuple
from decurtis.k8 import K8Lib
from decurtis.common import urljoin, urlparse, get_version
from decurtis.logger import get_logger
from decurtis.bitbucket import BitBucketApi
from decurtis.common import read_json_file, get_environment_data, create_auth_basic_token, save_allure, \
    app_secret_read_yaml
from psycopg2.extras import RealDictCursor
from types import SimpleNamespace as Namespace

logger = get_logger(name="WORKERS")


def get_wrong_urls(config, database):
    """
    Function to fetch wrong URLS
    :param: config
    :param: database
    """
    passed = True
    with open('test_data/configs_all.json', 'r') as _fp:
        data = _fp.read()

    data = data.replace("{{ship}}", urlparse(config.ship.url).hostname)
    data = data.replace("{{shore}}", urlparse(config.shore.url).hostname)
    data = data.replace("{{chat}}", re.sub(r"(.*?)-(.*)", r"chat-\2", urlparse(config.ship.url).hostname))
    data = data.replace("{{cms}}", re.sub(r"(.*?)-(.*)", r"cms-\2", urlparse(config.ship.url).hostname))
    data = data.replace("{{wordpress}}", re.sub(r"(.*?)-(.*)", r"wordpress-\2", urlparse(config.ship.url).hostname))

    # Sync URL's are different in Virgin and DXP, so this is required !!
    if config.platform == 'DXP':
        data = data.replace("{{sync}}", str(urlparse(config.ship.url).hostname).replace('sagar', 'sagar-syncgateway'))
    else:
        data = data.replace("{{syncgateway}}",
                            re.sub(r"(.*?)-(.*)", r"syncgateway-\2", urlparse(config.ship.url).hostname))
    reference = json.loads(data)

    _query = "select * from public.ufngetsettingvalueforverification();"
    configs_all = {
        'ship': None,
        'shore': None
    }
    for comp in ['ship', 'shore']:
        rows = getattr(database, comp).run_and_fetch_data(query=_query)

        # Saving all Internal, External and Exposed URL's
        configs = {
            'internal': {x['yamlkey']: x['value'] for x in rows if x['category'] == 'INTERNAL URL'},
            'external': {x['yamlkey']: x['value'] for x in rows if x['category'] == 'EXTERNAL URL'},
            'exposed': {x['yamlkey']: x['value'] for x in rows if x['category'] == 'EXPOSED URL'}
        }
        configs_all[comp] = configs
        save_allure(data=configs, name=f"configs_{comp}.json")

        extra_keys = {
            "internal": list(set(configs['internal'].keys()) ^ set(reference[comp]['internal'].keys())),
            "external": list(set(configs['external'].keys()) ^ set(reference[comp]['external'].keys())),
            "exposed": list(set(configs['exposed'].keys()) ^ set(reference[comp]['exposed'].keys()))
        }
        if len(extra_keys['internal']) > 0 or len(extra_keys['external']) > 0 or len(extra_keys['exposed']) > 0:
            save_allure(data=extra_keys, name=f'{comp}-extra-configs.json')

        not_matching = {
            "internal": dict(),
            "external": dict(),
            "exposed": dict()
        }

        for _key, now_value in configs['internal'].items():
            if _key in reference[comp]['internal']:
                ref_value = reference[comp]['internal'][_key]
                if ref_value != now_value:
                    not_matching["internal"][_key] = {'Reference': ref_value, "now": now_value}

        for _key, now_value in configs['external'].items():
            if _key in reference[comp]['external']:
                ref_value = reference[comp]['external'][_key]
                if ref_value != now_value:
                    not_matching["external"][_key] = {'Reference': ref_value, "now": now_value}

        for _key, now_value in configs['exposed'].items():
            if _key in reference[comp]['exposed']:
                ref_value = reference[comp]['exposed'][_key]
                if ref_value != now_value:
                    not_matching["exposed"][_key] = {'Reference': ref_value, "now": now_value}

        if len(not_matching['internal']) > 0 or len(not_matching['external']) > 0 or len(not_matching['exposed']) > 0:
            passed = False
            save_allure(data=not_matching, name=f"{comp}_not_matching.json")

    save_allure(data=configs_all, name=f'configs_all_{config.env}.json')
    return passed


def add_e2e_db_data(database, test_data, config):
    """
    Function to add data in e2e database
    :param: database
    :param: test_data
    :param: config
    """
    cursor = database.connection.cursor(cursor_factory=RealDictCursor)
    columns = [
        'case_id', 'component', 'correlationid', 'debark_date', 'e2e_job_name', 'e2e_job_number', 'embark_date',
        'environment', 'environment_masked', 'git_branch', 'git_pr_num', 'git_repo', 'guest_count', 'infra_job_name',
        'infra_job_number', 'password', 'platform', 'pr_author', 'reservation_id', 'result', 'ship', 'ship_code',
        'shore', 'test_case', 'user_name', 'voyage_id'
    ]
    values = ",".join(['%s' for _ in columns])
    columns = ",".join(columns)
    query = f"INSERT INTO e2e_data({columns}) VALUES ({values})"

    if 'jenkins' not in test_data:
        test_data['jenkins'] = {'job': []}

    records = (
        test_data['case_id'] if 'case_id' in test_data else None,
        test_data['comp'],
        test_data['correlationId'] if 'correlationId' in test_data else None,
        test_data['sailingDebarkDate'] if 'sailingDebarkDate' in test_data else None,
        test_data['JOB_NAME'] if 'JOB_NAME' in test_data else None,
        test_data['BUILD_NUMBER'] if 'BUILD_NUMBER' in test_data else None,
        test_data['sailingEmbarkDate'] if 'sailingEmbarkDate' in test_data else None,
        config.env,
        config.envMasked,
        test_data['jenkins']['job']['Branch'] if 'Branch' in test_data['jenkins']['job'] else None,
        test_data['jenkins']['job']['PR'] if 'PR' in test_data['jenkins']['job'] else None,
        test_data['jenkins']['job']['Repository'] if 'Repository' in test_data['jenkins']['job'] else None,
        test_data['guests'] if 'guests' in test_data else None,
        test_data['jenkins']['job']['name'] if 'name' in test_data['jenkins']['job'] else None,
        test_data['jenkins']['job']['number'] if 'number' in test_data['jenkins']['job'] else None,
        test_data['password'] if 'password' in test_data else None,
        config.platform,
        test_data['authors'] if 'authors' in test_data else None,
        test_data['reservationID'] if 'reservationID' in test_data else None,
        test_data['result'],
        config.ship.url,
        config.ship.code,
        config.shore.url,
        test_data['test_case'] if 'test_case' in test_data else None,
        test_data['username'] if 'username' in test_data else None,
        test_data['voyageId'] if 'voyageId' in test_data else None,
    )

    cursor.execute(query, records)
    database.connection.commit()


def verify_info_calls(rest_oci, info_calls, url):
    """
    Verify Info Calls
    :param: rest_oci
    :param: info_calls
    :param: url
    """
    failing_calls = dict()
    version_calls = dict()
    for _comp in info_calls:
        _call = info_calls[_comp].split('/')[-2]
        try:
            _content = rest_oci.send_request(method="GET", url=info_calls[_comp], auth="basic").content
        except Exception as exp:
            logger.warning(f"Info Call: {info_calls[_comp]} Throwing {exp}")
            failing_calls[_comp] = {'url': info_calls[_comp],
                                    'end-point': urlparse(info_calls[_comp]).path.split('/')[1]}
            continue
        version_calls[_call] = get_version(_content)

    name = "Ship"
    if "shore" in str(url).lower():
        name = "Shore"

    allure.attach(json.dumps(version_calls, indent=2, sort_keys=True), name=f'versions{name}.json',
                  attachment_type=allure.attachment_type.JSON)

    if len(failing_calls) > 0:
        allure.attach(json.dumps(failing_calls, indent=2, sort_keys=True), name=f'failed{name}Versions.json',
                      attachment_type=allure.attachment_type.JSON)

    return failing_calls, version_calls


def get_configs_from_database(database):
    """
    Get Configs from Database
    :param database
    """
    configs = collections.namedtuple('config', ['internal', 'external', 'exposed'])
    _query = "select * from public.ufngetsettingvalueforverification();"

    rows = database.run_and_fetch_data(query=_query)

    # Saving all Internal, External and Exposed URL's
    internal = {x['yamlkey']: x['value'] for x in rows if x['category'] == 'INTERNAL URL'}
    external = {x['yamlkey']: x['value'] for x in rows if x['category'] == 'EXTERNAL URL'}
    exposed = {x['yamlkey']: x['value'] for x in rows if x['category'] == 'EXPOSED URL'}

    return configs(internal=internal, external=external, exposed=exposed)


def get_credentials(rest_bb, platform, environment, ship=None, shore=None):
    """
    This function will read Credentials from Bit bucket and send them back in NamedTuple Format
    :param rest_bb
    :param platform
    :param environment
    :param ship
    :param shore
    """
    _file = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'utils/place_holder_app_secrets.json')
    data = read_json_file(_file)
    client = getattr(getattr(getattr(data, platform), environment), 'client')
    repo = getattr(getattr(getattr(data, platform), environment), 'repo')
    place_holders = getattr(getattr(getattr(data, platform), environment), 'place_holders')
    app_secrets = getattr(getattr(getattr(data, platform), environment), 'app_secrets')

    ship_app_secrets = app_secret_read_yaml(rest_bb.read_remote_file(client, repo, app_secrets.ship))
    ship_place_holders = rest_bb.read_remote_file(client, repo, place_holders.ship)

    shore_app_secrets = app_secret_read_yaml(rest_bb.read_remote_file(client, repo, app_secrets.shore))
    shore_place_holders = rest_bb.read_remote_file(client, repo, place_holders.shore)

    _id = ship_app_secrets['moderateonlinecheckinwebClientId']
    _secret = ship_app_secrets['moderateonlinecheckinwebClientSecret']
    moci = create_auth_basic_token(_id, _secret)

    _id = shore_app_secrets['onlineCheckInWebClientId']
    _secret = shore_app_secrets['onlineCheckInWebSecret']
    oci = create_auth_basic_token(_id, _secret)

    _id = shore_app_secrets['bookVoyageBFFClientId']
    _secret = shore_app_secrets['bookVoyageBFFClientSecret']
    book = create_auth_basic_token(_id, _secret)

    _id = ship_app_secrets['hydrationClientId']
    _secret = ship_app_secrets['hydrationClientSecret']
    hydration = create_auth_basic_token(_id, _secret)

    matchers = dict()
    matchers["host"] = re.compile(r"\$PGHOST_1\$\|(.*?)$", re.I | re.M)
    matchers["port"] = re.compile(r"\$PGPORT_INSTANCE_1\$\|(.*?)$", re.I | re.M)
    matchers["username"] = re.compile(r"\$PGUSER_READWRITE\$\|(.*?)$", re.I | re.M)
    matchers["password"] = re.compile(r"\$PGPASSWORD_READWRITE\$\|(.*?)$", re.I | re.M)

    structure = {
        'ship': {
            'db': {
                'host': re.search(matchers['host'], ship_place_holders).group(1),
                'port': re.search(matchers['port'], ship_place_holders).group(1),
                'username': re.search(matchers['username'], ship_place_holders).group(1),
                'password': re.search(matchers['password'], ship_place_holders).group(1)
            }
        },
        'shore': {
            'db': {
                'host': re.search(matchers['host'], shore_place_holders).group(1),
                'port': re.search(matchers['port'], shore_place_holders).group(1),
                'username': re.search(matchers['username'], shore_place_holders).group(1),
                'password': re.search(matchers['password'], shore_place_holders).group(1)
            }
        },
        'oci': oci,
        'moci': moci,
        'book': book,
        'hydration': hydration
    }

    # Get db host and port for ship
    if ship is not None and re.search(r"(.*?)\.pullrequest.dxp-decurtis.com", ship, re.I | re.M):
        # Pull Request Cluster
        structure['ship']['db']['host'] = '34.73.39.23'
        structure['ship']['db']['port'] = K8Lib(url=ship).get_postgres_port()
        structure['shore']['db']['host'] = '34.73.39.23'
        structure['shore']['db']['port'] = K8Lib(url=shore).get_postgres_port()
    elif ship is not None and re.search(r"(.*?)\.developer.dxp-decurtis.com", ship, re.I | re.M):
        # Developer Environments Postgres DB
        structure['ship']['db']['host'] = '35.229.92.86'
        structure['ship']['db']['port'] = K8Lib(url=ship).get_postgres_port()
        structure['shore']['db']['host'] = '35.229.92.86'
        structure['shore']['db']['port'] = K8Lib(url=shore).get_postgres_port()
    else:
        pass

    return json.loads(json.dumps(structure), object_hook=lambda d: Namespace(**d))
