from decurtis.common import *
from decurtis.rest import SendRestRequest


class SlackNotification(SendRestRequest):
    """
    Class to send slack notification
    """
    USER_LIST = "{}/users.list?limit=1000"
    CONVERSATIONS_LIST_PRIVATE = "{}/conversations.list?limit=1000&types=private_channel"
    CONVERSATIONS_LIST_PUBLIC = "{}/conversations.list?limit=1000&types=public_channel"
    CHAT_MESSAGE = "{}/chat.postMessage"
    FILE_UPLOAD = "{}/files.upload"
    USER_GROUPS = "{}/usergroups.list"
    USERS_LOOKUP_BY_EMAIL = "{}/users.lookupByEmail"

    def __init__(self, token=None):
        """
        Initiate Slack API session
        :param token:
        """
        super().__init__()
        self.base_url = "https://slack.com/api"
        if token is None:
            # This is Decurtis Alerts Token (Updated on 9th Jan 2020)
            self.token = 'xoxp-18967994931-482781045491-917601184723-6d19e9177e6302229bde6fef354a8bc0'
        else:
            self.token = token

        self.session.headers = {
            'Content-Type': 'application/json',
            'Authorization': f"Bearer {self.token}"
        }

        self.users_list = self._user_list()
        self.conversations_list = self._conversations_list()
        self.user_groups = self._user_groups()

        slack_data = dict()

        slack_data['users'] = [
            {'name': x['name'], 'id': x['id'], 'real_name': x['profile']['real_name'], 'email': x['profile']['email'], }
            for x in self.users_list if 'profile' in x and 'email' in x['profile'] and x['deleted'] is False]

        slack_data['channels'] = [
            {'name': x['name'], 'id': x['id'], 'is_channel': x['is_channel'], 'is_group': x['is_group']} for x in
            self.conversations_list if x['is_archived'] is False]

        slack_data['groups'] = [{'id': x['id'], 'name': x['name'], 'handle': x['handle']} for x in
                                self.user_groups['usergroups'] if x['is_usergroup'] is True]

        with open('slack_data.json', 'w') as _fp:
            _fp.write(json.dumps(slack_data, indent=2, sort_keys=True))

    def _send_file_post_request(self, url, recipient, message, file_name):
        """
        Send Post Request to upload file
        :param url:
        :param recipient:
        :param message:
        :param file_name:
        :return:
        """
        url = url.format(self.base_url)
        files = {
            "channels": (None, recipient),
            'title': (None, file_name),
            "filename": (None, file_name),
            'initial_comment': (None, message),
            'file': (file_name, open(file_name, 'rb')),
        }
        _content = self.send_request(method='POST', url=url, files=files)
        return _content

    def _user_list(self):
        """
        Get User List
        :return:
        """
        url = self.USER_LIST.format(self.base_url)
        _content = self.send_request(method='POST', url=url, data=None).content
        return _content['members']

    def _conversations_list(self):
        """
        Get Channels List
        :return:
        """
        url = self.CONVERSATIONS_LIST_PRIVATE.format(self.base_url)
        channels_private = self.send_request(method='POST', url=url, data=None).content['channels']

        url = self.CONVERSATIONS_LIST_PUBLIC.format(self.base_url)
        channels_public = self.send_request(method='POST', url=url, data=None).content['channels']

        conversations = channels_private + channels_public
        return sorted(conversations, key=lambda i: i['name'])

    def _user_groups(self):
        """
        Get Channels List
        :return:
        """
        url = self.USER_GROUPS.format(self.base_url)
        _content = self.send_request(method='POST', url=url, data=None).content
        return _content

    def _get_recipient(self, to_send):
        """
        Get Recipient to send message
        :param to_send:
        :return:
        """
        if '@' in to_send:
            user = self._get_user_id(to_send)
            if user is not None:
                return user
        else:
            channel = self._get_channel_id(to_send)
            if channel is not None:
                return channel

        # when no user is there, send it to anshuman.goyal
        user = self._get_user_id('anshuman.goyal@decurtis.com')
        return user

    def _get_user_id(self, user):
        """
        Get User ID for given email
        :param user:
        :return:
        """
        for _user in self.users_list:
            if 'profile' in _user:
                profile = _user['profile']
                if 'email' in profile and str(profile['email']).lower() == str(user).lower():
                    return _user['id']
        else:
            return None

    def _get_channel_id(self, channel):
        """
        Get Group ID for given Group
        :param channel:
        :return:
        """
        for _channel in self.conversations_list:
            if _channel['name'] == channel:
                return _channel['id']
        else:
            return None
            # raise Exception(f"ERROR: channel-id for {channel} not found !!"))

    def get_id(self, name):
        """
        Get Slack ID
        :param name:
        :return:
        """
        # Get id from user list first
        for _user in [x for x in self.users_list if 'profile' in x and 'email' in x['profile']]:
            if _user['name'] == name:
                return f"<@{_user['id']}>"
            elif str(_user['profile']['real_name']).lower() == str(name).lower():
                return f"<@{_user['id']}>"
            elif _user['profile']['email'] == name:
                return f"<@{_user['id']}>"
            else:
                continue

        # Get id from conversation list (channels)
        for _conversation in self.conversations_list:
            if _conversation['name'] == name:
                return f"<#{_conversation['id']}>"
            else:
                continue

        # Get id from groups
        for _group in self.user_groups['usergroups']:
            if _group['name'] == name:
                return f"<!subteam^{_group['id']}>"
            elif _group['handle'] == name:
                return f"<!subteam^{_group['id']}>"
            else:
                continue

    def send_file(self, message, recipients, file_name):
        """
        This Function will upload file with message
        :param message:
        :param recipients:
        :param file_name:
        :return:
        """
        if isinstance(recipients, str):
            recipients = recipients.split(',')

        if not isinstance(recipients, list):
            raise Exception(f"recipients has to be list always !!")

        url = self.FILE_UPLOAD.format(self.base_url)

        for recipient in recipients:
            _id = self._get_recipient(recipient)
            status = self._send_file_post_request(url=url, recipient=_id, message=message, file_name=file_name)
            if not status.ok:
                raise Exception(f"Failed to upload file '{file_name}' to {recipient}")

    def send_message(self, message, recipients):
        """
        Send message to user
        :param message:
        :param recipients:
        :return:
        """
        if isinstance(recipients, str):
            recipients = recipients.split(',')

        if not isinstance(recipients, list):
            raise Exception(f"recipients has to be list always !!")

        for recipient in recipients:
            _user = self._get_recipient(recipient)
            data = {
                "channel": _user,
                "as_user": True,
                "text": message
            }
            url = self.CHAT_MESSAGE.format(self.base_url)
            status = self.send_request(method='POST', url=url, data=data).content
            if not status['ok']:
                raise Exception(f"Failed to send message '{message}' to {recipient}")

            # Wait 1 second before sending message to 1+ users
            time.sleep(1)

    def get_user_by_email(self, email):
        """
        Get Slack ID by E-Mail, This Require certain roles to be added to user profile
        :param  email
        """
        url = self.USERS_LOOKUP_BY_EMAIL.format(self.base_url)
        params = {
            'email': email
        }
        _content = self.send_request(method='GET', url=url, params=params).content
        return _content['user']['id']


if __name__ == "__main__":
    slack = SlackNotification()
    # slack.get_user_by_email('anshuman.goyal@decurtis.com')
    # _id = slack.get_id('qa-support')
    # slack.send_file(f"{_id} Hi !!", "anshuman.goyal@decurtis.com", 'utils/slack.py')
    slack.send_message(f"Hi !!", "anshuman.goyal@decurtis.com")
    # slack.send_file(f"Hi !!", "anshuman.goyal@decurtis.com", 'jira.py')
