import json
import os
import re
import allure
from json import JSONDecodeError
from decurtis.common import urljoin
from decurtis.encryption import DecurtisEncryption

import kubernetes

from decurtis.logger import get_logger

logger = get_logger(name="K8LIB")


class K8Lib:
    """
    Class for Kubernetes Collection & Connection !!
    """

    def __init__(self, url):
        """
        Connect to K8 Cluster
        :param url:
        """
        self.url = url
        self.env = re.search(r"https://(.*?)-(sagar|shore)", self.url, re.I | re.M).group(1)
        self.name_space = "-".join(re.search(r"https://(.*?)-(sagar|shore)", self.url, re.I | re.M).group(1, 2))
        self.config = '/tmp/config.yaml'

        # for QA1, we have sagar and shore as namespace(s)
        if self.name_space == 'qa1-sagar':
            self.name_space = 'sagar'
        if self.name_space == 'qa1-shore':
            self.name_space = 'shore'

        # E2E is not monitoring these Pods
        self.ignored_pods = ['kafka', 'topic', 'pzoo', 'postgresql', 'redis']

        self.params = {
            'pretty': 'pretty_example'
        }
        self.path = os.path.dirname(os.path.realpath(__file__))

        # Load K8 Config
        self._load_k8_config()

        # Connect to K8 Core V1 API
        self.client = kubernetes.client.CoreV1Api()

        # Get all Pods
        self.pods = self._get_all_pods()

        self.known_messages = [
            re.compile(r"^Document modify date is NULL.*", re.I | re.M),
            re.compile(r"^HealthChecker$", re.I | re.M),
            re.compile(r"^Invalid guest doc type : Security Photo$", re.I | re.M),
            re.compile(r"^Invalid guest doc type : Profile Photo$", re.I | re.M),
            re.compile(r"^INFO: THIS IS NOT AN ERROR, MESSAGE IS FOR DEBUGGING PURPOSE ONLY$", re.I | re.M),
        ]

    def _load_k8_config(self):
        """
        Load K8 Config, this loads config in environment variables
        """
        if re.search(r"(.*?)\.pullrequest.dxp-decurtis.com", self.url, re.I | re.M):
            logger.debug("Loading K8 config for Master/PR Environments ...")
            config_file = os.path.join(self.path, 'credentials/k8_pr_master')
        elif re.search(r"(.*?)\.developer.dxp-decurtis.com", self.url, re.I | re.M):
            logger.debug("Loading K8 config for Developer Environments ...")
            config_file = os.path.join(self.path, 'credentials/k8_developer')
        elif re.search(r"https://qa1-(.*?)\.dxp-decurtis.com", self.url, re.I | re.M):
            logger.debug("Loading K8 config for QA1 Environment ...")
            config_file = os.path.join(self.path, 'credentials/k8_qa1')
        else:
            self.params['since_seconds'] = '720'
            raise Exception(f"K8 Logs are not available for {self.url}")

        if os.path.isfile(config_file):
            DecurtisEncryption().decrypt_file(config_file, self.config)
            kubernetes.config.load_kube_config(self.config)
        else:
            raise Exception(f"Not able to find config file at {config_file}")

    def get_postgres_port(self):
        """
        Get Pod Port
        """
        for _pod in self.client.list_namespaced_service_with_http_info(namespace=self.name_space)[0].items:
            _name = _pod.metadata.name
            if 'postgresql' in _name:
                return _pod.spec.ports[0].node_port
        else:
            return None

    def _get_all_pods(self):
        """
        Get all Pods in a name_space
        """
        _pods = []
        for _pod in self.client.list_namespaced_pod(namespace=self.name_space).items:
            if _pod.status.phase == 'Running':
                _pods.append(_pod.metadata.name)
        if len(_pods) == 0:
            logger.error(f"Found Zero Pods for {self.name_space} namespace !!")
        return [x for x in _pods if str(x).split('-')[0] not in self.ignored_pods]

    def get_all_pods_health(self):
        """
        Get Restart count of all containers inside a pod
        """
        pods_health = dict()
        for _pod in self.client.list_namespaced_pod(namespace=self.name_space).items:
            if _pod.status.container_statuses is not None:
                name = _pod.metadata.name
                if name.split('-')[0] in self.ignored_pods:
                    continue
                pods_health[name] = dict()
                try:
                    restart_count = sum(
                        [x.restart_count for x in _pod.status.container_statuses if x.restart_count > 0])
                except (TypeError, ValueError):
                    continue
                pods_health[name]['restart_count'] = restart_count
                pods_health[name]['version'] = str(_pod.status.container_statuses[0].image).split(":")[-1]

                for container in _pod.spec.containers:
                    pods_health[name]['liveness_probe'] = container.liveness_probe
                    pods_health[name]['readiness_probe'] = container.readiness_probe

        return pods_health

    def get_pods_with_restarts(self):
        """
        Get all Pods with Restarts
        """
        pods_health = self.get_all_pods_health()
        pods_with_restarts = {k: v for k, v in pods_health.items() if pods_health[k]['restart_count'] > 0}
        return pods_with_restarts

    def get_liveness_probes(self):
        """
        Get Pods with Liveliness Probes and fetch info/version calls from them
        """
        pods_health = self.get_all_pods_health()
        pods_with_liveness = {k: v for k, v in pods_health.items() if pods_health[k]['liveness_probe'] is not None}
        info_calls = dict()
        no_info_calls = dict()
        image_versions = dict()
        for k, v in pods_with_liveness.items():
            # if str(k).startswith('dxp'):
            if v['liveness_probe'].http_get and v['liveness_probe'].http_get.path:
                path = v['liveness_probe'].http_get.path
                if path.split('/')[-1] in ['info', 'version']:
                    info_calls[k.split('-')[0]] = urljoin(self.url, path)
                else:
                    no_info_calls[k.split('-')[0]] = urljoin(self.url, path)
                image_versions[k.split('-')[0]] = v['version']
        return info_calls, no_info_calls, image_versions

    def get_logs_for_restarted_containers(self):
        """
        Get logs for Restarted Containers
        """
        final_logs = dict()
        pods_with_restarts = self.get_pods_with_restarts()
        for container in pods_with_restarts:
            _logs = self.get_logs_for_pod(pod_name=container)
            final_logs[container] = _logs
        return final_logs

    def process_logs(self, logs):
        """
        Process the Pod Logs
        :param logs
        """
        severity = str(os.environ.get('PROCESS_POD_LOGS', 'ERROR,INFO,DEBUG')).upper().split(',')
        json_logs = []
        has_parsed = False
        for _log in logs.split('\n'):
            try:
                parsed = json.loads(_log)
                if isinstance(parsed, dict):
                    if len(parsed) > 1 and 'message' in parsed and 'severity' in parsed:
                        has_parsed = True
                        if str(parsed['severity']).upper() in (str(x).upper() for x in severity):
                            for _known in self.known_messages:
                                if re.search(_known, parsed['message']):
                                    break
                            else:
                                json_logs.append(parsed)
            except (JSONDecodeError, ValueError, Exception):
                pass

        if len(json_logs) == 0 and has_parsed is False:
            return logs

        return json_logs

    def get_logs_for_pod(self, pod_name, duration=None):
        """
        Get Logs for a given Pod
        :param pod_name:
        :param duration:
        :return:
        """
        if duration:
            self.params['since_seconds'] = str(duration)

        # @anshuman's comments 10-Feb-2020
        # We iterate over logs in each pod and capture only those logs which are Json Parse-Able
        # there are few patterns which we know already and those errors will be skipped
        for _pod in self.pods:
            if not re.search(f"{pod_name}.*", _pod, re.I | re.M):
                continue
            else:
                try:
                    logs = self.client.read_namespaced_pod_log(_pod, self.name_space, **self.params)
                except (Exception, KeyError, ValueError):
                    return None

                parsed = self.process_logs(logs)
                if bool(parsed):
                    return parsed
                else:
                    return []

    def get_logs_for_all_pods(self, duration=None):
        """
        Get Logs for all Pods
        :params: duration
        :return:
        """
        if duration:
            self.params['since_seconds'] = str(duration)

        logger.debug(f"Getting Pod Logs for last {duration} Seconds")

        # We are not saving Some Logs, as they are useless and huge !!
        final_logs = dict()
        for count, _pod in enumerate(self.pods, start=1):
            try:
                logger.debug(f"Gathering logs for {_pod} POD {count} of {len(self.pods)}")
                final_logs[_pod] = self.get_logs_for_pod(_pod, duration)
            except (Exception, ValueError):
                pass

        final_logs = {x: y for x, y in final_logs.items() if y is not None and len(y) != 0}
        for pod_name in final_logs:
            name = f"{self.name_space}-{pod_name}.json"
            allure.attach(json.dumps(final_logs[pod_name], indent=2, sort_keys=True), name=name,
                          attachment_type=allure.attachment_type.JSON)
            logger.debug(f"K8 Logs {name} saved to allure successfully")

        return final_logs

    def __del__(self):
        """
        Destructor, where we will do a cleanup
        """
        logger.debug(f'Deleting Config {self.config} File')
        os.unlink(self.config)


if __name__ == "__main__":
    sagar = K8Lib(url="https://anshumangoyal-sagar.developer.dxp-decurtis.com").get_logs_for_all_pods(duration=6000)
    shore = K8Lib(url='https://anshumangoyal-shore.developer.dxp-decurtis.com').get_logs_for_all_pods(duration=6000)
