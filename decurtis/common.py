import base64
import collections
import json
import os
import random
import re
import glob
import csv
import yaml
import secrets
import string
import time
import re
import uuid
import zipfile
from faker import Faker
from collections import namedtuple, OrderedDict
from datetime import datetime, date
from functools import wraps
from pathlib import Path
from types import SimpleNamespace as Namespace
from urllib.parse import urlsplit, urlparse, urlunparse
from psycopg2.extras import RealDictCursor
from decurtis.encryption import DecurtisEncryption

import websocket
import allure
import pytz
import requests
from bs4 import BeautifulSoup
from dateutil.parser import parse
from dateutil.relativedelta import relativedelta
from decurtis.logger import get_logger
from pycountry import countries
from pytz import timezone
import urllib3
from decurtis.database import *

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

logger = get_logger(name="COMMON")
URLs = set()

with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'link_mapper.json'), 'r') as fp:
    link_mapper = json.load(fp)


def generate_random_card_expiry(from_year, to_year):
    """
    Function to generate Random Date
    :param from_year:
    :param to_year:
    :return:
    """
    return date(random.randint(from_year, to_year), random.randint(1, 12), random.randint(1, 28)).strftime('%Y-%m')


def generate_credit_card_number(name, use_predefined=True):
    """
    Generate Credit Card Number
    :param name:
    :param use_predefined:
    :return:
    """
    card_type = random.choice(['visa', 'mastercard'])
    if card_type == 'visa':
        card_number = f"{str(4)}{str(generate_phone_number(15))}"
        card_expiry = generate_random_card_expiry(from_year=2022, to_year=2029)
        card_type = "Visa"
        card_cvv = str(generate_phone_number(3))
        zip_code = str(generate_phone_number(6))
    else:
        card_number = f"{str(5)}{str(generate_phone_number(15))}"
        card_expiry = generate_random_card_expiry(from_year=2022, to_year=2029)
        card_type = "Mastercard"
        card_cvv = str(generate_phone_number(3))
        zip_code = str(generate_phone_number(6))

    if use_predefined:
        return {
            "NameOnCard": name,
            "CardNo": '5122333344445555',
            "CreditCardType": 'Mastercard',
            "ExpiryDate": card_expiry,
            "CVV": card_cvv,
            "ZipCode": zip_code
        }
    else:
        return {
            "NameOnCard": name,
            "CardNo": card_number,
            "CreditCardType": card_type,
            "ExpiryDate": card_expiry,
            "CVV": card_cvv,
            "ZipCode": zip_code
        }


def generate_random_alpha_numeric_string(length=10):
    """
    Function to generate Random Alpha Numeric String
    :param length:
    :return:
    """
    random_alpha_numeric_string = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(length))
    return random_alpha_numeric_string


def generate_random_string(length=10):
    """
    Function to generate Random String
    :param length:
    :return:
    """
    random_string = ''.join(random.choice(string.ascii_uppercase) for _ in range(length))
    return random_string


def generate_guid():
    """
    Function to generate GUID
    :return:
    """
    guid = str(uuid.uuid4())
    return guid


def e2e_guid():
    """
    Function to generate GUID
    :return:
    """
    guid = str(uuid.uuid4())
    return f"e2e-{guid}"


def generate_random_number(low=0, high=99, include_all=False):
    """
    Function to generate a Random Number between low and high range
    :param low: Lowest Random Number
    :param high: Highest Random Number
    :param include_all: Highest Random Number
    :return: A Random number between Low and High
    """
    if include_all:
        number = random.randint(low, high)
    else:
        number = random.randrange(low, high)
    return number


def generate_document_number():
    """
    Function to generate Passport number
    :return:
    """
    char_1 = generate_random_string(1)
    char_2 = generate_random_string(1)
    char_3 = str(generate_random_number())
    char_4 = str(generate_random_number())
    char_5 = generate_random_string(1)
    char_6 = str(generate_random_number())
    passport_number = char_1 + char_2 + char_3 + char_4 + char_5 + char_6
    return passport_number


def generate_birth_date(member='adult'):
    """
    Function to generate Birth Date
    :param member:
    :return:
    """
    child_range = list(range(5, 18))  # 5 to 18 years of children
    minor_range = list(range(1, 5))  # 1 to 5 years of minor
    adult_range = list(range(19, 39))  # 19 years to 39 years of adults
    year_today = int(datetime.now().year)

    if member == 'child':
        start = year_today - child_range[-1]
        end = year_today - child_range[0]
    elif member == 'minor':
        start = year_today - minor_range[-1]
        end = year_today - minor_range[0]
    else:
        start = year_today - adult_range[-1]
        end = year_today - adult_range[0]

    return date(random.randint(start, end), random.randint(1, 12), random.randint(1, 28))


def generate_phone_number(max_digits=10):
    """
    Function to generate phone number
    :param max_digits:
    :return:
    """
    return random.randint(10 ** (max_digits - 1), 10 ** max_digits - 1)


def generate_profile_id(from_saved=False):
    """
    Function to generate Profile Photo ID's
    :return:
    """
    saved_ids = [
        '4a513aac-8d8f-40b3-913a-ba957b377180', 'c5e2de34-d698-4749-95c4-82f70908d2a1',
        '14267b29-41c1-466b-896f-98a225283244', '2e7a8a0c-a5f5-4fae-b368-9b32abbe713e',
        'cc97365f-33f5-483a-b125-99961c01b7dd', 'a931a781-a984-4963-818f-6cdeb2b61be8',
        'caf141a4-c8e9-4acb-849e-474488bf6518', '9b386d13-f3ab-459d-aa10-acb3779776b6',
        'f9a65a99-8c7f-479b-99e7-82ea2da1181a', '4cb9a1a2-5626-464b-97c0-e5bc63feba8a',
        'a97d5f53-f56d-469c-b0c3-17870915b66c', '4f732bc9-4dda-43b5-b37f-25f0cba66247',
        '4f732bc9-4dda-43b5-b37f-25f0cba66247', '4f732bc9-4dda-43b5-b37f-25f0cba66247',
        'f9a65a99-8c7f-479b-99e7-82ea2da1181a', '9b386d13-f3ab-459d-aa10-acb3779776b6',
        '91bd6bba-2d3e-4053-9a7c-100364f0ba08'
    ]
    if from_saved:
        return random.choice(saved_ids)
    else:
        return generate_guid()


def generate_security_id(from_saved=False):
    """
    Function to generate Profile Photo ID's
    :return:
    """
    saved_ids = [
        '4a513aac-8d8f-40b3-913a-ba957b377180', 'c5e2de34-d698-4749-95c4-82f70908d2a1',
        '14267b29-41c1-466b-896f-98a225283244', '2e7a8a0c-a5f5-4fae-b368-9b32abbe713e',
        'cc97365f-33f5-483a-b125-99961c01b7dd', 'a931a781-a984-4963-818f-6cdeb2b61be8',
        'caf141a4-c8e9-4acb-849e-474488bf6518', '14267b29-41c1-466b-896f-98a225283244',
        '04d8ae48-0b51-4a5a-a08e-fcb9da398b99', 'a8a19412-8941-42f5-9526-3021dd13e3fd',
        '968af4fc-bdd7-4461-b664-967dbfab7375', 'de60f6d0-4dd1-48c0-81f9-53641169c125'
    ]
    if from_saved:
        return random.choice(saved_ids)
    else:
        return generate_guid()


def generate_profile_photo_ids(from_saved=False):
    """
    Function to generate Profile Photo ID's
    :return:
    """
    saved_ids = [
        '4a513aac-8d8f-40b3-913a-ba957b377180', 'c5e2de34-d698-4749-95c4-82f70908d2a1',
        '14267b29-41c1-466b-896f-98a225283244', '2e7a8a0c-a5f5-4fae-b368-9b32abbe713e',
        'cc97365f-33f5-483a-b125-99961c01b7dd', 'a931a781-a984-4963-818f-6cdeb2b61be8',
        'caf141a4-c8e9-4acb-849e-474488bf6518', '9b386d13-f3ab-459d-aa10-acb3779776b6',
        'f9a65a99-8c7f-479b-99e7-82ea2da1181a', '4cb9a1a2-5626-464b-97c0-e5bc63feba8a',
        'a97d5f53-f56d-469c-b0c3-17870915b66c', '4f732bc9-4dda-43b5-b37f-25f0cba66247',
        '4f732bc9-4dda-43b5-b37f-25f0cba66247', '4f732bc9-4dda-43b5-b37f-25f0cba66247',
        'f9a65a99-8c7f-479b-99e7-82ea2da1181a', '9b386d13-f3ab-459d-aa10-acb3779776b6',
        '91bd6bba-2d3e-4053-9a7c-100364f0ba08'
    ]
    if from_saved:
        return random.choice(saved_ids)
    else:
        return generate_guid()


def generate_first_name(from_saved=False):
    """
    Function to generate First Name
    :return:
    """
    saved_ids = ['Carol', 'Caroline', 'Carolyn', 'Deirdre', 'Chloe', 'Claire', 'Deirdre', 'Diana', 'Donna', 'Dorothy',
                 'Elizabeth', 'Zoe', 'Wendy', 'Wanda', 'Virginia', 'Victoria', 'Vanessa', 'Una', 'Tracey', 'Theresa',
                 'Sue', 'Stephanie', 'Sophie', 'Sonia', 'Sarah', 'Samantha', 'Sally', 'Ruth']
    if from_saved:
        return random.choice(saved_ids)
    else:
        return str(Faker().first_name())


def generate_last_name(from_saved=False):
    """
    Function to generate Last Name
    :return:
    """
    saved_ids = ['Simon', 'Stephen', 'Steven', 'Stewart', 'Thomas', 'Tim', 'Trevor', 'Victor', 'Warren', 'William',
                 'Alan', 'Elliott', 'Victor', 'Bryce', 'Finn', 'Brantley', 'Edward', 'Abraham', 'Sebastian', 'Sean',
                 'Sam', 'Robert', 'Richard', 'Piers', 'Phil', 'Peter', 'Paul', 'Owen']
    if from_saved:
        return random.choice(saved_ids)
    else:
        return str(Faker().last_name())


def generate_email_id(**kwargs):
    """
    Function to generate Email ID
    :return:
    """
    tail = f"_{str(generate_phone_number(6))}@mailinator.com"
    email_id = re.sub(r'(.*?)@.*', r'\1{}'.format(tail), str(Faker().email()), re.I | re.M)
    return str(email_id).lower()


def generate_security_photo_ids(from_saved=False):
    """
    Function to generate Security Photo ID's
    :return:
    """
    saved_ids = [
        '4a513aac-8d8f-40b3-913a-ba957b377180', 'c5e2de34-d698-4749-95c4-82f70908d2a1',
        '14267b29-41c1-466b-896f-98a225283244', '2e7a8a0c-a5f5-4fae-b368-9b32abbe713e',
        'cc97365f-33f5-483a-b125-99961c01b7dd', 'a931a781-a984-4963-818f-6cdeb2b61be8',
        'caf141a4-c8e9-4acb-849e-474488bf6518', '14267b29-41c1-466b-896f-98a225283244',
        '04d8ae48-0b51-4a5a-a08e-fcb9da398b99', 'a8a19412-8941-42f5-9526-3021dd13e3fd',
        '968af4fc-bdd7-4461-b664-967dbfab7375', 'de60f6d0-4dd1-48c0-81f9-53641169c125'
    ]
    if from_saved:
        return random.choice(saved_ids)
    else:
        return generate_guid()


def generate_document_ids(from_saved=False):
    """
    Function to generate Document ID's
    :return:
    """
    saved_ids = [
        'cff09505-dc9b-4be7-bc4e-f6d101a09268', '4a513aac-8d8f-40b3-913a-ba957b377180',
        '4a513aac-8d8f-40b3-913a-ba957b377180', '294c318b-e86b-4ee9-a71a-3dc564d56183',
        '4c1d1748-c22b-47c9-a9c6-c3d906ccbd23', 'b17881da-a198-4624-9ade-7b0f3a6e9633',
        '4afce7d7-b618-49cb-8468-069ae12f2d47', 'e2cb6431-3811-46c9-a9e9-5a6244c174f8',
        'd0588969-0d9d-4525-8dc8-107591a158b1', '2fbe5504-ffdb-43b1-b804-1621e8ed25ab',
        'e6cc5a15-3f56-4426-a3b4-ae55b17e6f0c'
    ]
    if from_saved:
        return random.choice(saved_ids)
    else:
        return generate_guid()


def generate_menu_item_ids(from_saved=False):
    """
    Function to generate Menu Item ID's
    :return:
    """
    saved_ids = [
        '0e6b6f4a-0a83-432d-96e1-2f2ffd1b9c01', '2391e32b-e2c5-4745-8203-ac3c91e6a2d7',
        '27304b38-75ce-4ebc-98a3-836079bd2ef3', '4ec8ee29-632e-4dcc-b2af-492967f1b2f0',
        '5ae80def-dbc6-419c-884f-4d2aa7d24387', '6a4f6a35-0996-4614-84b7-67ed6f32b572',
        '7d7a2a92-aec3-4444-b965-c4c1c4e632f5', '831d0313-6cee-40ca-af8e-5189af1a567a',
        '914fc937-3af9-409e-9159-9f87ec8cfdbd'
    ]
    if from_saved:
        return random.choice(saved_ids)
    else:
        return generate_guid()


def parse_html(content):
    soup = BeautifulSoup(content, features="lxml")
    return soup


def compress_file(file_name):
    """
    Compress a file and return it's path
    :param file_name
    """
    if str(file_name).endswith('zip'):
        return file_name
    else:
        zip_name = ".".join([*[x for x in file_name.split('.')[0:-1]], *['zip']])

    _zip = zipfile.ZipFile(zip_name, 'w')
    _zip.write(file_name, compress_type=zipfile.ZIP_DEFLATED)
    _zip.close()

    return zip_name


def get_base_auth(username, password):
    """
    Return Base64 encoded authorization
    :param username:
    :param password:
    :return:
    """
    auth = str(base64.b64encode(bytes('%s:%s' % (username, password), 'utf-8')), 'ascii').strip()
    return {'Authorization': f'Basic {auth}'}


def generate_curl_command(method, headers, url, params=None, data=None):
    """
    Get Curl Command Before it is being Hit !!
    :param method:
    :param headers:
    :param url:
    :param params:
    :param data:
    :return:
    """
    # Check if data is in Json/dict format, convert it into string after that
    if isinstance(data, dict):
        data = json.dumps(data)
    headers = " -H ".join([f'"{k}: {v}"' for k, v in headers.items() if k not in ['User-Agent', 'Accept-Encoding']])

    if params:
        url = f'{url}?{"&".join([f"{k}={v}" for k, v in params.items()])}'

    if data:
        command = f"curl -i -sS -X {method} -H {headers} -d '{data}' '{url}'"
    else:
        command = f"curl -i -sS -X {method} -H {headers} '{url}'"

    # We always save Curl Command in environment variable, so that we know (in-case) of an exception what was it.
    os.environ["CURL"] = command
    return command


def get_correct_path(ref_path):
    """
    Get Correct Folder Path (running as standalone or from pytest)
    :param ref_path:
    :return:
    """
    root = os.getenv("ROOT_PATH", None)
    if root:
        _name = Path(root) / ref_path
    else:
        _name = Path(ref_path)

    if not os.path.exists(ref_path):
        os.makedirs(_name)

    return str(_name.resolve())


def get_all_keys_in_dict(dictionary):
    """
    Get all Keys available in dictionary (even nested ones)
    :param dictionary:
    :return:
    """
    final_list = []
    for k, v in dictionary.items():
        final_list.append(k)
        if isinstance(v, dict):
            final_list.extend(get_all_keys_in_dict(v))
        if isinstance(v, list):
            for _v in v:
                if isinstance(_v, dict):
                    final_list.extend(get_all_keys_in_dict(_v))
    return sorted(list(set(final_list)))


def urlbase(url):
    """
    This function will Return base of a URL removing any endpoint from it
    :param url:
    :return:
    """
    parsed = list(urlparse(url))
    parsed[2] = ''
    return str(urlunparse(parsed))


def urljoin(*args):
    """
    This function will join a URL and return back proper url
    :return:
    """
    parsed = list(urlparse('/'.join(args)))
    parsed[2] = re.sub("/{2,}", "/", parsed[2])
    _host = urlunparse(parsed)
    return _host


def convert_iso_to_epoch(date_time, format_string="%s.%f"):
    """
    Convert ISO Date format '2019-09-06T06:50:58.533278856Z' in EPOCH Time
    :param date_time:
    :param format_string:
    :return:
    """
    return parse(date_time).strftime(format_string)


def modify_birth_date(birth_date):
    """
    This function will modify birth date into passport birth date which is required for MRZ
    :param birth_date:
    :return:
    """
    split_up = str(birth_date).split("-")
    year = "".join(split_up[0][2::])
    month = split_up[1]
    day = split_up[2]
    return "".join([year, month, day])


def generate_random_password(length=10):
    """
    Generate a Random Password of length characters
    :param length:
    :return:
    """
    alphabet = string.ascii_letters + string.digits
    password = ''.join(secrets.choice(alphabet) for _ in range(length))
    return password


def generate_document_expiry(from_year=2021, to_year=2031):
    """
    Function to generate passport expiry
    :param from_year:
    :param to_year:
    :return:
    """
    return date(random.randint(from_year, to_year), random.randint(1, 12), random.randint(1, 28))


def is_key_there_in_dict(key, dictionary, empty_check=True, text=None):
    """
    Check if key is there in dictionary
    :param key:
    :param dictionary:
    :param empty_check:
    :param text:
    :return:
    """
    if key not in dictionary:
        if text is None:
            logger.error(f"'{key}' not found in _content !!")
            raise Exception(f"'{key}' not found in _content !!")
        else:
            logger.error(f"'{key}' not found in _content | {text}")
            raise Exception(f"'{key}' not found in _content | {text}")
    else:
        if empty_check:
            if isinstance(dictionary[key], (list, tuple, dict)):
                if len(dictionary[key]) == 0:
                    logger.debug(f"{key} is empty !!")
            elif dictionary[key] is None:
                pass
            else:
                pass


def find_key_in_dict(key, dictionary):
    """
    Function that will find key in dictionary
    :param key: Key that has to be found
    :param dictionary: dictionary to be searched
    :return:
    """

    for k, v in dictionary.items():
        if k == key:
            yield v
        elif isinstance(v, dict):
            for result in find_key_in_dict(key, v):
                yield result
        elif isinstance(v, list):
            for d in v:
                if isinstance(d, dict):
                    for result in find_key_in_dict(key, d):
                        yield result


def retry_when_fails(method):
    """
    Decorator function to retry a function for 600 seconds (10*60) !!
    The interval also increases by 5 each iteration by iteration
    :param method:
    :return:
    """

    @wraps(method)
    def inner(self, *method_args, **method_kwargs):
        """
        Inner Function to perform a retry
        :param self:
        :param method_args:
        :param method_kwargs:
        :return:
        """
        retries = 60
        sleep = 10
        final_exception = None
        while retries > 0:
            try:
                response = method(self, *method_args, **method_kwargs)
            except Exception as exp:
                logger.info(f"{retries} of 60 Retries Left !!")
                retries -= 1
                time.sleep(sleep)
                final_exception = exp
                pass
            else:
                break
        else:
            logger.error(final_exception)
            raise Exception(f"Retries Exhausted {final_exception} Curl: {os.environ.get('CURL', 'Not-Captured')}!!")
        return response

    return inner


def check_response_time(max_time=6000):
    """
    Check Response Time Function
    :param max_time:
    :return:
    """

    def decorator_check_response_time(function):
        """
        Check Response Time Decorator
        :param function:
        :return:
        """

        def wrapper_check_response_time(*args, **kwargs):
            """
            Check Response Time Wrapper
            :param args:
            :param kwargs:
            :return:
            """
            elapsed = int(args[0].elapsed.microseconds / 100)
            if elapsed > max_time:
                logger.debug(f"Time taken to run Query: {elapsed} > {max_time}")
                assert False, f"Time taken to run Query: {elapsed} > {max_time}"
            return function(*args, **kwargs)

        return wrapper_check_response_time

    return decorator_check_response_time


@check_response_time(max_time=10000)
def process_response(response):
    """
    Function that will process response of a Rest Call
    :param response:
    :return:
    """
    # Saving all URL's that we are using in E2E (for reference)
    global URLs
    URLs.add(response.request.url)
    with open('urlPaths.json', 'w') as _fp:
        _fp.write(json.dumps(sorted(list(URLs)), indent=2, sort_keys=True))

    to_return = ['method', 'url', 'status_code', 'reason', 'headers', 'content', 'ok', 'elapsed']
    RestResponse = namedtuple('RestResponse', to_return)

    headers = dict()
    for k, v in response.headers.items():
        headers[k] = v

    # Publish Rest Response Content
    try:
        content = response.content
        content = content.decode('utf-8')
    except UnicodeDecodeError:
        content = response.content
        content = content.decode('latin-1')

    try:
        content = json.loads(content)
    except json.JSONDecodeError:
        try:
            content = response.content.decode('utf-8')
        except (UnicodeDecodeError, AttributeError):
            content = response.content

    if content is None:
        raise Exception("ERROR Content is None !! Please Check")

    processed = RestResponse(
        method=response.request.method,
        url=response.request.url,
        status_code=response.status_code,
        reason=response.reason,
        headers=headers,
        content=content,
        ok=response.ok,
        elapsed=response.elapsed.microseconds
    )

    # Attach Response to allure in Json Format
    now = datetime.now(timezone('Asia/Calcutta'))
    if not response.ok:
        name = f"FailResponse_{now.minute}{now.second}{now.microsecond}.json"
    else:
        name = f"PassResponse_{now.minute}{now.second}{now.microsecond}.json"

    try:
        content = json.dumps(content, indent=2, sort_keys=True)
        allure.attach(content, name=name, attachment_type=allure.attachment_type.JSON)
    except (json.JSONDecodeError, TypeError):
        pass

    if not response.ok:
        # Not sending Header in Error Response
        logger.error(processed)
        raise Exception(json.dumps(processed, sort_keys=True))

    return processed


def send_post_request(url, data, headers, params=None):
    """
    Send simple Post Request
    :param url:
    :param data:
    :param headers:
    :param params:
    :return:
    """
    response = requests.request("POST", url, data=data, headers=headers, params=params, verify=False)
    return process_response(response)


def send_get_request(url, headers, params=None, timeout=None):
    """
    Send simple Post Request
    :param url:
    :param headers:
    :param params:
    :param timeout:
    :return:
    """
    if timeout:
        response = requests.request("GET", url, headers=headers, params=params, timeout=timeout, verify=False)
    else:
        response = requests.request("GET", url, headers=headers, params=params, verify=False)

    return process_response(response)


def convert_country_code(code, length=3):
    """
    This function will convert a given country code into given length, returns back name if length > 3
    :param code:
    :param length:
    :return:
    """
    for _country in list(countries):
        if len(code) == 2:
            if _country.alpha_2 == code:
                if length == 2:
                    return _country.alpha_2
                if length == 3:
                    return _country.alpha_3
                if length >= 3:
                    return _country.name
        elif len(code) == 3:
            if _country.alpha_3 == code:
                if length == 2:
                    return _country.alpha_2
                if length == 3:
                    return _country.alpha_3
                if length >= 3:
                    return _country.name
        else:
            if _country.name == code:
                if length == 2:
                    return _country.alpha_2
                if length == 3:
                    return _country.alpha_3
                if length >= 3:
                    return _country.name


def generate_country_code(country='United States', length=3):
    """
    Generate Country Code (Randomly)
    :param country:
    :param length:
    :return:
    """
    if str(country).upper() == "RANDOM":
        _country = random.choice(list(countries))
        if length == 2:
            return _country.alpha_2
        elif length == 3:
            return _country.alpha_3
        else:
            raise Exception(f"Length can be either 2 or 3 and not {length}")
    else:
        for _country in list(countries):
            if str(_country.name).upper() == str(country).upper():
                if length == 2:
                    return _country.alpha_2
                elif length == 3:
                    return _country.alpha_3
                else:
                    raise Exception(f"Length can be either 2 or 3 and not {length}")


def get_version(content):
    """
    Function to process the response and return back version
    :param content:
    :return:
    """
    if isinstance(content, dict):
        if 'version' in content:
            version = str(content['version']).strip().replace("â€™", "").replace('â€˜', "")
            version = version.split(' ')[0]
            return version
        elif 'Version' in content:
            version = str(content['Version']).strip().replace("â€™", "").replace('â€˜', "")
            version = version.split(' ')[0]
            return version
        else:
            raise Exception(f"Format Not Supported: {content}")
    else:
        if len(str(content).split('\n')) == 1:
            version = str(content).replace("â€™", "").replace('â€˜', "")
            version = version.split(' ')[0]
            return version
        else:
            raise Exception(f"Format Not Supported: {content}")


def process_rest_response(response):
    """
    Function to process the failed Rest response
    :param response:
    :return:
    """
    search = re.search(r'^Exception: (.*)', response, re.I | re.M)
    if search:
        data = None
        try:
            data = json.loads(search.group(1))
        except json.JSONDecodeError:
            pass

        if data is not None and isinstance(data, list):
            if data[0] in ['GET', 'POST', 'PUT']:
                return {
                    'method': data[0],
                    'url': data[1],
                    'status_code': data[2],
                    'reason': data[3],
                    'microseconds': data[4],
                    'headers': data[5],
                    'content': data[6],
                    'ok': data[7]
                }['reason']
            else:
                return data
        else:
            return search.group(1)
    return response


def calculate_off_set(embark_date_time, ship_time):
    """
    Calculate Offset depending upon Sailing Date
    :param embark_date_time:
    :param ship_time:
    :return:
    """
    embark_date = datetime.strptime(embark_date_time, "%Y-%m-%dT%H:%M:%S")
    offset = relativedelta(embark_date.replace(tzinfo=pytz.UTC), ship_time.replace(tzinfo=pytz.UTC))
    offset = (offset.days * 24 * 60) + (offset.hours * 60) + offset.minutes
    return offset


def convert_embark_date_time(embark_date, tz="EST"):
    """
    Convert Ship Embark Date to Date Time Format
    :param embark_date:
    :param tz:
    :return:
    """
    local_tz = timezone(tz)
    _date = datetime.strptime(embark_date, "%Y-%m-%dT%H:%M:%S").replace(tzinfo=pytz.utc).astimezone(local_tz)
    return _date


def process_token(content):
    """
    Process Token Response
    :param content: Response Content
    :return:
    """
    # Get token type from content
    if 'token_type' in content:
        _type = content['token_type']
    elif 'tokenType' in content:
        _type = content['tokenType']
    elif 'tokendetails' in content:
        if 'token_type' in content['tokendetails']:
            _type = content['tokendetails']['token_type']
        elif 'tokenType' in content['tokendetails']:
            _type = content['tokendetails']['tokenType']
        else:
            _type = None
    else:
        _type = None

    # Get token from content
    if 'access_token' in content:
        _token = content['access_token']
    elif 'accessToken' in content:
        _token = content['accessToken']
    elif 'tokendetails' in content:
        if 'access_token' in content['tokendetails']:
            _token = content['tokendetails']['access_token']
        elif 'accessToken' in content['tokendetails']:
            _token = content['tokendetails']['accessToken']
        else:
            _token = None
    else:
        _token = None

    if 'clientToken' in content:
        bearer_token = content['clientToken']
    elif _type is None or _token is None:
        raise Exception("ERROR: Cannot find Bearer _token or _type")
    else:
        bearer_token = f"{_type} {_token}"

    return bearer_token


def change_merged_services_link(url):
    """
    Change URL as per Services Merge
    :param url
    """
    # For Slack etc we will always skip and don't convert the url
    always_skip = ['bitbucket.org', 'slack.com', 'testrail.io', 'atlassian.net']
    for _skip in always_skip:
        if _skip in url:
            return url

    # For some components this has not been merged, so skipping them off
    for _not_merged in ['dc']:  # TODO @sarvesh remove when we have dxpcore in virgin
        if _not_merged in urlsplit(url).hostname:
            return url

    for _service in link_mapper.keys():
        if _service in url:
            _url = re.sub(r"(.*?)(/{})(.*)".format(_service), r"\1/{}\3".format(link_mapper[_service]), url)
            return _url

    return url


def base64_encode(username, password):
    """
    Return Base64 encoded string
    :param username:
    :param password:
    :return:
    """
    encoded = str(base64.b64encode(bytes(f'{username}:{password}', 'utf-8')), 'ascii').strip()
    return encoded


def base64_decode(data):
    """
    Return Base64 encoded string
    :return:
    """
    decoded = str(base64.b64decode(data), 'ascii')
    if len(decoded.split(":")) != 2:
        raise Exception("This is not a valid username password encoded string !!")
    username = decoded.split(":")[0]
    password = decoded.split(":")[1]
    return username, password


def get_test_rail_config(configs, env, platform):
    """
    Get Test Rail Configurations
    :param configs:
    :param env:
    :param platform:
    :return:
    """
    final_configs = []
    for _config in configs:
        if _config['name'] == 'DXP' and platform == "DXP":
            for _env in _config['configs']:
                _name = str(_env['name']).upper()
                if _name == 'PROD' and env == 'DC':
                    final_configs.append(_env['id'])
                elif _name == 'QA1' and env == 'QA1':
                    final_configs.append(_env['id'])
                elif _name == 'DEV' and env == 'DEV':
                    final_configs.append(_env['id'])
                elif env not in ['DC', 'QA1', 'DEV'] and _name == 'DEVELOPER':
                    final_configs.append(_env['id'])
                else:
                    continue
        elif _config['name'] == 'Virgin Voyages' and platform == "VIRGIN":
            for _env in _config['configs']:
                _name = str(_env['name']).upper()
                if _name == 'CERT' and env == 'QA':
                    final_configs.append(_env['id'])
                elif _name == 'DEV' and env == 'DEV':
                    final_configs.append(_env['id'])
                elif _name == 'INT' and env == 'INTEGRATION':
                    final_configs.append(_env['id'])
                elif _name == 'STAGE' and env == 'STAGE':
                    final_configs.append(_env['id'])
                else:
                    continue
        else:
            continue
    return final_configs


def generate_test_plan(test_rails, config, items):
    """
    Generate Test Plan in Test Rails
    :param test_rails
    :param config
    :param items
    """
    cases = []
    env = str(config.env).upper()
    platform = str(config.platform).upper()
    comp_cases = dict()
    to_ignore = {'testrail', 'run', 'skip', 'xfail', 'SkipForNonDevEnv', 'SkipForDCEnv', 'SkipForDevEnv', 'SHIP',
                 platform}
    # Sort Test-Cases Based on Platform and Markers
    for item in items:
        markers = [*item.cls.pytestmark, *item.own_markers]
        names = {x.name for x in markers}
        if 'testrail' in names and 'skip' not in names:
            _id = [getattr(x, 'kwargs')['ids'][0] for x in markers if getattr(x, 'name') == 'testrail'][0]
            cases.append(int(_id))
            _comp = list(names - to_ignore)
            if len(_comp) != 1:
                raise Exception(f"Additional Marker {_comp} in Test-Case !!")
            else:
                comp = _comp[0]
                if comp not in comp_cases:
                    comp_cases[comp] = {_id}
                else:
                    comp_cases[comp].add(_id)
    name = f'{config.platform} {env} API Test Plan {datetime.now().strftime("%Y-%m-%d %H:%M:%S")}'

    # Get Test Rail Configs
    configs = get_test_rail_config(test_rails.get_configs(project_id=1), env, platform)

    # Create Test Plan and Test Plan's Data
    test_rail_data = test_rails.create_test_plan(project_id=1, name=name, cases=cases, configs=configs)
    plan_id = int(list({x['plan_id'] for x in test_rail_data})[0])
    logger.debug(f"Test Plan: {plan_id}")

    data = {
        'test_plan': f"https://decurtis.testrail.io/index.php?/plans/view/{plan_id}",
        'testRailData': test_rail_data
    }

    return data


def compare_ship_shore_data(shore_data, ship_data, no_check=None):
    """
    Function to Compare all keys in Ship and Shore Data
    :param shore_data:
    :param ship_data:
    :param no_check:
    :return:
    """
    if no_check is None:
        no_check = set()
    _ship_keys = set(get_all_keys_in_dict(OrderedDict(ship_data)))
    _shore_keys = set(get_all_keys_in_dict(OrderedDict(shore_data)))
    if len(_ship_keys) != len(_shore_keys):
        _ship_extra = _ship_keys - (_ship_keys & _shore_keys)
        _shore_extra = _shore_keys - (_ship_keys & _shore_keys)
        if len(_ship_extra) > 0 and len(_ship_extra - no_check) > 0:
            raise Exception(f"ERROR: {list(_ship_extra - no_check)} Key(s) not present in Shore")
        if len(_shore_extra) > 0 and len(_shore_extra - no_check) > 0:
            raise Exception(f"ERROR: {list(_shore_extra - no_check)} Key(s) not present in Ship")


def compare_kafka_topics(configurations, topics):
    """
    Compare Kafka Topics available in Configurations
    :param configurations
    :param topics
    """
    not_found = []
    for topic in topics:
        for config in configurations:
            if topic in [x['Value'] for x in configurations[config] if 'Value' in x]:
                break
        else:
            not_found.append(topic)

    return not_found


def uncurl_from_curl(command):
    """
    Uncurl the curl command and return args for request command
    """
    if re.search(r"--request", command):
        url_with_params = str(command.split(" ")[4]).replace("'", "").split('?')
        method = str(re.search(r'--request\s+(.*?)\s+', command, re.I | re.M).group(1)).upper()
        if re.search(r"--data-raw\s*'([^']*)'", command) is not None:
            data = json.loads(re.search(r"--data-raw\s*'([^']*)'", command, re.I | re.M).group(1))
        else:
            data = None
        headers = {str(x.split(':')[0]).strip(): str(x.split(':')[1]).strip() for x in
                   re.findall(r'--header \'(.*?)\'', command, re.I | re.M)}
    else:
        url_with_params = str(command.split(" ")[-1]).replace("'", "").split('?')
        method = str(re.search(r'-x\s+(.*?)\s+', command, re.I | re.M).group(1)).upper()
        if re.search(r"-d\s*'(.*?)'", command) is not None:
            data = json.loads(re.search(r"-d\s*'(.*?)'", command, re.I | re.M).group(1))
        else:
            data = None
        headers = {str(x.split(':')[0]).strip(): str(x.split(':')[1]).strip() for x in
                   re.findall(r'-H \"(.*?)\"', command, re.I | re.M)}
    url = url_with_params[0]
    if len(url_with_params) > 1:
        params = url_with_params[1]
    else:
        params = None

    return {
        'method': method,
        'url': url,
        'data': data,
        'params': params,
        'headers': headers,
    }


def read_validate_json():
    """
    This function read all .json file in verification data and return json in format of this{file_name: file_Json} .
    :return:
    """
    _json_files = []
    directory = 'test_data/verification_data'
    for root, dirs, file in os.walk(directory):
        for filename in file:
            if filename.endswith('.json'):
                _json_files.append(os.path.join(root, filename))

    if len(_json_files) != 0:
        final_data = {}
        for _file in _json_files:
            with open(_file, 'r') as _fp:
                _file_content = json.load(_fp)
            final_data.update({os.path.basename(_file).split('.json')[0]: _file_content})
        json_files = namedtuple('MyTuple', final_data)
        files = json_files(**final_data)
        return files
    else:
        raise Exception("There's no Json File in {directory} !!")


def get_job_name_for_slack():
    """
    Return Jenkins Job Name to be sent with Slack Message,
    This is fetched from Environment Variables. Which means is only available while running in Jenkins
    :return:
    """
    url = os.environ.get('BUILD_URL', None)
    name = os.environ.get('JOB_NAME', None)
    number = os.environ.get('BUILD_NUMBER', None)
    if url and name and number:
        return f"<{url}consoleFull|{name} #{number}>"
    else:
        return None


def validate_url_via_dxp_core(url, database, rest):
    """
    Validate URL's accessibility via dxp core
    :param url
    :param database
    :param rest
    """
    failed = {
        "INTERNAL URL": dict(),
        "EXTERNAL URL": dict(),
        "EXPOSED URL": dict()
    }
    _query = "select * from public.ufngetsettingvalueforverification();"
    rows = database.run_and_fetch_data(query=_query)

    url = urljoin(url, 'dxpcore/validateurl')
    for row in rows:
        key = row['yamlkey']
        value = row['value']
        category = row['category']
        if 'VALUE_NOT_AVAILABLE' in value or not str(value).startswith('http'):
            continue

        params = {"url": value}
        passed = True
        try:
            _content = rest.send_request(method="GET", url=url, auth="bearer", params=params).content
            if _content['rc'] in ['200', '400', '401']:
                continue
            else:
                passed = False
        except (Exception, ValueError):
            passed = False
            pass

        if not passed:
            failed[category][key] = {
                'consumerdetail': row['consumerdetail'] if 'consumerdetail' in row else None,
                'rc': _content['rc'] if _content and 'rc' in _content else None,
                'value': value,
            }

    return failed


def read_json_file(file_name, nt=True):
    """
    This function will read Json and return it back in Named-Tuples format
    :param file_name
    :param nt
    """
    if not os.path.isfile(file_name):
        raise Exception(f"File {file_name} Does Not Exist !!")

    with open(file_name, 'r') as _fp:
        if nt:
            data = json.load(_fp, object_hook=lambda d: Namespace(**d))
        else:
            data = json.load(_fp)

    return data


def get_environment_data(ship, shore):
    """
    Generate Environment Data, such as Platform, environment, and ship-code
    :param ship
    :param shore
    """
    _ship_codes = {
        "sagar": "DX",
    }

    code = None
    env = None
    platform = "DXP"
    for _data in re.finditer(r'(\w+)', shore, re.I | re.M):
        platform = "DXP"
        env = "-".join(re.search(r'https://(.*?)\.', ship, re.I | re.M).group(1).split('-')[0:-1])
        code = _ship_codes[re.search(r'https://(.*?)\.', ship, re.I | re.M).group(1).split('-')[-1]]

    if env not in ['qa1', 'dev', 'dc']:
        env_masked = 'dev'
    else:
        env_masked = env

    if code not in _ship_codes.values():
        raise Exception(f"ERROR: Ship-Code: {code} is not a valid Ship Code | {_ship_codes.values()}")

    return platform, env, code, env_masked


def create_auth_basic_token(login, secret):
    """
    Function to create basic token from id and secret
    :param login:
    :param secret:
    """
    login = str(base64.b64decode(login), 'ascii').strip()
    secret = str(base64.b64decode(secret), 'ascii').strip()
    token = str(base64.b64encode(bytes('%s:%s' % (login, secret), 'utf-8')), 'ascii').strip()
    return token


def save_allure(data, name):
    """
    Save allure report by converting data to Json
    :param data: 
    :param name: 
    :type name: 
    :return: 
    """
    dump = json.dumps(data, indent=2, sort_keys=True)
    allure.attach(dump, name=name, attachment_type=allure.attachment_type.JSON)
    with open(name, 'w') as _fp:
        _fp.write(dump)


def connect_qa_db():
    """
    Create Connection with QA DB
    """

    creds = read_creds_file()
    details = creds.qa_postgres
    data = Database(
        host=details.host,
        username=details.username,
        password=details.password,
        database=details.database,
        port=5432
    )
    return data


def read_creds_file():
    """
    Function to Read Creds File
    """
    data = DecurtisEncryption().decrypt_file(
        os.path.join(os.path.dirname(os.path.realpath(__file__)), 'credentials/creds'))
    return json.loads(data, object_hook=lambda d: Namespace(**d))


def app_secret_read_yaml(app_secrets):
    """
    Read and Process App Secrets File
    :param app_secrets:
    :return:
    """
    data = []
    for line in app_secrets.split('\n'):
        if re.search(r'{{.*?}}', line, re.I | re.M):
            continue
        data.append(str(line).rstrip())

    data = "\n".join(data)
    data = yaml.full_load(data)
    return data['data']
