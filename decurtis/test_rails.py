from decurtis.common import *
from decurtis.rest import SendRestRequest
from types import SimpleNamespace as Namespace


class Testrail(SendRestRequest):
    TEST_CASE_URI = "index.php?/api/v2/add_case/{}"
    UPDATE_CASE_URI = "index.php?/api/v2/update_case/{}"
    TEST_SECTION_URI = "index.php?/api/v2/add_section/{}"
    SECTIONS_URI = "index.php?/api/v2/get_sections/{}&suite_id={}"
    TEST_RUN_URI = "index.php?/api/v2/get_tests/{}"
    RESULT_URI = "index.php?/api/v2/add_result_for_case/{}/{}"
    GET_CASES_URI = "index.php?/api/v2/get_cases/{}&suite_id={}&section_id={}"
    GET_CASE_URI = "index.php?/api/v2/get_case/{}"
    GET_PROJECT_CASES_URI = "index.php?/api/v2/get_cases/{}"
    GET_PROJECT_CASES_IN_SUIT_URI = "index.php?/api/v2/get_cases/{}&suite_id={}"
    GET_FILTERED_CASES_IN_SUIT_URI = "index.php?/api/v2/get_cases/{}&suite_id={}&created_after={}&created_before={}"
    ADD_PLAN_ENTRY_URI = "index.php?/api/v2/add_plan_entry/{}"
    DELETE_TEST_RUN_URI = "index.php?/api/v2/delete_run/{}"
    DELETE_TEST_PLAN = "index.php?/api/v2/delete_plan/{}"
    UPDATE_SECTION = "index.php?/api/v2/update_section/{}"
    ADD_TEST_PLAN_URI = "index.php?/api/v2/add_plan/{}"
    GET_CONFIGS_URI = "index.php?/api/v2/get_configs/{}"
    GET_PLAN_URI = "index.php?/api/v2/get_plan/{}"
    GET_PLANS_URI = "index.php?/api/v2/get_plans/{}&limit=250&offset={}"
    GET_TEST_SUITS_URI = "index.php?/api/v2/get_suites/{}"
    GET_CASES_IN_SUIT_URI = "index.php?/api/v2/get_cases/{}&suite_id={}"
    GET_RESULTS_FOR_RUN_URI = "index.php?/api/v2/get_results_for_run/{}"
    ADD_ATTACHMENT_TO_RESULT_URI = "index.php?/api/v2/add_attachment_to_result/{}"
    CLOSE_PLANS_URI = "index.php?/api/v2/close_plan/{}"
    GET_PRIORITIES = "index.php?/api/v2/get_priorities"
    GET_TYPES = "index.php?/api/v2/get_case_types"
    GET_STATUS = "index.php?/api/v2/get_statuses"
    GET_SUITES = "index.php?/api/v2/get_suite/{}"
    GET_USERS = "index.php?/api/v2/get_users"

    TEST_CASE_TEMPLATE = 4  # PCS Test Case Template
    TEST_CASE_DEFAULT_TYPE = 6  # Functional
    TEST_CASE_DEFAULT_PRIORITY = 2  # Medium

    MAPPING_METHOD = {"TESTRAIL_TEST_ID": 1, "AUTOMATION_TEST_TITLE": 2}
    PRIORITY = {"LOW": 1, "MEDIUM": 2, "HIGH": 3, "CRITICAL": 4}
    AUTOMATED = {"NOT_AUTOMATED": 0, "AUTOMATED": 1, "TO_BE_AUTOMATED": 2, "NOT_TO_BE_AUTOMATED": 3}
    TYPE = {"FUNCTIONAL": 6, "PERFORMANCE": 8, "SECURITY": 10, "USABILITY": 12}
    TEST_CYCLE = {"SANITY": 1, "REGRESSION": 2, "SMOKE": 3, "CE": 4}

    def __init__(self, **kwargs):
        """
        :param kwargs:
            url: testrail url
            username: testrail username
            password: testrail password
            project_id: testrail project id
        """
        super().__init__()
        self.url = kwargs['url']
        self.project_id = kwargs.pop('project_id', 1)
        self.username = kwargs.pop('username')
        self.password = kwargs.pop('password')

        auth = str(base64.b64encode(bytes('%s:%s' % (self.username, self.password), 'utf-8')), 'ascii').strip()
        self.session.headers = {
            "Authorization": f"Basic {auth}",
            "Content-Type": "application/json"
        }

    @retry_when_fails
    def send_request(self, **kwargs):
        """
        Get/POST request
        :param kwargs:
        :return:
        """
        return SendRestRequest.send_request(self, **kwargs)

    @staticmethod
    def get_testrail_config_for_module(**kwargs):
        """
        Required when test cases are created in testrail from automation using test title as unique identifier
        mapping_key  : Unique key to map test cases between automation and testrail
        sections     : testrail sections info corresponding to this module
        test_info    : More specific test data from data file for steps & expected result. Docstring in test will be
                       ignored if same key is present here
        """
        testrail_data = {
            "module_name": kwargs["module_name"],
            "mapping_key": kwargs["mapping_key"] if "mapping_key" in kwargs
            else Testrail.MAPPING_METHOD["TESTRAIL_TEST_ID"],
            "sections": kwargs["sections"] if "sections" in kwargs else list(),
            "test_info": kwargs["test_info"] if "test_info" in kwargs else dict()
        }
        return testrail_data

    @staticmethod
    def _format_data(**kwargs):
        """
        Format test rail data
        :param kwargs:
        :return:
        """
        data = {
            'title': kwargs['title'],
            'template_id': Testrail.TEST_CASE_TEMPLATE,
            'type_id': Testrail.TYPE[kwargs.pop('type', Testrail.TEST_CASE_DEFAULT_TYPE).upper()],
            'priority': Testrail.PRIORITY[kwargs.pop('priority', Testrail.TEST_CASE_DEFAULT_PRIORITY).upper()],
            'custom_automation_type': kwargs.pop('automated', 1)
        }

        if 'references' in kwargs.keys():
            data['references'] = kwargs['references']
        if 'pre_conditions' in kwargs.keys():
            data['custom_preconds'] = kwargs['pre_conditions']
        if 'steps' in kwargs.keys():
            data['custom_steps'] = kwargs['steps']
        if 'expected_result' in kwargs.keys():
            data['custom_expected'] = kwargs['expected_result']

        tags_map = Testrail.TEST_CYCLE
        if 'test_cycle' in kwargs.keys():
            tags = [tags_map[tag] for tag in kwargs['test_cycle'] if tag in Testrail.TEST_CYCLE]
            data['custom_tags'] = tags if tags else [tags_map['REGRESSION']]
        else:
            data['custom_tags'] = [tags_map['REGRESSION']]

        if "tags" in kwargs:
            data['custom_tagsfreeformat'] = kwargs.get("tags")

        return data

    def get_statuses(self):
        """
        Get available Status in Test Rails
        :return:
        """
        url = urljoin(self.url, Testrail.GET_STATUS)
        _content = self.send_request(method="GET", url=url).content
        return {str(x['label']).upper(): x['id'] for x in _content}

    def get_priorities(self, reverse=False):
        """

        :return:
        """
        url = urljoin(self.url, Testrail.GET_PRIORITIES)
        _content = self.send_request(method="GET", url=url).content
        if reverse:
            return {x['id']: x['name'] for x in _content}
        else:
            return {x['name']: x['id'] for x in _content}

    def get_types(self, reverse=False):
        """

        :return:
        """
        url = urljoin(self.url, Testrail.GET_TYPES)
        _content = self.send_request(method="GET", url=url).content
        if reverse:
            return {x['id']: x['name'] for x in _content}
        else:
            return {x['name']: x['id'] for x in _content}

    def get_case(self, **kwargs):
        """
        Get Plan Details
        :param kwargs:
        :return:
        """
        case_id = kwargs['case_id']
        url = urljoin(self.url, Testrail.GET_CASE_URI.format(case_id))
        _content = self.send_request(method="GET", url=url).content
        return _content

    def get_plan(self, **kwargs):
        """
        Get Plan Details
        :param kwargs:
        :return:
        """
        _id = kwargs['plan_id']
        url = urljoin(self.url, Testrail.GET_PLAN_URI.format(_id))
        _content = self.send_request(method="GET", url=url).content
        return _content

    def get_configs(self, **kwargs):
        """
        Get Configs Present in TR
        :param kwargs:
        :return:
        """
        project_id = kwargs['project_id']
        url = urljoin(self.url, Testrail.GET_CONFIGS_URI.format(project_id))
        _content = self.send_request(method="GET", url=url).content
        return _content

    def add_test_case(self, **kwargs):
        """
        Add test case to a section
        :param kwargs:
            section_id: section to add test case in
            title: title of the test case
        :return: id of the test case, None on failure
        """
        section_id = kwargs['section_id']
        data = self._format_data(**kwargs)
        url = urljoin(self.url, Testrail.TEST_CASE_URI.format(section_id))
        _content = self.send_request(method="POST", url=url, data=data).content
        return _content['id']

    def update_test_case(self, **kwargs):
        """
        Update test-case
        :param kwargs:
        :return:
        """
        test_case_id = kwargs['case_id']
        del kwargs['case_id']
        url = urljoin(self.url, Testrail.UPDATE_CASE_URI.format(test_case_id))
        kwargs['url'] = url
        _content = self.send_request(method="POST", **kwargs).content
        return _content['id']

    def add_section(self, **kwargs):
        """
        :param kwargs:
            description: description for section
            parent_id: parent section id
            name: section name
        :return: id of section, None on failure
        """
        url = urljoin(self.url, Testrail.TEST_SECTION_URI.format(self.project_id))
        data = {
            'description': kwargs.pop('description', 'Automation'),
            'parent_id': kwargs.pop('parent_id', None),
            'name': kwargs['name']
        }
        _content = self.send_request(method="POST", url=url, data=data).content
        return _content['id']

    def get_sections(self, suite_id):
        """
        Get Sections from Test Rails
        :return:
        """
        url = urljoin(self.url, Testrail.SECTIONS_URI.format(self.project_id, suite_id))
        _content = self.send_request(method="GET", url=url).content
        return _content

    def get_section_cases(self, **kwargs):
        """
        Get Test-Case Sections
        :param kwargs:
        :return:
        """
        url = urljoin(self.url,
                      Testrail.GET_CASES_URI.format(self.project_id, kwargs["suite_id"], kwargs["section_id"]))
        _content = self.send_request(method="GET", url=url).content
        return _content

    def get_project_cases(self):
        """
        Get Test-Case Project
        :return:
        """
        url = urljoin(self.url, Testrail.GET_PROJECT_CASES_URI.format(self.project_id))
        _content = self.send_request(method="GET", url=url).content
        return _content

    def get_project_cases_in_suit(self, suit):
        """
        Get Test-Case Project
        :return:
        """
        url = urljoin(self.url, Testrail.GET_PROJECT_CASES_IN_SUIT_URI.format(self.project_id, suit))
        _content = self.send_request(method="GET", url=url).content
        return _content

    def get_filtered_project_cases_in_suit(self, suit, start_time, end_time):
        """
        Get Test-Case Project
        :return:
        """
        url = urljoin(self.url,
                      Testrail.GET_FILTERED_CASES_IN_SUIT_URI.format(self.project_id, suit, start_time,
                                                                     end_time))
        _content = self.send_request(method="GET", url=url).content
        return _content

    def get_section_id(self, **kwargs):
        """
        Get Section ID
        :param kwargs:
        :return:
        """
        section_name = kwargs['name']
        parent_id = kwargs['parent_id']
        suite_id = kwargs['suite_id']
        sections = self.get_sections(suite_id)
        for section in sections:
            if section['name'] == section_name and section['parent_id'] == parent_id:
                return section['id']
        return None

    def check_section_exists(self, **kwargs):
        """
        Check if Section Exists
        :param kwargs:
        :return:
        """
        section_name = kwargs['name']
        parent_id = kwargs['parent_id']
        sections = self.get_sections(parent_id)
        for section in sections:
            if section['name'] == section_name and section['parent_id'] == parent_id:
                return section['id']
        return None

    def get_test_run(self, **kwargs):
        """
        Get Test Run
        :param kwargs:
        :return:
        """
        run_id = kwargs['run_id']
        url = urljoin(self.url, Testrail.TEST_RUN_URI.format(run_id))
        _content = self.send_request(method="GET", url=url).content
        return _content

    def upload_test_data(self, **kwargs):
        """
        Upload Test Json File to Results
        :param kwargs:
        :return:
        """
        _id = kwargs['id']
        data = kwargs['data']
        url = urljoin(self.url, Testrail.ADD_ATTACHMENT_TO_RESULT_URI.format(_id))
        file_name = 'temp.json'

        with open(file_name, 'w') as _fp:
            _fp.write(json.dumps(data, indent=2, sort_keys=True))

        files = {'attachment': open(file_name, 'rb')}
        auth = str(base64.b64encode(bytes('%s:%s' % (self.username, self.password), 'utf-8')), 'ascii').strip()
        headers = {'Authorization': 'Basic ' + auth}
        _content = requests.post(url, headers=headers, files=files)
        os.remove(file_name)
        return _content

    def add_test_result(self, **kwargs):
        """
        Add Test-Results
        :param kwargs:
        :return:
        """
        run_id = kwargs['run_id']
        case_id = kwargs['case_id']
        url = urljoin(self.url, Testrail.RESULT_URI.format(run_id, case_id))
        result = kwargs['result']
        result_to_testrail = self.get_statuses()
        if result not in result_to_testrail.keys():
            raise Exception(f"Result {result} Unknown By Test-Rail System !!")
        data = {
            'status_id': result_to_testrail[result],
            'comment': kwargs['comment'],
            'version': kwargs['version'],
            'elapsed': kwargs['elapsed']
        }
        _content = self.send_request(method="POST", url=url, data=data).content
        return _content['id']

    def add_test_plan(self, **kwargs):
        """
        Adds new test plan
        :param kwargs:
            project_id: The id of the test project you want to add
            data: The data about the test run
        :return: id of the test plan, None on failure
        """
        project_id = kwargs['project_id']
        data = kwargs["data"]
        url = urljoin(self.url, Testrail.ADD_TEST_PLAN_URI.format(project_id))
        _content = self.send_request(method="POST", url=url, data=data).content
        return _content['id']

    def add_test_run_to_plan(self, **kwargs):
        """
        Adds a test run to a plan
        :param kwargs:
            plan_id: The id of the test plan you want to add the run to
            data: The data about the test run
        :return: id of the test run, None on failure
        """
        _id = kwargs['plan_id']
        data = kwargs['data']
        url = urljoin(self.url, Testrail.ADD_PLAN_ENTRY_URI.format(_id))
        _content = self.send_request(method="POST", url=url, data=data).content
        return _content[0]['id']

    def delete_test_run(self, **kwargs):
        """
        Deletes a test run to a plan
        :param kwargs:
            plan_id: The id of the test plan
        :return: 1 on success, None on failure
        """
        test_run_id = kwargs['test_run_id']
        data = {}
        url = urljoin(self.url, Testrail.DELETE_TEST_RUN_URI.format(test_run_id))
        _content = self.send_request(method="POST", url=url, data=data).content
        return _content

    def update_section(self, **kwargs):
        """
        Update Section Name
        :param kwargs:
        :return:
        """
        section_id = kwargs["section_id"]
        data = {"name": kwargs["name"]}
        url = urljoin(self.url, Testrail.UPDATE_SECTION.format(section_id))
        _content = self.send_request(method="POST", url=url, data=data).content
        return _content[0]['id']

    def get_test_suits(self, **kwargs):
        """
        Get Test Run
        :param kwargs:
        :return:
        """
        project_id = kwargs['project_id']
        url = urljoin(self.url, Testrail.GET_TEST_SUITS_URI.format(project_id))
        _content = self.send_request(method="GET", url=url).content
        return _content

    def get_test_cases_in_suit(self, **kwargs):
        """
        Get Test Run
        :param kwargs:
        :return:
        """
        project_id = kwargs['project_id']
        suite_id = kwargs['suite_id']
        url = urljoin(self.url, Testrail.GET_CASES_IN_SUIT_URI.format(project_id, suite_id))
        _content = self.send_request(method="GET", url=url).content
        return _content

    def get_results_for_run(self, **kwargs):
        """
        Get Test Run
        :param kwargs:
        :return:
        """
        run_id = kwargs['run_id']
        url = urljoin(self.url, Testrail.GET_RESULTS_FOR_RUN_URI.format(run_id))
        _content = self.send_request(method="GET", url=url).content
        return _content

    def add_attachment_to_test_result(self, **kwargs):
        """
        Add attachment to test result
        :param kwargs:
        :return:
        """
        test_id = kwargs['test_id']
        file_name = kwargs['file_name']
        url = urljoin(self.url, Testrail.ADD_ATTACHMENT_TO_RESULT_URI.format(test_id))
        file = {'file': open(file_name, 'rb')}
        auth = str(base64.b64encode(bytes('%s:%s' % (self.username, self.password), 'utf-8')), 'ascii').strip()
        headers = {
            "Authorization": f"Basic {auth}",
            "Content-Type": "application/json"
        }
        _content = requests.post(url, headers=headers, files=file)
        if _content.status_code != requests.codes.ok:
            return None
        return 1

    def create_test_plan(self, **kwargs):
        """
        Prepare Dictionary required for decurtis automation
        :return:
        """
        project_id = kwargs['project_id']
        name = kwargs['name']
        cases = kwargs['cases']
        configs = kwargs['configs']
        _all_cases = []
        suits = self.get_test_suits(project_id=1)
        for suit in suits:
            for _case in self.get_test_cases_in_suit(project_id=1, suite_id=suit['id']):
                if _case['id'] in cases:
                    _all_cases.append(_case)

        data = {
            "name": name,
            "entries": []
        }

        for _suite in list({_data['suite_id'] for _data in _all_cases}):
            _cases = [_data['id'] for _data in _all_cases if _data['suite_id'] == _suite]
            data['entries'].append({
                "suite_id": _suite, "include_all": False, "case_ids": _cases, "config_ids": configs,
                "runs": [{"include_all": False, "case_ids": _cases, "config_ids": configs, }]
            })

        _plan_id = self.add_test_plan(project_id=project_id, data=data)
        for _entry in self.get_plan(plan_id=_plan_id)['entries']:
            for _run in _entry['runs']:
                for _count, _cases in enumerate(_all_cases):
                    if _run['suite_id'] == _cases['suite_id']:
                        _all_cases[_count]['run_id'] = _run['id']
                        _all_cases[_count]['plan_id'] = _plan_id
                        _all_cases[_count]['configs'] = configs

        return _all_cases

    def get_plans(self, **kwargs):
        """
        Get Plan Details
        :param kwargs:
        :return:
        """
        project_id = kwargs['project_id']
        _offset = kwargs['offset']
        url = urljoin(self.url, Testrail.GET_PLANS_URI.format(project_id, _offset))
        _content = self.send_request(method="GET", url=url).content
        return _content

    def delete_test_plan(self, **kwargs):
        """
        Deletes a test run to a plan
        :param kwargs:
            plan_id: The id of the test plan
        :return: 1 on success, None on failure
        """
        _id = kwargs['plan_id']
        url = urljoin(self.url, Testrail.DELETE_TEST_PLAN.format(_id))
        _content = self.send_request(method="POST", url=url, data=None).content
        return _content

    def close_test_plan(self, **kwargs):
        """
        Close a test plan
        :param kwargs:
        :return:
        """
        _id = kwargs['plan_id']
        url = urljoin(self.url, Testrail.CLOSE_PLANS_URI.format(_id))
        _content = self.send_request(method="POST", url=url, data=None).content
        return _content

    def get_cases_in_project(self):
        """
        Get Test-Cases in a Project
        :return:
        """
        all_cases = []
        for suit in self.get_test_suits(project_id=self.project_id):
            cases = self.get_project_cases_in_suit(suit['id'])
            all_cases += cases
        return all_cases

    def get_filtered_cases_in_project(self, start_time, end_time):
        """
        Get Test-Cases in a Project from start_time till end_time
        :return:
        """
        all_cases = []
        for suit in self.get_test_suits(project_id=self.project_id):
            cases = self.get_filtered_project_cases_in_suit(suit['id'], start_time, end_time)
            all_cases += cases
        return all_cases

    def get_suites(self, suite_id):
        """
        Get Test-Cases in a Project based on Suite ID
        :return:
        """
        url = urljoin(self.url, Testrail.GET_SUITES.format(suite_id))
        _content = self.send_request(method="GET", url=url).content
        return _content

    def get_users(self):
        """
        Get Test Rail Users in a Project based on User ID
        :return:
        """
        url = urljoin(self.url, Testrail.GET_USERS)
        _content = self.send_request(method="GET", url=url).content
        return _content


if __name__ == "__main__":
    creds = read_creds_file()
    test_rail = Testrail(
        url=creds.test_rails.url,
        username=creds.test_rails.email,
        password=creds.test_rails.api_key
    )
    to_delete = set()
    to_close = set()
    done = False
    offset = 0
    while not done:
        response = test_rail.get_plans(project_id=1, offset=offset)
        if len(response.content) >= 250:
            offset += len(response.content)
        else:
            done = True
        for plan in response.content:
            tc_id = plan['id']
            passed = plan['passed_count']
            blocked = plan['blocked_count']
            retest = plan['retest_count']
            untested = plan['untested_count']
            failed = plan['failed_count']
            created_on = plan['created_on']

            if created_on < (int(datetime.utcnow().timestamp()) - (3600 * 48)) and not plan['is_completed']:
                # Delete if there is no pass and more than 1 un-tested
                if passed == 0 and untested > 0:
                    to_delete.add(tc_id)

                # Delete if there is zero pass and zero fail
                if passed == 0 and failed == 0:
                    to_delete.add(tc_id)

                # Close test plans which have zero untested test-cases
                if untested == 0:
                    to_close.add(tc_id)

                # Close test plans which have only one failure
                if failed == 1:
                    to_close.add(tc_id)

                if int(passed / (untested + passed)) < 25:
                    to_close.add(tc_id)

    for plan_id in to_delete:
        if plan_id not in [1, 11]:
            test_rail.delete_test_plan(plan_id=plan_id)

    for plan_id in (to_close - to_delete):
        if plan_id not in [1, 11]:
            test_rail.close_test_plan(plan_id=plan_id)

if __name__ == "__main__":
    creds = read_creds_file()
    test_rail = Testrail(url=creds.test_rails.url, username=creds.test_rails.email,
                         password=creds.test_rails.api_key)
