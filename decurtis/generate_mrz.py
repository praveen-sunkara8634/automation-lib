from mrz.generator.td3 import TD3CodeGenerator
from mrz.generator.mrvb import MRVBCodeGenerator
from PIL import Image, ImageDraw, ImageFont
from decurtis.common import *


class GenerateMrzImages:
    """
    Class to Generate MRZ Images
    """

    def __init__(self, **kwargs):
        """
        :param kwargs:
        """
        self.mrz_data = kwargs
        self.code = None
        self.path = os.path.dirname(os.path.realpath(__file__))
        self.generated_images = os.path.abspath('generated_images')
        if not os.path.isdir(self.generated_images):
            os.makedirs(self.generated_images)

        # Generate mrz Code
        self.generate_mrz()

    def generate_mrz_file_name(self, extension='.jpeg'):
        """
        Function to generate Random File Name
        :param extension:
        :return:
        """
        name = os.path.join(self.generated_images, f'{generate_random_alpha_numeric_string(length=15)}{extension}')
        return name

    def select_random_image(self):
        """
        Function to find .JPEG files in a folder
        :return:
        """
        if self.mrz_data['document_type'] == "P":
            images = os.path.join(self.path, 'ref_images/passport_images')
        else:
            images = os.path.join(self.path, 'ref_images/visa_images')

        files_found = []
        for dir_path, dir_names, file_names in os.walk(images):
            for file_name in file_names:
                if str(file_name).endswith('.jpeg'):
                    files_found.append(os.path.abspath(os.path.join(dir_path, file_name)))
        return os.path.abspath(random.choice(files_found))

    def mrz_image(self):
        """
        Function that will append text to an image
        :return:
        """
        image = Image.open(self.select_random_image())
        draw = ImageDraw.Draw(image)

        font_file_path = os.path.join(self.path, 'fonts/ocrbmt.ttf')
        font = ImageFont.FreeTypeFont(font_file_path, size=28)

        # starting position of the message
        (x, y) = (30, 560)
        message = str(self.code)
        color = 'rgb(0, 0, 0)'  # black color

        # draw the message on the background
        draw.text((x, y), message, fill=color, font=font)

        # save the edited image
        _file = self.generate_mrz_file_name()
        image.save(_file, compress_level=1)
        while True:
            if os.path.isfile(_file):
                break
            else:
                time.sleep(1)
                continue

        return _file

    def generate_mrz(self):
        """
        Generate MRZ Data
        :return:
        """
        # data = {
        #     'document_type': self.mrz_data['doc_type'],
        #     'country_code': self.mrz_data['country'],
        #     'given_names': self.mrz_data['first_name'],
        #     'surname': self.mrz_data['last_name'],
        #     'passport_number': self.mrz_data['passport_number'],
        #     'birth_date': self.mrz_data['birth_date'],
        #     'nationality': self.mrz_data['country'],
        #     'sex': self.mrz_data['sex'],
        #     'expiry_date': self.mrz_data['expiry']
        # }
        #
        if self.mrz_data['document_type'] == 'P':
            self.code = TD3CodeGenerator(**self.mrz_data)
        else:
            self.code = MRVBCodeGenerator(**self.mrz_data)


if __name__ == "__main__":
    data = {
        'document_type': 'P',
        'country_code': 'USA',
        'surname': 'Edward',
        'given_names': 'Chloe',
        'passport_number': 'WP9281A96',
        'nationality': 'USA',
        'birth_date': '950601',
        'sex': 'M',
        'expiry_date': '260418'
    }

    _mrz = GenerateMrzImages(**data)
