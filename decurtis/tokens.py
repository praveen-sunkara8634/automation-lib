__author__ = 'anshuman.goyal'

from decurtis.common import *
from decurtis.rest import SendRestRequest


class BitBucketTokens(SendRestRequest):
    """
    Generate BitBucket
    """

    def __init__(self, username, password):
        """
        Init Function, which will call and generate Bitbucket Token
        :param username:
        :param password:
        """
        super().__init__()
        self.username = username
        self.password = password

        if self.username is None or self.password is None:
            raise Exception("Both Username and Password are required, cannot be None")

        encoded = str(base64.b64encode(bytes(f"{username}:{password}", 'utf-8')), 'ascii').strip()
        self.basic_token = f"Basic {encoded}"
        self.session.headers['Authorization'] = self.basic_token


class OciTokens(SendRestRequest):
    """
    Generate DXP OCI Tokens
    """

    def __init__(self, ship, shore, platform, auth=None):
        """
        Init Function, which will Generate Tokens for any DXP System
        :param ship:
        :param shore:
        :param platform:
        :param auth:
        """
        super().__init__()
        self.ship = ship
        self.shore = shore
        self.platform = platform
        self.auth = auth
        if self.auth is None:
            raise Exception("auth is required for OCI Tokens")

        self.basic_token = None
        self.bearer_token = None
        self.crew_token = None

        self.update_basic_token()
        self.update_bearer_token()
        if platform in ['DXP', 'VIRGIN'] and ship is not None:
            self.update_crew_token()

    def update_basic_token(self):
        """
        Function to Get Basic Token
        :return:
        """
        _user_pass = base64.b64decode(self.auth).decode()
        username = str(_user_pass).split(":")[0]
        password = str(_user_pass).split(":")[1]

        if username is None or password is None:
            raise Exception("ERROR: username and password are required to get token !!")

        encoded = str(base64.b64encode(bytes(f"{username}:{password}", 'utf-8')), 'ascii').strip()
        self.basic_token = f"Basic {encoded}"
        self.session.headers['Authorization'] = self.basic_token

    def update_bearer_token(self):
        """
        Function to Get Bearer Token
        :return:
        """
        if self.platform in ['DXP', 'DCL', 'VIRGIN']:
            bearer_host = 'identityaccessmanagement-service/oauth/token?grant_type=client_credentials'
        else:
            raise Exception(f"Platform {self.platform} not supported")

        _host = urljoin(self.shore, bearer_host)
        content = self.send_request("POST", url=_host).content
        self.bearer_token = process_token(content)

    def update_crew_token(self, crew_data=None):
        """
        Function to Get Crew Token
        :param crew_data:
        :return:
        """
        if 'stage' not in self.shore:
            crew_host = '/user-account-service/signin/email'
            _host = urljoin(self.ship, crew_host)
            if crew_data is None:
                crew_data = {
                    "userName": "rosa.yang@gmail.com",
                    "password": "1234",
                    "appId": "38c1ab51-ce97-439c-9d20-c48d724e1fe5"
                }
            _content = self.send_request("POST", url=_host, data=crew_data).content
            self.crew_token = f"{_content['tokenType']} {_content['accessToken']}"


class MociTokens(SendRestRequest):
    """
    Generate DXP MOCI Tokens
    """

    def __init__(self, ship, shore, platform, auth=None):
        """
        Init Function, which will Generate Tokens for any DXP System
        :param ship:
        :param shore:
        :param platform:
        :param auth:
        """
        super().__init__()
        self.ship = ship
        self.shore = shore
        self.platform = platform
        self.auth = auth
        if self.auth is None:
            raise Exception("auth is required for MOCI Tokens")

        self.basic_token = None
        self.bearer_token = None
        self.crew_token = None
        self.admin_token = None

        self.update_basic_token()
        self.update_bearer_token()
        self.update_admin_token()

        self.update_crew_token()

    def update_basic_token(self):
        """
        Function to Get Basic Token
        :return:
        """
        _user_pass = base64.b64decode(self.auth).decode()
        username = str(_user_pass).split(":")[0]
        password = str(_user_pass).split(":")[1]

        if username is None or password is None:
            raise Exception("ERROR: username and password are required to get token !!")

        encoded = str(base64.b64encode(bytes(f"{username}:{password}", 'utf-8')), 'ascii').strip()
        self.basic_token = f"Basic {encoded}"
        self.session.headers['Authorization'] = self.basic_token

    def update_bearer_token(self):
        """
        Function to update Bearer Token
        :return:
        """
        if self.platform == 'DXP':
            bearer_host = '/reservation-bff/auth'
        elif self.platform == 'VIRGIN':
            bearer_host = '/bookvoyage-bff/startup'
        elif self.platform == 'DCL':
            bearer_host = 'identityaccessmanagement-service/oauth/token?grant_type=client_credentials'
        else:
            raise Exception(f"Platform {self.platform} not supported")
        if self.platform == 'DCL':
            _host = urljoin(self.shore, bearer_host)
            content = self.send_request("POST", url=_host).content
            self.bearer_token = process_token(content)
        else:
            _host = urljoin(self.shore, bearer_host)
            content = self.send_request("GET", url=_host).content
            self.bearer_token = process_token(content)

    def update_admin_token(self, admin_host=None, admin_creds=None):
        """
        Function to update admin token
        :param admin_host:
        :param admin_creds:
        :return:
        :rtype:
        """
        if self.platform in ['DXP', 'DCL']:
            if admin_host is None:
                admin_host = '/moderate-online-check-in-bff/login'
                _host = urljoin(self.shore, admin_host)
            if admin_creds is None:
                admin_creds = {
                    "username": "lise.kelvin",
                    "password": "1234",
                    "appId": str(generate_guid()).upper(),
                    "deviceId": str(generate_random_alpha_numeric_string(length=32)).lower(),
                }
        elif self.platform == 'VIRGIN':
            if admin_host is None:
                admin_host = '/moderate-online-check-in-bff/login'
                _host = urljoin(self.shore, admin_host)
            if "dev" in _host:
                username = "lise.kelvin"
                password = "1010"
            else:
                username = "lise.kelvin@gmail.com"
                password = "Yellow*99"
            if admin_creds is None:
                admin_creds = {
                    "username": username,
                    "password": password,
                    "appId": str(generate_guid()).upper(),
                    "deviceId": str(generate_random_alpha_numeric_string(length=32)).lower(),
                }
        else:
            raise Exception(f"Platform {self.platform} not supported")
        _content = self.send_request("POST", url=_host, data=admin_creds).content
        self.admin_token = f"{_content['tokenDetail']['tokenType']} {_content['tokenDetail']['accessToken']}"

    def update_crew_token(self, crew_host=None, crew_creds=None):
        """
        Update Crew Token
        :param crew_host:
        :param crew_creds:
        :return:
        :rtype:
        """

        if crew_host is None:
            crew_host = '/identityaccessmanagement-service/oauth/token?grant_type=password'
        if crew_creds is None:
            crew_creds = {
                "username": "rosa.yang",
                "password": "1234",
                "appId": "a7c8d0ac-7e69-11e7-a7e8-0a1a4261e962"
            }

        _host = urljoin(self.ship, crew_host)
        self.session.headers.update({'Content-Type': 'application/x-www-form-urlencoded'})
        _content = process_response(self.session.post(url=_host, data=crew_creds)).content
        self.crew_token = f"{_content['token_type']} {_content['access_token']}"
        self.session.headers.update({'Content-Type': 'application/json'})


if __name__ == "__main__":
    u_name = 'testing.user@decurtis.com'
    u_pass = 'Hu8#tud@999'
    bb = BitBucketTokens(username=u_name, password=u_pass)
