import decurtis.common
import pandas as pd


class Excel:
    """
     Class for Excel Function
    """

    def __init__(self, name, sheet):
        self.excel = name
        self.sheet = sheet

    def read_data(self):
        wb = pd.read_excel(self.excel, self.sheet)
        return wb
