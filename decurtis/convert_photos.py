__author__ = 'anshuman.goyal'

import os
from PIL import Image


class ConvertPhotos:
    """
    Class to convert images in a given folder
    """

    def __init__(self, folder_path):
        """
        Init Function
        :param folder_path:
        """
        self.folder_path = os.path.abspath(folder_path)
        if not os.path.isdir(self.folder_path):
            raise Exception(f"ERROR: Invalid folder path {self.folder_path}")

    def convert_image(self, _from, _to):
        """
        Convert all images in a folder
        :return:
        """
        for _dir, _dir_name, _file_names in os.walk(self.folder_path):
            for _file_name in _file_names:
                if str(_file_name).lower().endswith(_from):
                    _name = os.path.join(_dir, _file_name)
                    _new_name = str(_name).lower().replace(_from, _to)
                    im = Image.open(_name)
                    im.save(_new_name, 'PNG')

    def convert_image_to_jpeg(self, _from, _to):
        """
        Convert all images to JPEG
        :param _from:
        :param _to:
        :return:
        """

        for dir, dir_names, file_names in os.walk(self.folder_path):
            for file_name in file_names:
                image_path = os.path.join(dir, file_name)
                im = Image.open(image_path)
                rgb_im = im.convert('RGB')
                image_path = str(image_path).replace(".png", ".jpeg")
                rgb_im.save(image_path)
