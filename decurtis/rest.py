__author__ = 'anshuman.goyal'

import json
import os
from datetime import datetime
from functools import wraps

import allure
import pytz
import requests
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry

from decurtis.common import process_response, generate_curl_command, e2e_guid, logger, change_merged_services_link

correlationId = e2e_guid()


def update_authentication(method):
    """
    Decorator function to update auth type
    :param method:
    :return:
    """

    @wraps(method)
    def wrapper_function(self, *method_args, **method_kwargs):
        """
        Wrapper Function to set authentication type and update header accordingly
        :param self:
        :param method_args:
        :param method_kwargs:
        :return:
        """
        if 'auth' in method_kwargs:

            # Remove all authorizations from Header First
            if 'authorization' in self.session.headers:
                del self.session.headers['authorization']
            if 'Authorization' in self.session.headers:
                del self.session.headers['Authorization']

            # Update Basic Token
            if str(method_kwargs['auth']).lower() == "basic":
                assert self.basic_token is not None, "ERROR: Basic Token is None !!"
                self.session.headers['Authorization'] = self.basic_token

            # Update Bearer Token
            elif str(method_kwargs['auth']).lower() == "bearer":
                assert self.bearer_token is not None, "ERROR: Bearer Token is None !!"
                self.session.headers['Authorization'] = self.bearer_token

            # Update User Token
            elif str(method_kwargs['auth']).lower() == "user":
                assert self.user_token is not None, "ERROR: User Token is None !!"
                self.session.headers['Authorization'] = self.user_token

            # Update Crew Token
            elif str(method_kwargs['auth']).lower() == "crew":
                assert self.crew_token is not None, "ERROR: Crew Token is None !!"
                self.session.headers['Authorization'] = self.crew_token

            # Update Admin Token
            elif str(method_kwargs['auth']).lower() == "admin":
                assert self.admin_token is not None, "ERROR: Admin Token is None !!"
                self.session.headers['Authorization'] = self.admin_token

            else:
                pass

        # Change the URL as per changed services (dxpcore)
        method_kwargs['url'] = change_merged_services_link(method_kwargs['url'])

        # Execute the method and save it's response
        response = method(self, *method_args, **method_kwargs)

        return process_response(response)

    return wrapper_function


class SendRestRequest:
    """
    Class to send Rest Requests on a remote server
    """

    def __init__(self):
        """
        Init Function to get the Auth Token
        """
        global correlationId
        self.headers = dict()

        self.basic_token = None
        self.bearer_token = None
        self.user_token = None
        self.crew_token = None
        self.admin_token = None

        self.con_to = 60  # TODO @bug DCP-39429 -> Has to be 10 seconds
        self.res_to = 60  # TODO @bug DCP-39429 -> Has to be 10 seconds
        self.timeout = (self.con_to, self.res_to)

        logger.debug(f"correlationId: {correlationId}")
        os.environ["correlationId"] = correlationId

        self.session = requests.Session()
        self.session.verify = True

        # Adding Retries when there is a connect, redirect and read issue !!
        adapter = HTTPAdapter(
            max_retries=Retry(total=2, connect=2, read=2, backoff_factor=0.5, status_forcelist=(422, 500, 502, 504)))
        self.session.mount('http://', adapter)
        self.session.mount('https://', adapter)

        self.session.headers['Content-Type'] = 'application/json'
        self.session.headers['Accept'] = "*/*"
        self.session.headers['correlationId'] = correlationId

    @update_authentication
    def send_request(self, method, url, data=None, files=None, params=None, headers=None, auth='Basic'):
        """
        Send any Request
        :param method
        :param url
        :param data
        :param files
        :param params
        :param headers
        :param auth
        """
        method = str(method).upper()
        exception = None
        arguments = {
            'method': method,
            'url': url,
            'data': json.dumps(data),
            'files': files,
            'timeout': self.timeout,
            'params': params
        }

        # Remove None Params, so that they are not sent with Final Call
        if data is None:
            del arguments['data']
        if files is None:
            del arguments['files']
        if params is None:
            del arguments['params']

        if headers:
            arguments['headers'] = headers

        if files:
            if 'content-type' in self.session.headers:
                del self.session.headers['content-type']
            if 'Content-Type' in self.session.headers:
                del self.session.headers['Content-Type']

        now = datetime.now(pytz.timezone('Asia/Calcutta'))
        try:
            # Generate and save curl command
            curl = generate_curl_command(method=method, headers=self.session.headers, url=url, params=params, data=data)
            name = f"CurlCmd_{now.minute}{now.second}{now.microsecond}.json"
            allure.attach(curl, name=name, attachment_type=allure.attachment_type.TEXT)
            # logger.debug(f"{curl}")

            response = self.session.request(**arguments)
            response.raise_for_status()
            return response
        except requests.exceptions.HTTPError as err:
            exception = err
        finally:
            if files:
                self.session.headers['content-type'] = 'application/json'
            if exception:
                generate_curl_command(method=method, headers=self.session.headers, url=url, params=params, data=data)
                raise Exception(f'** {exception} **')
