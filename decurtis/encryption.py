__author__ = 'anshuman.goyal'

from cryptography.fernet import Fernet
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.hazmat.backends import default_backend
import cryptography
import base64
import os


class DecurtisEncryption:
    """
    Class to Encrypt and Decrypt Data/File
    """

    def __init__(self):
        """
        Init Function
        """
        self.salt = 'salt_'.encode()
        self.password = os.environ.get('E2E_PASSWORD', None)
        if self.password is None:
            raise Exception('Password Needed for Decoding Credentials')

    def generate_key(self):
        """
        Generate Key for Encryption
        """
        kdf = PBKDF2HMAC(
            algorithm=hashes.SHA256(),
            length=32,
            salt=self.salt,
            iterations=100000,
            backend=default_backend()
        )
        key = base64.urlsafe_b64encode(kdf.derive(self.password.encode()))
        return key

    def encrypt_data(self, data):
        """
        Function to Encrypt Data
        :param data: data to be encrypted
        """
        key = self.generate_key()
        data = base64.b64encode(bytes(data, encoding='utf-8'))
        f = Fernet(key)
        encrypted = f.encrypt(data)
        return encrypted

    def decrypt_data(self, data):
        """
        Function to decrypt Data
        :param data: data to be decrypted
        """
        key = self.generate_key()
        f = Fernet(key)
        try:
            decrypted = base64.b64decode(f.decrypt(data))
            return decrypted.decode()
        except (Exception, cryptography.fernet.InvalidToken):
            raise Exception(f"Password to Decode Encrypted File is Invalid !!")

    def decrypt_file(self, file_name, destination=None):
        """
        Function to decrypt File, if destination is defined we write file to that destination else we return back the
            decided content
        :param file_name: File to be decrypted
        :param destination: destination where the output has to be written
        """
        with open(file_name, 'r') as _fp:
            data = _fp.read()

        dec = self.decrypt_data(data.encode())
        if destination:
            with open(destination, 'w') as _fp:
                _fp.write(dec)
        else:
            return dec

    def encrypt_file(self, file_name, destination=None):
        """
        Function to Encrypt File
        :param file_name: File to be encrypted
        :param destination: Destination Fine Name
        """
        with open(file_name, 'r') as _fp:
            data = _fp.read()
        enc = self.encrypt_data(data)

        if destination:
            with open(destination, 'w') as _fp:
                _fp.write(enc.decode())


if __name__ == "__main__":
    DecurtisEncryption().decrypt_file('decurtis/credentials/creds', 'decurtis/credentials/creds.json')
    DecurtisEncryption().encrypt_file('decurtis/credentials/creds.json', 'decurtis/credentials/creds')

    DecurtisEncryption().decrypt_file('decurtis/credentials/k8_developer', 'decurtis/credentials/k8_developer.yaml')
    DecurtisEncryption().encrypt_file('decurtis/credentials/k8_developer.yaml', 'decurtis/credentials/k8_developer')

    DecurtisEncryption().decrypt_file('decurtis/credentials/k8_pr_master', 'decurtis/credentials/k8_pr_master.yaml')
    DecurtisEncryption().encrypt_file('decurtis/credentials/k8_pr_master.yaml', 'decurtis/credentials/k8_pr_master')

    DecurtisEncryption().decrypt_file('decurtis/credentials/k8_qa1', 'decurtis/credentials/k8_qa1.yaml')
    DecurtisEncryption().encrypt_file('decurtis/credentials/k8_qa1.yaml', 'decurtis/credentials/k8_qa1')
