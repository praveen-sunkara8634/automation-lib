__author__ = 'anshuman.goyal'

import json
import os
import re
from decurtis.common import urljoin, read_creds_file
from decurtis.tokens import BitBucketTokens


class BitBucketApi:
    def __init__(self, username=None, password=None):
        """
        Bit Bucket API to interact with Bit bucket
        :param username:
        :param password:
        """
        self.base = "https://api.bitbucket.org/2.0/repositories/"
        if username is None or password is None:
            self.creds = read_creds_file()
            username = self.creds.bit_bucket.username
            password = self.creds.bit_bucket.password

        self.bb = BitBucketTokens(username=username, password=password)

    def update_client(self, client):
        """
        Update the client (required to switch in-between)
        :param client:
        """
        self.base = urljoin("https://api.bitbucket.org/2.0/repositories/", client)

    def read_remote_file(self, client, repo, file_path):
        """
        Get list of file as a particular path
        :param client:
        :param repo:
        :param file_path:
        :return:
        """
        if self.base in file_path:
            url = file_path
        else:
            url = urljoin(self.base, client, repo, file_path)
        return self.bb.send_request("GET", url=url).content

    def get_list_of_files(self, client, repo, path='test_scripts'):
        """
        Get list of file as a particular path
        :param client:
        :param repo:
        :param path:
        :return:
        """
        url = urljoin(self.base, client, repo, re.sub(r"/$", "", path, re.I | re.M))
        content = self.bb.send_request("GET", url=url).content
        values = content['values']
        while 'next' in content:
            content = self.bb.send_request("GET", url=content['next']).content
            values.extend(content['values'])

        return values

    def get_open_pull_requests(self, client, repo):
        """
        Get Open Pull Requests in Repository
        :param client:
        :param repo:
        """
        url = urljoin(self.base, client, repo, 'pullrequests?state=OPEN&pagelen=50')
        return self.bb.send_request("GET", url=url).content

    def get_pull_requests_statuses(self, client, repo, number):
        """
        Get Open Pull Requests in Repository
        :param client:
        :param repo:
        :param number:
        """
        url = urljoin(self.base, client, repo, f'pullrequests/{number}/statuses')
        return self.bb.send_request("GET", url=url).content

    def get_commits(self, client, repo):
        """
        Get Last 100 Commits
        :param client:
        :param repo:
        :return:
        """
        url = urljoin(self.base, client, repo, 'commits/master?pagelen=50')
        return self.bb.send_request("GET", url=url).content

    def get_last_non_system_commit_user_email(self, client, repo):
        """
        Get Emails of all commits to master
        :param client:
        :param repo:
        :return:
        """
        commits = self.get_commits(client, repo)
        authors = [x['author']['raw'] for x in commits['values']]
        emails = [re.search(r'<(.*?)>', x, re.I | re.M).group(1) for x in authors]
        filtered_emails = list(set(
            filter(lambda a: a not in ['ciuser@decurtis.com', 'ciduser@decurtis.com', None] and '@decurtis.com' in a,
                   emails)))
        return filtered_emails[0:2]

    def get_pr_author_email(self, client, repo, number):
        """
        Get author or Pull Request
        :return:
        """
        url = urljoin(self.base, client, repo, f'pullrequests/{number}/commits')
        content = self.bb.send_request("GET", url=url).content
        authors = []
        for raw in [x['author']['raw'] for x in content['values']]:
            match = re.search(r".*<(.*?@decurtis.com)>", raw)
            if match:
                authors.append(match.group(1))
        return authors

    def upload_file_to_branch(self, client, repo):
        """
        Upload file to a bit bucket branch
        """
        url = urljoin(self.base, client, repo, 'src/bitbucket-testing', 'bitbucket.py')
        with open('bitbucket.py', 'r') as fp:
            data = self.bb.send_request("POST", url=url, files=fp)
            print()


if __name__ == "__main__":
    bb = BitBucketApi()
    # data = bb.get_pr_author_email('decurtis', 'dxp-infra-identity-access-management-service', "364")
    bb.upload_file_to_branch('decurtis', 'ncl-e2e-automation')
    print()
