from decurtis.common import *
from jira import JIRA


class DxpJira:
    """
    Class to play with Jira Ticketing System
    """

    def __init__(self, user, api_key):
        """
        Connect to Jira etc.
        :param user:
        :param api_key:
        """
        self.user = user
        self.api_key = api_key
        self.basic_auth = (self.user, self.api_key)
        self.epic_link = 'customfield_10004'

        # Static Variables
        self.epic = 'TDCP-1'  # TODO Change it to DCP-23138
        self.project_name = 'TDCP'  # TODO: Change it to DCP

        # To Be Used Variables
        self.project = None
        self.project_id = None
        self.fields = None
        self.jira = JIRA(server='https://decurtis.atlassian.net/', basic_auth=self.basic_auth, timeout=30)

        # Always Run these functions
        self._issue_fields(self.epic)
        self._connect_to_project(self.project_name)

    def _connect_to_project(self, name):
        """
        Connect to a Project
        :param name:
        :return:
        """
        for project in self.jira.projects():
            if project.key == name:
                self.project_id = project.id
                break

        self.project = self.jira.project(self.project_id)

    def get_user_name(self, user):
        """
        Get User Name from give user (in Jira Structure)
        :param user:
        :return:
        """
        _user_name = str(user).split('@')[0]
        if len(_user_name.split('.')) <= 1:
            raise Exception(f"Wrong username '{user}'")
        for _users in self.jira.search_users(_user_name.split(".")[0]):
            if _user_name == _users.key:
                return _users.name, _users.accountId
        raise Exception(f"ERROR: No matching user found {user} !!")

    def delete_issue(self, issue):
        """
        Delete and issue
        :param issue:
        :return:
        """
        self.jira.issue(issue).delete()

    def _issue_fields(self, sample):
        """
        Get Fields in an issue
        :param sample:
        :return:
        """
        issue = self.jira.issue(sample)
        self.fields = issue.fields

    def get_issue_details(self, issue):
        """
        Get issue details
        :param issue:
        :return:
        """
        issue_data = self.jira.issue(issue, fields='summary,description,assignee,issuetype,customfield_10004')
        return issue_data

    def get_open_component_bugs(self, summary, environment, service):
        """
        Get all Open Bugs in Project
        :param summary:
        :param environment:
        :param service:
        :return:
        """
        if service == 'online-check-in-bff':
            query = f'status NOT IN ("Closed", "Done", "Rejected") AND summary ~ "E2E | {environment} | {service}' \
                    f' Failed" AND NOT summary ~ "moderate-online-check-in-bff"'
        else:
            query = f'status NOT IN ("Closed", "Done", "Rejected") AND summary ~ "{summary}"'
        _result = self._run_query(query)
        if _result.total > 0:
            return _result.iterable[0].key
        else:
            return None

    def get_resolved_components_bug(self, summary, environment, service):
        """
        Get all resolved Bugs in Project
        :param summary:
        :param environment:
        :param service:
        :return:
        """
        if service == 'online-check-in-bff':
            query = f"status IN (\"Resolved\") AND summary ~ \"E2E | {environment} | {service} Failed\" AND NOT summary ~ " \
                    f"\"moderate-online-check-in-bff\""
        else:
            query = f"status IN (\"Resolved\") AND summary ~ \"{summary}\""
        _result = self._run_query(query)
        if _result.total > 0:
            return _result.iterable[0].key
        else:
            return None

    def get_all_open_bugs(self):
        """
        Get all Open Bugs in Project
        :return:
        """
        _result = self._run_query('status NOT IN ("Closed", "Done", "Rejected")')

    def _run_query(self, query):
        """
        Function to run Query on Jira
        :param query:
        :return:
        """
        query = f"project = \"{self.project_name}\" AND {query}"
        _result = self.jira.search_issues(query)
        return _result

    def check_comp_ticket_is_open(self, summary, environment, service):
        """
        Get if component ticket is already open
        :param summary:
        :param environment:
        :param service:
        :return:
        """
        # summary = f"E2E For {component} Failed"
        issues = self.get_open_component_bugs(summary, environment, service)
        return issues

    def get_statuses(self):
        """
        Get the all status of JIRA
        :return:
        """
        return self.jira.statuses()

    def create_ticket(self, **kwargs):
        """
        Function to create a ticket in specified format
        :param kwargs:
        :return:
        """
        assignee = self.get_user_name(kwargs['assignee'])[1]
        environment = kwargs['environment']
        test_case = kwargs['test_case']
        job_link = kwargs['job_link']
        service = kwargs['service']
        reason = kwargs['reason']
        correlation = kwargs['correlation']
        curl = kwargs['curl']
        test_data = kwargs['test_data']
        kibana_urls = kwargs['kibana_urls']
        kibana_error = kwargs['kibana_error']

        # Jira Head Line
        summary = f"E2E | {environment} | {service} Failed !!"
        ticket = self.check_comp_ticket_is_open(summary.replace(" !!", ""), environment, service)

        # Format the body to be added in Jira Ticket
        _file = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'JiraDescription.txt')
        with open(_file, 'r') as _fp:
            body = _fp.read()

        # Get Shore Error Message, else set this to None
        shore_error = json.dumps(kibana_error['shore'])
        ship_error = json.dumps(kibana_error['ship'])
        urls = json.dumps(kibana_urls)

        # Form the body to be pasted in Jira Ticket !!
        body = body.format(assignee, environment, test_case, job_link, job_link, service, reason, correlation, curl,
                           shore_error, ship_error, urls)

        if ticket:
            # TODO: do this in future
            # issue_all_value = self.jira.issue(ticket, (str('description')))
            # description = issue_all_value.raw['fields']['description']
            resolved_ticket = self.get_resolved_components_bug(summary.replace(" !!", ""), environment, service)
            if resolved_ticket:
                self.jira.transition_issue(ticket, 'OPEN')
                self.jira.add_comment(ticket, body)
                self.jira.assign_issue(ticket, self.get_user_name(kwargs['assignee'])[0])
                logger.info(f"Jira Ticket => {ticket} Re-Opened and Comment(s) Added Successfully ...")
            else:
                self.jira.add_comment(ticket, body)
                logger.info(f"Jira Ticket => {ticket} Comment(s) Added Successfully  ...")
            return f"https://decurtis.atlassian.net/browse/{ticket}"
        else:
            fields = {
                'project': {'id': self.project_id},
                'summary': summary,
                'description': body,
                'issuetype': {'name': 'Bug'},
                self.epic_link: self.epic,
                'assignee': {'name': self.get_user_name(kwargs['assignee'])[0]},
            }
            _content = self.jira.create_issue(fields=fields)
            logger.info(f"Jira Ticket => {_content.key} Created/Added successfully ...")
            return f"https://decurtis.atlassian.net/browse/{_content.key}"
