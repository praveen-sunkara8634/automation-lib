from mrz.generator.td3 import TD3CodeGenerator
from PIL import Image, ImageDraw, ImageFont
from decurtis.common import *


class GenerateVisaMrzImages:
    """
    Class to Generate MRZ Images
    """

    def __init__(self, **kwargs):
        """
        Generate MRZ with following data
            document_type: str,
            country_code: str,
            surname: str,
            given_names: str,
            passport_number: str,
            nationality: str,
            birth_date: str,
            sex: str,
            expiry_date: str,
            optional_data="",
        :param kwargs:
        """
        self.mrz_data = kwargs
        self.code = self.generate_mrz()
        self.path = os.path.dirname(os.path.realpath(__file__))
        self.generated_images = os.path.abspath('generated_images')
        if not os.path.isdir(self.generated_images):
            os.makedirs(self.generated_images)

        # self.append_text_to_image()

    def visa_file_name(self, extension='.jpeg'):
        """
        Function to Get Visa File Name
        :param extension:
        :return:
        """
        name = os.path.join(self.generated_images, f'{generate_random_alpha_numeric_string(length=15)}{extension}')
        return os.path.abspath(name)

    def select_random_visa_image(self):
        """
        Function to Choose Random Visa Image
        :return:
        """
        visa_images = os.path.join(self.path, 'ref_images/visa_images')
        files_found = []
        for dir_path, dir_names, file_names in os.walk(visa_images):
            for file_name in file_names:
                if str(file_name).endswith('.jpeg'):
                    files_found.append(os.path.abspath(os.path.join(dir_path, file_name)))
        return os.path.abspath(random.choice(files_found))

    def append_text_to_image(self):
        """
        Function that will append text to an image
        :return:
        """
        image = Image.open(self.select_random_visa_image())
        draw = ImageDraw.Draw(image)
        font_file_path = os.path.join(self.path, 'fonts/ocrbmt.ttf')
        font = ImageFont.FreeTypeFont(font_file_path, size=28)
        # starting position of the message
        (x, y) = (30, 560)
        message = str(self.code)
        color = 'rgb(0, 0, 0)'  # black color
        # draw the message on the background
        draw.text((x, y), message, fill=color, font=font)
        # save the edited image
        _file = self.visa_file_name()
        image.save(_file, compress_level=1)
        while True:
            if os.path.isfile(_file):
                break
            else:
                time.sleep(1)
                continue
        return _file

    def generate_mrz(self):
        """
        Generate MRZ Data
        :return:
        """
        doc_type = self.mrz_data['doc_type']
        country = self.mrz_data['country']
        last_name = self.mrz_data['last_name']
        first_name = self.mrz_data['first_name']
        passport_number = self.mrz_data['document_number']
        birth_date = self.mrz_data['birth_date']
        sex = self.mrz_data['sex']
        expiry = self.mrz_data['expiry']
        optional_data = self.mrz_data['document_number']
        code = TD3CodeGenerator(doc_type, country, last_name, first_name, passport_number, country,
                                birth_date, sex, expiry, optional_data)
        return code


if __name__ == "__main__":
    _first_name = str(generate_first_name())
    _last_name = str(generate_last_name())
    _birth_date = str(generate_birth_date().strftime('%y%m%d'))
    _expiry = str(generate_document_expiry().strftime('%y%m%d'))
    _country = str(generate_country_code())
    final_file = GenerateVisaMrzImages(
        doc_type="P",
        country=_country,
        last_name=_last_name,
        first_name=_first_name,
        passport_number=str(generate_document_number()),
        birth_date=str(_birth_date),
        sex=str(random.choice(['M', 'F'])),
        expiry=str(_expiry)
    ).append_text_to_image()
