#!groovy

// Global Variables
gitCredentials = 'agoyal-dc'
gitRepo = 'git@bitbucket.org:decurtis/dxp-python-automation.git'
gitBranch = 'master'
dockerImageToUpdate = "anshumang/ubuntu-python"

node {
    stage('Git-Clone') {
        git url: gitRepo, credentialsId: gitCredentials, branch: gitBranch
    }
    stage('Build-Docker-Image') {
        def workspace = pwd()
        docker.withRegistry('', 'docker-anshuman') {
            def dockerImage = docker.build(dockerImageToUpdate)
            dockerImage.inside('-u root') {
                sh 'apt-get --assume-yes clean'
                sh 'apt-get --assume-yes --allow-unauthenticated update'
                sh 'apt-get --assume-yes --allow-unauthenticated upgrade'
                sh 'apt autoremove --assume-yes --allow-unauthenticated'
                sh 'ls -l'
            }
            dockerImage.push()
        }
    }
}
