#!groovy

// Global Variables
gitCredentials = 'agoyal-dc'
gitRepo = 'git@bitbucket.org:decurtis/dxp-python-automation.git'
gitBranch = 'master'
dockerImageToUpdate = "anshumang/ubuntu-python"

node {
    stage('Git-Clone') {
        git url: gitRepo, credentialsId: gitCredentials, branch: gitBranch
    }
    stage('Run-PyTest-In-Docker') {
        def workspace = pwd()
        docker.withRegistry('', 'docker-anshuman') {
            def dockerImage = docker.image(dockerImageToUpdate)
            dockerImage.inside('-u root -v $workspace/:/root/:z') {
                sh 'mkdir -p allure_results'
                sh 'mkdir -p automation_logs'
                sh 'py.test --test_data="dev.json" test_scripts --cache-clear'
            }
        }
    }
    stage('Allure-Results') {
        allure includeProperties: false, jdk: 'JAVA', results: [[path: "${workspace}/"]]
    }
}
