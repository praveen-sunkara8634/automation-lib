__author__ = 'anshuman.goyal'
"""
Python Script to check and report database connections with different servers from Jenkins Box
"""

import json
import re
import socket

from decurtis.bitbucket import BitBucketApi
from decurtis.common import read_json_file


class CheckDbConnection(BitBucketApi):
    """
    Main Class to check Database Connection
    """

    def __init__(self):
        """
        Init Function which will iterate over all environments
        """
        super().__init__()
        self.client = None
        self.repo = None

        self.data = read_json_file('decurtis/utils/place_holder_app_secrets.json', nt=False)
        self.final_status = dict()
        for platform in self.data:
            self.final_status[str(platform).upper()] = dict()
            for environment in self.data[platform]:
                self.platform = str(platform).upper()
                self.environment = str(environment).upper()
                self.client = self.data[platform][environment]['client']
                self.repo = self.data[platform][environment]['repo']
                ship_status, ship_details = self.check_connection(url=self.data[platform][environment]['place_holders']['ship'])
                shore_status, shore_details = self.check_connection(url=self.data[platform][environment]['place_holders']['shore'])
                self.final_status[self.platform][str(environment).upper()] = {
                    "SHIP": 'OK' if ship_status is True else ship_details,
                    "SHORE": 'OK' if shore_status is True else shore_details,
                }

        with open('dbPortStatus.json', 'w') as _fp:
            _fp.write(json.dumps(self.final_status, sort_keys=True, indent=2))

    def check_connection(self, url):
        """
        Check connection by reading placeholder file and fetching host and port from it
        :param url
        """
        data = self.read_remote_file(client=self.client, repo=self.repo, file_path=url)
        host = re.search(r"\$PGHOST_1\$\|(.*)", data, re.I | re.M).group(1)
        port = re.search(r"\$PGPORT_INSTANCE_1\$\|(.*)", data, re.I | re.M).group(1)
        username = re.search(r"\$PGUSER_READWRITE\$\|(.*)", data, re.I | re.M).group(1)
        password = re.search(r"\$PGPASSWORD_READWRITE\$\|(.*)", data, re.I | re.M).group(1)

        if host is None or port is None or username is None or password is None:
            raise Exception(f'Data not sufficient for db connection !!')

        status = self.check_port(host=host, port=port)
        if status:
            print(f"'{self.platform}-{self.environment}' '{host}:{port}' is OK")
        else:
            print(f"'{self.platform}-{self.environment}' '{host}:{port}' is not working !!")
        return status, f"{host}:{port}"

    @staticmethod
    def check_port(host, port):
        """
        Check if we are able to open a socket connection on given host/port
        :param host
        :param port
        """
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        result = sock.connect_ex((host, int(port)))
        sock.close()
        if result != 0:
            return False
        return True


if __name__ == "__main__":
    con = CheckDbConnection()
