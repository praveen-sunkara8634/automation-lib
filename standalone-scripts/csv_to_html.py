__author__ = 'sarvesh.singh'

import os
import pandas as pd
from decurtis.slack import SlackNotification


class CsvToHtml:
    """
    Class to Convert the CSV into a HTML file
    """

    def __init__(self):
        """
        Convert csv to html
        """
        csv_details = self.read_csv(conatins='vv-booking')
        data = csv_details[0]
        load_file = csv_details[1].split('_clients')[0]
        csv_file = csv_details[1].split('.')[0]
        self.delete_columns(data=data,
                            columns_name=['50%', '66%', '75%', '80%', '90%', '95%', '98%', '99%', '99.9%', '99.99%',
                                          '99.999', '100%'], axis=1, inplace=True)
        styled_data = data.style
        styled_data = styled_data.applymap(self.color_highest_red)
        styled_data = styled_data.set_table_styles([{'selector': '', 'props': [('border', '4px solid #010B12')]}])

        styled_data = styled_data.set_properties(
            **{'text-align': 'center', 'border-color': 'Black', 'border-width': 'thin', 'border-style': 'dotted'})
        self.write_html(path=f'{csv_file}.html', html=styled_data)
        slack = SlackNotification()
        slack.send_file(message=f'Please see the Load report for {load_file} here', recipients='testrail-notification',
                        file_name=f'{csv_file}.html')

    @staticmethod
    def read_csv(conatins):
        """
        To read the csv file using pandas as dataframe
        """
        for fname in os.listdir('.'):
            if fname.endswith('.csv') and conatins in fname:
                break
        else:
            raise Exception("There is no csv present for load")
        data = pd.read_csv(fname)
        return data, fname

    @staticmethod
    def delete_columns(data, columns_name=None, axis=None, inplace=None):
        """
        To delete the columns from the dataframe
        """
        data.drop(columns_name,
                  axis=axis,
                  inplace=inplace)

    @staticmethod
    def color_highest_red(val):
        """
        Takes a scalar and returns a string with
        the css property `'color: red'` for val > 3000
        `'color: orange'` for val >= 1000 and val < 3000
        integer values, black otherwise.
        """
        if isinstance(val, int):
            if val >= 3000:
                color = 'red'
                return 'color: %s' % color
            elif val >= 1000 and val < 3000:
                color = 'orange'
                return 'color: %s' % color
            else:
                color = 'black'
                return 'color: %s' % color
        else:
            color = 'black'
            return 'color: %s' % color

    @staticmethod
    def write_html(path, html):
        """
        To write the html code in a html file
        """
        path = os.path.abspath(path)
        Html_file = open(path, "w")
        Html_file.write(html.render())
        Html_file.close()


if __name__ == "__main__":
    CsvToHtml()
