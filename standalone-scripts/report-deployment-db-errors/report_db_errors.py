__author__ = 'anshuman.goyal'

from decurtis.common import read_json_file, read_creds_file
from decurtis.jenkins import JenkinsAutomation
from decurtis.slack import SlackNotification
from datetime import datetime
import re
import json


class ReportPromotionErrors:
    """
    Report All PSQL Promotion Errors to DB Team
    """

    def __init__(self):
        """
        Init Function
        """
        self.slack = SlackNotification()
        self.channel = 'support-db'
        self.mapping_file_name = 'standalone-scripts/report-deployment-db-errors/job-mappings.json'
        self.mapping = self.read_mapping_file()
        self.since_minutes = 15
        self.creds = read_creds_file()
        all_errors = dict()
        overall_status = 'PASS'
        for platform in self.mapping.keys():
            env_errors = dict()
            for environment in self.mapping[platform]:
                self.name = self.mapping[platform][environment]['JOB_NAME']
                self.url = getattr(self.creds.jenkins, self.mapping[platform][environment]['JENKINS']).url
                self.user = getattr(self.creds.jenkins, self.mapping[platform][environment]['JENKINS']).username
                self.token = getattr(self.creds.jenkins, self.mapping[platform][environment]['JENKINS']).token
                print(f"Checking PSQL Errors in {platform} {environment} {self.url} {self.name}"
                      f" in past {self.since_minutes} Minutes ...")
                messages = self.report_errors(self.url, self.name, self.user, self.token)
                env_errors[environment] = messages
                if len(messages) > 0:
                    overall_status = "FAIL"
            all_errors[platform] = env_errors

        with open('ReportedErrors.json', 'w') as _fp:
            _fp.write(json.dumps(all_errors, indent=2, sort_keys=True))

        if overall_status == 'FAIL':
            raise Exception("PSQL ERRORS SEEN IN SOME JOBS | CHECK ARTIFACTS !!")

    @staticmethod
    def generate_slack_job_link(url, name, number):
        """
        Generate Click-Able Jenkins Job Link
        """
        return f"<{url}/job/{name}/{number}/consoleFull|{name} #{number}>"

    def read_mapping_file(self):
        """
        Read Mapping File (Json)
        """
        return read_json_file(self.mapping_file_name, nt=False)

    def report_errors(self, url, name, user, token):
        """
        Report PSQL Error
        :param url
        :param name
        :param user
        :param token
        """
        all_messages = dict()
        jenkins = JenkinsAutomation(base_url=url, username=user, password=token)
        jobs = self.filter_jobs(jenkins.get_all_jobs(name), since_minutes=self.since_minutes)
        for count, job in enumerate(jobs, start=1):
            number = int(job['number'])
            print(f"-> Checking {count} of {len(jobs)} :: {name} Job #{number}")
            console_logs = jenkins.get_console_logs(name=name, number=number)
            messages = self.get_psql_errors(console_logs)
            if len(messages) > 0:
                dump = json.dumps(list(messages), indent=2, sort_keys=True)
                _link = self.generate_slack_job_link(url, name, number)
                message = f"<!subteam^SUE2865CG> PSQL ERRORS in {_link} ```{dump}```"
                self.slack.send_message(message, self.channel)
                all_messages[number] = messages
        return all_messages

    @staticmethod
    def get_psql_errors(console_logs):
        """
        Get PSQL Errors in Console Logs
        """
        # Finding PSQL Errors
        messages = set()
        for count, log in enumerate(console_logs.split('\n')):
            found = re.search(r'(.*?\s*)(psql:\s*.*)', log, re.I | re.M)
            if found:
                tar = found.group(1)
                error = found.group(2)

                # skipping tar based errors
                if "tar" in tar:
                    continue

                if re.search(r'fatal|error', error, re.I | re.M):
                    messages.add(log)

        return list(messages)

    @staticmethod
    def filter_jobs(jobs, since_minutes=2):
        """
        Function to filter out
        :param jobs
        :param since_minutes
        """
        time_now = int(datetime.utcnow().timestamp())
        return [x for x in jobs if int(x['timestamp'] / 1000) >= time_now - (60 * since_minutes)]


if __name__ == "__main__":
    ReportPromotionErrors()
