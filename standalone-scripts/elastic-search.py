from elasticsearch import Elasticsearch

es = Elasticsearch('https://qa1-infra.dxp-decurtis.com/elasticsearch/',
                   # turn on SSL
                   use_ssl=True,
                   # no verify SSL certificates
                   verify_certs=False,
                   # don't show warnings about ssl certs verification
                   ssl_show_warn=False
                   )

doc = es.search(index="shoreapplog-*",
                body={"query": {"bool": {"must": [{"match_phrase": {"severity": {"query": "ERROR"}}}],
                                         "filter": [{"multi_match": {"type": "phrase",
                                                                     "query": "e2e-fefb2b22-059b-48ef-b010-649f9a63b955",
                                                                     "lenient": True}}]}}})

print()
