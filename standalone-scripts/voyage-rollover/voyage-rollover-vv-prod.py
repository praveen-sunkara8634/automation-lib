import requests
import re
import json
import time
from urllib.parse import urlparse, urlunparse


class VoyageRollover:
    """
    Class to perform Voyage Rollover
    """

    def __init__(self, environment='prod'):
        """
        Perform the following:
            - Get access tokens
            - Get active voyage in system
            - Get all voyages present (which can be set active)
            - Generate Voyage Data (needed for roll over)
            - Get Ship Time (and Date)
            - Do a Rollover only in case debark date is behind ship time (means voyage is complete)
        """
        if environment == 'prod':
            self.base_url = 'https://application.scl.virginvoyages.com'
            self.ship = 'https://application.scl.virginvoyages.com'
            self.sync = f"{self.ship}/syncgatewayfeedersadmin/prodappdocstore"
        elif environment == 'integration':
            self.base_url = 'https://application-integration.ship.virginvoyages.com/svc'
            self.ship = 'https://application-integration.ship.virginvoyages.com/svc'
            self.sync = f"{self.ship}/syncgatewayfeedersadmin/appdocstorenew_v1"
        elif environment in ['cert', 'qa']:
            self.base_url = 'https://k8s-qaship.virginvoyages.com/'
            self.ship = 'https://k8s-qaship.virginvoyages.com'
            self.sync = f"{self.ship}/syncgatewayfeedersadmin/appdocstorenew_v1"
        else:
            raise Exception("ERROR: This Script Tested & Made for Prod and integration only !!")

        self.basic_token = 'NzgzZDg2OGMtMWM1NS00YWM4LWFhNGItMGU3OThmZWYwMmY4Ojhqd2JhMmZBRmtmNDk3Y3E='
        self.access_token = self.get_access_token()
        self.ship_date = self.get_ship_date()
        self.current_voyage = self.get_active_voyage()
        self.voyage_data = self.get_all_voyages()
        self.voyage_id = self.voyage_data['voyageId']

    def get_access_token(self):
        """
        Get Token
        """
        headers = {"Authorization": f"Basic {self.basic_token}"}
        params = {"grant_type": "client_credentials"}
        url = self.urljoin(self.base_url, 'identityaccessmanagement-service/oauth/token')
        data = {}

        content = requests.request("POST", url, data=data, headers=headers, params=params, verify=False).json()
        return content['access_token']

    def get_ship_date(self):
        """
        Get Ship Time
        """
        url = f"{self.ship}/crew-bff/crew-embarkation/shiptime"
        params = {"shipcode": "SC"}
        headers = {"Authorization": f"bearer {self.access_token}"}
        content = requests.request("GET", url, headers=headers, params=params, verify=False).json()
        return content['utcTimestamp'].split('T')[0]

    @staticmethod
    def set_data_for_active_voyage(voyage):
        """
        This function will return voyage data required for setting an active voyage
        :param voyage:
        :return:
        """
        to_delete = [
            'createdByUser',
            'creationTime',
            'modificationTime',
            'modifiedByUser',
            '_links',
            # 'boardingMethod'
        ]
        to_set_true = ['isActive', 'isVoyageRollover']

        # Delete Fields
        for _to_delete in to_delete:
            if _to_delete in voyage:
                del voyage[_to_delete]

        # Delete Fields in voyageItineraries
        for _count in range(0, len(voyage['voyageItineraries'])):
            for _to_delete in to_delete:
                if _to_delete in voyage['voyageItineraries'][_count]:
                    del voyage['voyageItineraries'][_count][_to_delete]

        # Set to True
        for _to_set_true in to_set_true:
            if _to_set_true in voyage:
                voyage[_to_set_true] = True

        return voyage

    @staticmethod
    def urljoin(*args):
        """
        This function will join a URL and return back proper url
        :return:
        """
        parsed = list(urlparse('/'.join(args)))
        parsed[2] = re.sub("/{2,}", "/", parsed[2])
        _host = urlunparse(parsed)
        return _host

    def get_active_voyage(self):
        """
        Get active voyage
        """
        url = f"{self.ship}/dxpcore/voyages/search/findByactiveVoyage"
        params = {"shipcode": "SC"}
        headers = {"Authorization": f"bearer {self.access_token}"}
        content = requests.request("GET", url, headers=headers, params=params, verify=False).json()
        return content['_embedded']['voyages'][0]

    def get_all_voyages(self):
        """
        Get active voyage
        """
        url = f"{self.ship}/dxpcore/voyages"
        params = {"size": "400", "page1": None}
        headers = {"Authorization": f"bearer {self.access_token}"}
        all_voyages = requests.request("GET", url, headers=headers, params=params, verify=False).json()['_embedded'][
            'voyages']

        debark_date = self.current_voyage['debarkDate'].split('T')[0]
        to_roll_over = [x for x in all_voyages if x['embarkDate'].split('T')[0] == debark_date]
        if len(to_roll_over) == 0 or len(to_roll_over) > 1:
            raise Exception(f"{len(to_roll_over)} Voyages Found !!")

        voyage_data = self.set_data_for_active_voyage(to_roll_over[0])
        return voyage_data

    def perform_voyage_roll_over(self):
        """
        Perform Voyage Rollover
        """
        debark = str(self.current_voyage['debarkDate']).split('T')[0]
        if debark > self.ship_date:
            print(f"Debark-Date: {debark} > Ship-Date: {self.ship_date}, Voyage Rollover Not-Required !!")
            return

        url = f"{self.ship}/dxpcore/voyages"
        headers = {
            "Content-Type": "application/json",
            "Authorization": f"bearer {self.access_token}"
        }
        data = self.voyage_data
        # print(json.dumps(data, indent=2))
        content = requests.request("POST", url, data=json.dumps(data), headers=headers, verify=False)
        if not content.ok:
            raise Exception("Failed to Perform Voyage Roll-Over")

        self.validate_voyage_in_couch()

    def validate_voyage_in_couch(self, wait_time=60):
        """
        Validate Voyage is in Couch
        """
        url = f"{self.sync}/Voyage::{self.voyage_id}"

        time_now = int(time.time())
        while (int(time.time()) - time_now) < wait_time:
            success = requests.request("GET", url, verify=False).ok
            if success:
                break
            else:
                time.sleep(10)
        else:
            raise Exception("Error: Voyage Not Found in Couch !!")


if __name__ == "__main__":
    VoyageRollover(environment='prod').perform_voyage_roll_over()
