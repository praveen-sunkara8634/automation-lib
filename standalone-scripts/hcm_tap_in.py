from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.keys import Keys

options = webdriver.ChromeOptions()
url = "http://localhost:4444/wd/hub"
# options.add_argument('--headless')
options.add_argument("--no-sandbox")
# options.add_argument("--disable-gpu")
options.add_argument("--privileged")

driver = webdriver.Remote(command_executor=url, desired_capabilities=options.to_capabilities())
driver.get("https://z1.microimagehcm.com/app/")
driver.find_element_by_id("UserName").send_keys("anshuman.goyal@decurtis.com")
driver.find_element_by_id("Password").send_keys("MASKED")
driver.find_element_by_id("signin").click()
timeout = 120
try:
    element_present = EC.presence_of_element_located((By.CLASS_NAME, 'dash-item'))
    WebDriverWait(driver, timeout).until(element_present)
except TimeoutException:
    print("Timed out waiting for page to load")

driver.find_element_by_id("")
