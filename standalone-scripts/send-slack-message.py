from decurtis.slack import SlackNotification

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--users", default='anshuman.goyal@decurtis.com', type=str, required=True, help="Users to Tag")
parser.add_argument("--message", default='Test-Message', type=str, required=True, help="Message to be sent")

args = parser.parse_args()

users = str(args.users).replace(" ", "").lower().split(',')
message = args.message

slack = SlackNotification()

slack.send_message(message, users)
