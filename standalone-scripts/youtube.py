from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.keys import Keys
import requests
import json


class TestHackaThon:
    """
    Main Class
    """
    host = 'http://54.169.34.162:5252'

    def __init__(self):
        """
        Init Class
        """

        self.to_search = self.send_request(method="GET", "{}/video".format(self.host))

    def send_get_request(self, url):
        """
        Function to send a rest request on a remote server
        :param url:
        :return:
        """
        header = {
            'Content-Type': "application/json",
            'Accept': "*/*",
            'Cache-Control': "no-cache",
            'Accept-Encoding': "gzip, deflate",
            'Connection': "keep-alive",
        }
        session = requests.Session()
        session.headers.update(header)
        session.verify = False
        response = session.get(url)
        try:
            content = json.loads(response.content.decode("utf-8"))
        except:
            content = response.content.decode("utf-8")
        return content

    def selenium_automation(self):
        """
        Run Selenium Automation
        :return:
        """


if __name__ == "__main__":
    test_hack = TestHackaThon()

options = webdriver.ChromeOptions()
url = "http://localhost:4444/wd/hub"
# options.add_argument('--headless')
options.add_argument("--no-sandbox")
# options.add_argument("--disable-gpu")
options.add_argument("--privileged")

# driver = webdriver.Remote(command_executor=url, desired_capabilities=options.to_capabilities())
driver.find_element_by_id("search").send_keys("anshuman.goyal@decurtis.com")
driver = webdriver.Chrome(executable_path='/Users/anshumangoyal/Projects/dxp-python-automation/chromedriver')
driver.get("http://youtube.com")
# driver.maximize_window()
driver.find_elements_by_class_name("style-scope ytd-masthead").send_keys("anshuman.goyal@decurtis.com")
driver.find_element_by_id("Password").send_keys("India@13579#$")
driver.find_element_by_id("signin").click()
timeout = 120
try:
    element_present = EC.presence_of_element_located((By.CLASS_NAME, 'dash-item'))
    WebDriverWait(driver, timeout).until(element_present)
except TimeoutException:
    print("Timed out waiting for page to load")

driver.find_element_by_id("")
