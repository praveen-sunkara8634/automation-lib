"""
This standalone script will pull active voyages from system
"""
import json
import argparse
from datetime import datetime
from datetime import timedelta
from decurtis.common import urljoin, read_creds_file
from decurtis.bitbucket import BitBucketApi
from decurtis.workers import get_credentials
from decurtis.tokens import OciTokens


class GetEmbarkDates:
    """
    Class to get Voyages
    """

    def __init__(self, ship, shore, platform="DXP", environment="dev"):
        """
        Init Function to initialise the Variables
        :param ship:
        :param shore:
        :param platform:
        :param environment:
        """
        self.ship = ship
        self.shore = shore
        self.platform = platform
        self.environment = environment
        self.bb = BitBucketApi()
        self.creds = get_credentials(self.bb, platform, environment, self.ship, self.shore)
        self.auth = self.creds.oci
        self.request = self.rest_oci()
        self.get_future_voyages()

    @staticmethod
    def credentials():
        """
        Creds Fixture with all Credentials
        :return:
        """
        return read_creds_file()

    def rest_oci(self):
        """
        Rest Session Fixture for OCI
        :return:
        """
        # Token Generation is different for Virgin and DXP
        response = OciTokens(ship=self.ship, shore=self.shore, platform=self.platform, auth=self.auth)
        return response

    def get_future_voyages(self):
        """
        Get Future Voyages
        :return:
        :rtype:
        """
        _now = datetime.now().date() + timedelta(days=2)
        _later = datetime.now().date() + timedelta(days=31)
        _url = f"dxpcore/voyages/search/findbyembarkdate?startdate={_now}&enddate={_later}"
        url = urljoin(self.ship, _url)

        # Get Voyages and see if there's any active voyage or not !!
        _content = self.request.send_request(method="GET", url=url, auth="bearer").content
        voyages = _content['_embedded']['voyages']
        embark_dates = sorted([x['embarkDate'].split('T')[0] for x in voyages if x['shipCode'] == 'DX'])
        with open('embarkDates.json', 'w') as fp:
            fp.write(json.dumps(embark_dates, indent=2))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ship", default='https://qa1-sagar.dxp-decurtis.com', type=str, required=False, help="Ship")
    parser.add_argument("--shore", default='https://qa1-shore.dxp-decurtis.com', type=str, required=False, help="Shore")
    args = parser.parse_args()
    GetEmbarkDates(ship=args.ship, shore=args.shore, platform="DXP")
