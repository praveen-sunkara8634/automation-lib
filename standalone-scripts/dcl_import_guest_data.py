from decurtis.common import *
from decurtis.excel import Excel


class DCLImportGuests:
    def __init__(self):
        self.qa_db = connect_qa_db()

    @staticmethod
    def get_file_data():
        """
        get data from excel file
        :return:
        """
        file = 'Reservation-Guest-data-Import.xlsx'
        path = os.path.abspath(file)
        excel = Excel(name=path, sheet='Reservation Guest')
        sheet_data = excel.read_data()
        return sheet_data

    def get_db_columns(self):
        """
        get all DB columns
        :return:
        """
        query = "SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'excel_import_dcl'"
        columns = self.qa_db.run_and_fetch_data(query)
        return columns

    def check_db_for_columns(self):
        """
        get columns from excel and DB
        """
        data = self.get_file_data()
        columns_excel = data.columns.array
        db_columns = self.get_db_columns()
        self.add_new_column(excel_columns=columns_excel, db_columns=db_columns)

    def add_new_column(self, excel_columns, db_columns):
        """
        To add new Column after checking discrepancy in DB and Excel
        """

        data = []
        for column in excel_columns:
            column_value = (column.replace(" ", "_")).lower()
            db = [sub['column_name'] for sub in db_columns]
            if column_value not in db:
                data.append(f'ADD COLUMN {column_value} varchar(200)')
        if data:
            tables = ",".join(data)
            query = f"ALTER TABLE excel_import_dcl {tables};"
            self.qa_db.insert_columns(query=query)


if __name__ == "__main__":
    import_utility = DCLImportGuests()
    import_utility.check_db_for_columns()
