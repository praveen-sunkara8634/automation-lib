"""
This Script will Check A Job and filter out unique jobs which have failed.
At the end it will re-trigger Jobs which have failed and send a slack notification to the user
"""

import re
import time
import json
from datetime import datetime

from decurtis.bitbucket import BitBucketApi
from decurtis.jenkins import JenkinsAutomation
from decurtis.slack import SlackNotification
from decurtis.common import read_creds_file


class JenkinsRerunPR:
    """
    Class to Rerun Failed Jenkins Failed PR Builds
    """

    def __init__(self, name='Pull-Request-Env', server='infra'):
        """
        Init Function that will make connection with Jenkins Server
        :param: name
        :param: server
        """
        self.name = name
        self.channel = 'e2e-infra-checks'
        self.creds = read_creds_file()

        self.username = getattr(self.creds.jenkins, server).username
        self.password = getattr(self.creds.jenkins, server).token
        self.server = getattr(self.creds.jenkins, server).url
        self.jenkins = JenkinsAutomation(base_url=self.server, username=self.username, password=self.password)

        self.e2e_re_trigger_stages = ['Running E2E Testing Script', 'Master-CI', 'Run-Tests', 'E2E Test Script']

        self.slack = SlackNotification()

        self.failed_builds, self.report_builds = self.get_failed_pr_jobs_to_rerun(self.name)

        # Send Slack Message for Builds that we are not re-triggering
        self.send_slack_for_not_triggering_builds()

        # Re-Trigger Jobs
        self.re_trigger_jobs()

    def get_all_build_details(self, name):
        """
        Fetch all Build Details
        """
        original_data = self.jenkins.get_all_jobs(name)
        to_delete = []
        for count, _data in enumerate(original_data):
            number = _data['number']
            try:
                url = self.jenkins.WF_API.format(self.jenkins.base_url, name, number)

                # Get Details of Jenkins Job
                details = self.jenkins.send_request("GET", url=url).content
                original_data[count]['status'] = details['status']
                original_data[count]['stages'] = details['stages']

                # Get Params of Jenkins Job
                params = self.jenkins.get_build_params(name, _data['number'])
                params = {x: y for x, y in params.items() if y not in [None, '', 'null']}
                original_data[count]['params'] = params

                original_data[count]['description'] = " - ".join(sorted(params.values()))
            except (TypeError, ValueError):
                to_delete.append(count)
                pass

        # Remove data which wfapi failed to get
        original_data = [i for j, i in enumerate(original_data) if j not in to_delete]

        with open("jenkins-data.json", 'w') as _fp:
            _fp.write(json.dumps(original_data, indent=2, sort_keys=True))

        return original_data

    @staticmethod
    def filter_duplicate_jobs(to_rerun):
        """
        Filter out duplicate Jobs
        """
        to_trash = []
        to_report = []
        all_failed_descriptions = {x['description'] for x in to_rerun}
        for _description in all_failed_descriptions:
            indexes = [index for index, x in enumerate(to_rerun) if x['description'] == _description]
            if len(indexes) >= 2:
                to_trash.extend(indexes)
                to_report.append(to_rerun[indexes[0]])
            else:
                to_trash.extend(indexes[1::])

        return [x for index, x in enumerate(to_rerun) if index not in to_trash], to_report

    def filter_jobs_not_in_e2e_stage(self, to_rerun):
        """
        Filter Jobs which are have not reached E2E Stage
        """
        # Remove Jobs which have not reached E2E Stage
        to_trash = []
        for _count, _failed in enumerate(to_rerun):
            e2e_failed_stage = False
            for stage in [x['name'] for x in _failed['stages']]:
                if stage in self.e2e_re_trigger_stages:
                    e2e_failed_stage = True
                    break
            if not e2e_failed_stage:
                to_trash.append(_count)

        return [x for index, x in enumerate(to_rerun) if index not in to_trash]

    def get_failed_pr_jobs_to_rerun(self, name):
        """
        Get PR's which have failed from Jenkins
        :param name:
        :return:
        """
        _original_data = self.get_all_build_details(name)

        all_failed = list(filter(lambda x: x['result'] == 'FAILURE', _original_data))
        all_passed = list(filter(lambda x: x['result'] == 'SUCCESS', _original_data))
        all_running = list(filter(lambda x: x['status'] == 'IN_PROGRESS', _original_data))

        all_passed_descriptions = {x['description'] for x in all_passed}
        all_running_descriptions = {x['description'] for x in all_running}

        to_trash = []
        for _count, _failed in enumerate(all_failed):
            if _failed['description'] in all_passed_descriptions:
                to_trash.append(_count)
                continue
            if _failed['description'] in all_running_descriptions:
                to_trash.append(_count)
                continue

        to_rerun = [x for index, x in enumerate(all_failed) if index not in to_trash]

        # Trash Duplicates
        to_rerun, to_report = self.filter_duplicate_jobs(to_rerun)

        # Remove Jobs which have not reached E2E Stage
        to_rerun = self.filter_jobs_not_in_e2e_stage(to_rerun)

        return to_rerun, to_report

    def _process_failed_builds(self):
        """
        Process Failed builds for stages
        """
        to_re_trigger = []
        for _failed in self.failed_builds:
            if len(_failed['stages']) > 0:
                all_stages = {x['name']: x['status'] for x in _failed['stages']}
                if len(set(all_stages).intersection(set(self.e2e_re_trigger_stages))) > 0:
                    to_re_trigger.append(_failed)
        return to_re_trigger

    def send_slack_for_not_triggering_builds(self):
        """
        Send Slack message for not running builds
        """
        utc_now = datetime.utcnow().timestamp()
        one_hour = 60 * 60 * 1
        for report in self.report_builds:
            if int(report['timestamp'] / 1000) <= (utc_now - one_hour):
                continue

            number = int(report['number'])
            _users = self.jenkins.get_user_name(self.name, number)
            _users = list(filter(lambda a: a not in ['ciuser@decurtis.com', 'ciduser@decurtis.com', None], _users))
            _users = ", ".join(list(set(_users)))
            params = report['params']
            job_link = f"<{self.server}/job/{self.name}/{number}/consoleFull|{self.name} #{number}>"

            # Send message to e2e-infra-checks Channel and Tag DXP Support
            message = f"<!subteam^SRCPK843S> {job_link} is not re-triggered | failed >= 2 ```{_users}\n{params}```"
            self.slack.send_message(message, self.channel)

            # Send message to Individual
            if len(_users) > 0:
                message = f"{job_link} is not re-triggered | failed >= 2 ```{_users}\n{params}```"
                self.slack.send_message(message, _users)

    def read_console_logs(self, name, number):
        """
        Function to read console logs of a job
        Automatically switches to Master-CI Job
        """
        console_logs = self.jenkins.get_console_logs(name, number)
        return console_logs

    @staticmethod
    def get_psql_errors_from_console_logs(console_logs):
        """
        Function to Read PSQL Errors from Console Logs
        """
        messages = set()
        for log in console_logs.split('\n'):
            found = re.search(r'.*?\s*(psql:\s*.*)', log, re.I | re.M)
            if found:
                content = found.group(1)
                if re.search(r'fatal|error', content, re.I | re.M):
                    messages.add(content)
        return messages

    def re_trigger_jobs(self):
        """
        Re-Trigger Jobs
        """
        to_re_trigger = self._process_failed_builds()

        utc_now = datetime.utcnow().timestamp()
        one_hour = 60 * 60 * 1

        for trigger in to_re_trigger:
            if int(trigger['timestamp'] / 1000) <= (utc_now - one_hour):
                continue
            number = trigger['id']
            params = trigger['params']
            users = self.jenkins.get_user_name(self.name, number)
            console_logs = self.read_console_logs(self.name, number)
            messages = self.get_psql_errors_from_console_logs(console_logs)

            # Check if the PR is still in Open State
            pr_num = None
            repo = None
            if 'Repository' in params and 'PR' in params:
                repo = params['Repository']
                pr_num = int(params['PR'])
            if repo is None or pr_num is None:
                continue

            open_pr = BitBucketApi().get_open_pull_requests(client='decurtis', repo=repo)
            for _prs in open_pr['values']:
                if _prs['id'] == pr_num:
                    break
            else:
                continue

            # Get unique users, don't send it to multiple users
            users = list(filter(lambda a: a not in ['ciuser@decurtis.com', 'ciduser@decurtis.com', None], users))
            if len(users) == 0:
                users.append('anshuman.goyal@decurtis.com')
            users = list(set(users))

            # Don't Rerun Scala Jobs, they have some other issue
            if 'Technology' in params and params['Technology'] == 'Scala':
                job_link = f"<{self.server}/job/{self.name}/{number}/consoleFull|{self.name} #{number}>"
                message = f"{job_link} is Scala Job and thus not re-triggering ```{params}```"
                self.slack.send_message(message, self.channel)
                continue

            job_link = f"<{self.server}/job/{self.name}/{number}/consoleFull|{self.name} #{number}>"
            if len(messages) > 0:
                message = '\n'.join(messages)
                message = f"<!subteam^SUE2865CG> Re-Trigger-Job Found PSQL ERRORS in  {job_link} ```{message}```"
                self.slack.send_message(message, 'support-db')

            data = self.jenkins.rebuild_job(self.name, number)
            new_number = data['number']

            # Get Old and New Link
            old = f"<{self.server}/job/{self.name}/{number}/consoleFull|{self.name} #{number}>"
            new = f"<{self.server}/job/{self.name}/{new_number}/consoleFull|{self.name} #{new_number}>"
            message = f"Re-Triggering Job: Failed E2E: {old} Re-Triggered {new} ```{users}\n{params}```"

            old = f"{self.server}/job/{self.name}/{number}/consoleFull"
            new = f"{self.server}/job/{self.name}/{new_number}/consoleFull"
            print(f"Failed E2E {self.name} {old} Re-Triggering {new}")
            self.slack.send_message(message, users)
            self.slack.send_message(message, self.channel)

            # Wait for Job to finish
            time.sleep(30)
            self.jenkins.wait_for_stage(self.name, new_number, self.e2e_re_trigger_stages)


if __name__ == "__main__":
    JenkinsRerunPR()
