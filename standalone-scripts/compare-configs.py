from bs4 import BeautifulSoup
from decurtis.common import *


class ConfigCompare:
    """
    This Class will compare two configs of two different environments
    """

    def __init__(self, reference, under_test):
        """
        Compare two configs
        :param reference
        :param under_test
        """
        self.ref_ids = self.get_config_ids(reference)
        self.ut_ids = self.get_config_ids(under_test)

        self.ref_data = self.get_config_data(self.ref_ids)
        self.ut_data = self.get_config_data(self.ut_ids)

        _diff = self.compare_configs(self.ref_data, self.ut_data)
        print(_diff)

    @staticmethod
    def _get_soup(url):
        """
        Get Soup for URL (assuming it is HTML Parser
        """
        response = requests.request("GET", url)
        text = str(response.text).replace('<!DOCTYPE html>\n', '')
        soup = BeautifulSoup(text, features="lxml")
        return soup

    def get_config_ids(self, url):
        _url = url.format('configuration/Home/ViewSettings')
        soup = self._get_soup(_url)
        configs = dict()
        for _option in soup.findAll('option'):
            name = _option.text
            attrs = _option.attrs
            if 'value' in attrs:
                value = attrs['value']
                configs[name] = {
                    'value': value,
                    'url': url.format("configuration/Home/ViewSettings?component={}".format(value))
                }

        return configs

    def get_config_data(self, reference):
        final_data = dict()
        for name in reference:
            ref = reference[name]
            url = ref['url']
            soup = self._get_soup(url)

            try:
                options = soup.findAll('table')[0].findAll('tbody')[0].findAll('tr')
            except IndexError:
                continue

            for _option in options:
                _data = _option.text.split('\n')
                _key = _data[1]
                _value = _data[2]
                _env = _data[3]
                if name not in final_data:
                    final_data[name] = {_key: {'value': _value, 'env': _env}}
                else:
                    final_data[name].update({_key: {'value': _value, 'env': _env}})

        return final_data

    @staticmethod
    def compare_configs(reference, under_test):
        diff_data = dict()
        for _ref in reference:
            if _ref not in under_test:
                diff_data[_ref] = {
                    "reference": reference[_ref],
                    "under_test": None
                }
            else:
                if reference[_ref] != under_test[_ref]:
                    diff_data[_ref] = {
                        "reference": reference[_ref],
                        "under_test": under_test[_ref]
                    }

        return diff_data


if __name__ == '__main__':
    ref = "https://anshumangoyal-sagar.developer.dxp-decurtis.com/{}"
    ut = "https://kapilkgupta-sagar.developer.dxp-decurtis.com/{}"
    ConfigCompare(ref, ut)
