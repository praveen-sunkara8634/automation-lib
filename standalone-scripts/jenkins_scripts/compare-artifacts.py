"""
This Script will ReBuild a Job when Job Name and number are given to it
"""

import argparse
import re
import json
import os
import time
from decurtis.jenkins import JenkinsAutomation
from decurtis.slack import SlackNotification

parser = argparse.ArgumentParser()
parser.add_argument("--server", default='http://jenkinsqa.dxp-decurtis.com', type=str, required=False, help="Server")
parser.add_argument("--name", default='Pull-Request-Env', type=str, required=False, help="Job name")
args = parser.parse_args()

server = args.server
username = os.environ.get("JENKINS_USER", None)
password = os.environ.get("JENKINS_PASS", None)
name = args.name

if username is None or password is None:
    raise Exception("ERROR: username and password are required to access Jenkins !!")

jenkins = JenkinsAutomation(base_url=server, username=username, password=password)
slack = SlackNotification()
data = {
    'dev': {
        'ship': [],
        'shore': []
    },
    'qa1': {
        'ship': [],
        'shore': []
    },
    'dc': {
        'ship': [],
        'shore': []
    }
}

while True:
    _links = jenkins.get_last_success_build_artifact_links(name)
    for artifact_name in _links.keys():
        artifact_url = _links[artifact_name]
        regExp = re.search(r'(\w+?)_(\w+?)_', artifact_name, re.I | re.M)
        env = regExp.group(1)
        core = regExp.group(2)
        _new_data = jenkins.get_artifact_data_from_jenkins(artifact_url)
        if len(data[env][core]) == 0:
            data[env][core] = _new_data
            message = "{} {} -> {}".format(env, core, _new_data)
            print(message)
            slack.send_message(message, ['dxp-version-changes'])
        else:
            for _code_data in data[env][core]:
                _new_version = _new_data[_code_data]
                _old_version = data[env][core][_code_data]
                if _new_version != _old_version:
                    message = "{} {} {} {} -> {}".format(env, core, _code_data, _old_version, _new_version)
                    print(message)
                    slack.send_message(message, ['dxp-version-changes'])

    time.sleep(10)
