import argparse
import os
from decurtis.jenkins import JenkinsAutomation

parser = argparse.ArgumentParser()
parser.add_argument("--server", default='http://jenkinsqa1.dxp-decurtis.com', type=str, required=True,
                    help="Jenkins Server")
parser.add_argument("--name", default='dxp-e2e', type=str, required=True, help="Job name")
args = parser.parse_args()

server = args.server
name = args.name
username = os.environ.get("JENKINS_USER", None)
password = os.environ.get("JENKINS_PASS", None)

jenkins = JenkinsAutomation(base_url=server, username=username, password=password)
jenkins.delete_successful_jobs(name)
