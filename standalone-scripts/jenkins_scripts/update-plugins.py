from jenkins import Jenkins as JenkinsApi
import argparse
import os

parser = argparse.ArgumentParser()
parser.add_argument("--server", default='http://qa.dxp-decurtis.com:8080/', type=str, required=False, help="Server")
args = parser.parse_args()
server = args.server
username = os.environ.get("JENKINS_USER", None)
password = os.environ.get("JENKINS_PASS", None)

jenkins = JenkinsApi(url=server, username=username, password=password)

plugins = jenkins.get_plugins()
to_update = [plugins.items_dict[x] for x in plugins.items_dict if plugins.items_dict[x]['hasUpdate'] is True]

for _update in to_update:
    name = _update['shortName']
    try:
        print("Updating {} Plugin".format(name))
        status = jenkins.install_plugin(name)
    except Exception as exp:
        pass

print("Please Restart Jenkins to make updated plugins active ...")
