import json
import os
import requests
from decurtis.tokens import OciTokens
from decurtis.common import get_version

with open('../test_data/services_owners.json', 'r') as fp:
    data = json.load(fp)


def get_request(url, rest_oci):
    headers = {
        'Authorization': rest_oci.bearer_token
    }
    response = requests.request('GET', url, headers=headers)
    if response.ok:
        try:
            content = json.loads(response.content)
        except:
            content = response.content
        _version = get_version(content)
        return _version
    else:
        return None


_ship = 'https://dev-sagar.dxp-decurtis.com/'
_shore = 'https://dev-shore.dxp-decurtis.com/'

rest_oci = OciTokens(_ship, _shore, "DXP")

_final = {
    "ship": [],
    "shore": []
}

_versions = dict()

for _data in data:
    for _shipShore in [_ship, _shore]:
        for _call in ['info', 'version']:
            url = "{}{}/{}".format(_shipShore, _data, _call)
            version = get_request(url, rest_oci)
            if version:
                _versions[url] = version
                if 'sagar' in _shipShore:
                    _final['ship'].append("{}/{}".format(_data, _call))
                else:
                    _final['shore'].append("{}/{}".format(_data, _call))
                print("{} -> {}".format(url, version))

_final['shore'] = sorted(_final['shore'])
_final['ship'] = sorted(_final['ship'])

with open('final.json', 'w') as fp:
    fp.write(json.dumps(_final))
print(_final)

with open('versions.json', 'w') as fp:
    fp.write(json.dumps(_versions))
print(_versions)
