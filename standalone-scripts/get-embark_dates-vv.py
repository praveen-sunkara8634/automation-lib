"""
This standalone script will pull active voyages from system
"""
import json
import argparse
from datetime import datetime
from datetime import timedelta
from decurtis.common import urljoin, read_creds_file
from decurtis.bitbucket import BitBucketApi
from decurtis.workers import get_credentials
from decurtis.tokens import OciTokens


class GetEmbarkDates:
    """
    Class to get Voyages for VV Int
    """

    def __init__(self, ship, shore, platform="VIRGIN", environment="integration"):
        """
        Init Function to initialise the Variables
        :param ship:
        :param shore:
        :param platform:
        :param environment:
        """
        self.ship = ship
        self.shore = shore
        self.platform = platform
        self.environment = environment
        self.bb = BitBucketApi()
        self.creds = get_credentials(self.bb, platform, environment, self.ship, self.shore)
        self.auth = self.creds.oci
        self.request = self.rest_oci()
        self.get_future_voyages()

    @staticmethod
    def credentials():
        """
        Creds Fixture with all Credentials
        :return:
        """
        return read_creds_file()

    def rest_oci(self):
        """
        Rest Session Fixture for OCI
        :return:
        """
        # Token Generation is different for Virgin and DXP
        response = OciTokens(ship=self.ship, shore=self.shore, platform=self.platform, auth=self.auth)
        return response

    def get_future_voyages(self):
        """
        Get Future Voyages
        :return:
        :rtype:
        """
        _now = datetime.now().date()
        _later = datetime.now().date() + timedelta(days=10)
        url = urljoin(self.ship, "dxpcore/voyages?page=1&size=300")
        _content = self.request.send_request(method="GET", url=url, auth="bearer").content
        voyages = sorted(_content['_embedded']['voyages'], key=lambda i: i['embarkDate'])
        for count, voyage in enumerate(voyages):
            if voyage['isActive']:
                embark_dates = []
                embark_datess = voyages[count + 1]['embarkDate'].split('T')[0]
                embark_dates.append(embark_datess)
                break
        else:
            raise Exception("There's no active voyage in system !!")
        with open('embarkDates.json', 'w') as fp:
            fp.write(json.dumps(embark_dates, indent=1))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ship", default='https://application-integration.ship.virginvoyages.com/svc/', type=str,
                        required=False, help="Ship")
    parser.add_argument("--shore", default='https://integration.virginvoyages.com/svc/', type=str, required=False,
                        help="Shore")
    args = parser.parse_args()
    GetEmbarkDates(ship=args.ship, shore=args.shore, platform="VIRGIN", environment='integration')
