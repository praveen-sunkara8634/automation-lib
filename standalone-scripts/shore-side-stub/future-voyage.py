import os
import json
import argparse

import re
from decurtis.database import Database
from datetime import datetime, timedelta
from decurtis.bitbucket import BitBucketApi
from decurtis.workers import get_credentials


class FutureVoyage:
    """
    Class to perform Future Voyage Data Rollover automation
    """

    def __init__(self, ship, shore):
        """
        Connect to Database
        """
        self.pat_3d = re.compile(r"^DX(.*?)3DCARIBBEAN$", re.I | re.M)
        self.pat_4d = re.compile(r"^DX(.*?)4DCARIBBEAN$", re.I | re.M)
        self.pat_all = re.compile(r"^DX(.*?)CARIBBEAN$", re.I | re.M)

        self.date_today = datetime.utcnow().strftime("%Y%m%d")
        self.ship = ship
        self.shore = shore
        self.platform = 'DXP'
        self.env = re.search(r"https://(.*?)-shore", self.shore, re.I | re.M).group(1)
        self.bb = BitBucketApi()
        if self.env not in ['qa1', 'dev', 'dc']:
            self.envMasked = 'dev'
        else:
            self.envMasked = self.env

        self.database = get_credentials(self.bb, self.platform, self.envMasked, ship=self.ship, shore=self.shore)

        self.dxpcore = Database(
            host=self.database.shore.db.host,
            username=self.database.shore.db.username,
            password=self.database.shore.db.password,
            port=self.database.shore.db.port,
            database='dxpcore'
        )

        self.stub = Database(
            host=self.database.shore.db.host,
            username=self.database.shore.db.username,
            password=self.database.shore.db.password,
            port=self.database.shore.db.port,
            database='shoresidesorstub'
        )

        self.last_file = None
        self.next_file = None

        self.query_3d = []
        self.query_4d = []

        self.get_last_bit_bucket_file()

        # Get Next 3 voyages from dxp core
        self.max_days = 3
        self.next_voyages = self.get_next_voyages()

        self.get_voyages_to_remove()
        self.write_final_file()

    def get_last_bit_bucket_file(self):
        """
        Get Bitbucket Files
        """
        bb = BitBucketApi()
        files = bb.get_list_of_files(client='decurtis', repo='dxp-masterdatadxp',
                                     path='src/master/Databases/shoresidesorstub/UpgradeScripts/Scripts/')
        self.last_file = os.path.basename(
            sorted([x['path'] for x in files if str(x['path']).endswith('sql')])[-1]).replace('.sql', '')
        self.next_file = f"{str(self.last_file).split('.')[0]}.{int(str(self.last_file).split('.')[1]) + 1}"

    def get_next_voyages(self):
        """
        Read dxp core db and get next 3 voyages
        """
        voyages = self.dxpcore.run_and_fetch_data(
            '''select * from voyage where shipcode='DX' and embarkdate>now()::date order by embarkdate;''')

        # Prepare stub for next set of voyages 3-Days
        return sorted([x['number'] for x in voyages if re.search(self.pat_all, x['number'])])

    def get_voyages_to_remove(self):
        """
        Get voyages to be removed
        """
        voyages = self.stub.run_and_fetch_data(
            '''SELECT * FROM requestresponse where ("parameter" ilike 'DX%') order by parameter;'''
        )

        voyages = [x for x in voyages if re.search(self.pat_all, x['parameter'])]
        self.query_3d = self.filter_voyages(
            voyages=[x['parameter'] for x in voyages if re.search(self.pat_3d, x['parameter'])],
            pattern=self.pat_3d,
            sailing_days=3
        )
        self.query_4d = self.filter_voyages(
            voyages=[x['parameter'] for x in voyages if re.search(self.pat_4d, x['parameter'])],
            pattern=self.pat_4d,
            sailing_days=4
        )

    def filter_voyages(self, voyages, pattern, sailing_days):
        """
        Filter Voyages as per patterns
        :param voyages
        :param pattern
        :param sailing_days
        """
        next_voyages = [x for x in self.next_voyages if re.search(pattern, x)]
        final = []
        to_remove = [x for x in voyages if re.search(pattern, x).group(1) < self.date_today]
        to_keep = [x for x in voyages if re.search(pattern, x).group(1) >= self.date_today]
        for _keep in to_keep:
            voyage_date = re.search(pattern, _keep)
            embark = str(datetime.strptime(voyage_date.group(1), '%Y%m%d').strftime('%Y-%m-%d'))
            debark = str(
                (datetime.strptime(voyage_date.group(1), '%Y%m%d') + timedelta(days=sailing_days)).strftime('%Y-%m-%d'))
            final.append(f"'{embark}', '{debark}', '{_keep}', '{_keep}'")

        # Remove all voyages which we want to keep as they have been processed already
        next_voyages = sorted(list(set(next_voyages) - set(to_keep)))

        for _remove in to_remove:
            _next = next_voyages.pop(0)
            voyage_date = re.search(pattern, _next)
            embark = str(datetime.strptime(voyage_date.group(1), '%Y%m%d').strftime('%Y-%m-%d'))
            debark = str(
                (datetime.strptime(voyage_date.group(1), '%Y%m%d') + timedelta(days=sailing_days)).strftime('%Y-%m-%d'))
            final.append(f"'{embark}', '{debark}', '{_remove}', '{_next}'")

        return final

    def write_final_file(self):
        with open('standalone-scripts/shore-side-stub/header.sql', 'r') as _header:
            header = _header.read().format(self.last_file)

        with open('standalone-scripts/shore-side-stub/footer.sql', 'r') as _footer:
            footer = _footer.read().format(self.next_file)

        # Write Next File Name in Json, so that it can be consumed by Jenkins
        with open('next-file.json', 'w') as _fp:
            _fp.write(json.dumps({"next-file": f"{self.next_file}.sql"}))

        with open(f"{self.next_file}.sql", 'w') as _fp:
            _fp.write(header)
            if len(self.query_3d) > 0:
                _fp.write('''
perform  public.insertfuturevoyage_3DCaribbean
('3D Caribbean',
/*new voyage embark date, new voyage debark date, current/prev voyage number, future voyage number*/
''')
                _fp.write(",\n".join(self.query_3d))
                _fp.write('''
);

''')
            if len(self.query_4d) > 0:
                _fp.write('''
perform public.insertfuturevoyage_4DCaribbean
('4D Caribbean',
/*new voyage embark date, new voyage debark date, current/prev voyage number, future voyage number*/
''')
                _fp.write(",\n".join(self.query_4d))
                _fp.write('''
);

''')
            _fp.write(footer)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--shore", default='https://anshumangoyal-shore.developer.dxp-decurtis.com', type=str,
                        required=False, help="Shore Link")
    parser.add_argument("--ship", default='https://anshumangoyal-sagar.developer.dxp-decurtis.com', type=str,
                        required=False, help="Ship Link")
    args = parser.parse_args()
    FutureVoyage(args.ship, args.shore)
