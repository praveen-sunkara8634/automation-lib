from decurtis.slack import SlackNotification

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--users", default='anshuman.goyal@decurtis.com', type=str, required=True, help="Users to Tag")

args = parser.parse_args()

users = str(args.users).lower().split(',')
slack = SlackNotification()

if "ciduser@decurtis.com" in users:
    users.append('piyush.kumar@decurtis.com')
if "vertical-qa@decurtis.com" in users:
    users.append('piyush.kumar@decurtis.com')
users = list(set(users))

_tag_string = []
for _user in users:
    for _user_list in slack.users_list:
        if 'email' in _user_list['profile']:
            if '@' in _user and _user_list['profile']['email'] == _user:
                _tag_string.append("<@{}>".format(_user_list['id']))

if len(_tag_string) > 0:
    _tag_string = " ".join(_tag_string)

with open('users', 'w') as fp:
    fp.write(_tag_string)
