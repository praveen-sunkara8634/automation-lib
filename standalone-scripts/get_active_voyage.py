import psycopg2
import requests
import uuid
import json
import re
import urllib.parse as urlparse
from datetime import datetime
from datetime import timedelta
from pytz import timezone
from dateutil.relativedelta import relativedelta
import time


class CursorByName():
    def __init__(self, cursor):
        self._cursor = cursor

    def __iter__(self):
        return self

    def __next__(self):
        row = self._cursor.__next__()

        return {description[0]: row[col] for col, description in enumerate(self._cursor.description)}


class GetActiveVoyage:
    def __init__(self, ship, shore):
        """
        Init the Environment where this has to be used
        :param environment:
        """

        # self.db_server = "postgresql.{}".format(shore)
        self.db_host = "35.229.92.86"
        self.db_port = 30889
        self.db_user = 'postgres'
        self.db_pass = 'Hu8#de3'
        self.token = "NDllMTQ2NzEtMzJkMy00MWU4LWI1NzktZDc0N2I1NGZmZDIzOnNlY3JldA=="

        # Generate Token Link
        self.ship_time = "https://{}/dxpcore/shiptimes/7ab60bb8-db1f-48fa-8033-be8fd52798ad".format(ship)
        self.ship_service = "https://{}/dxpcore/voyages/".format(ship)
        self.token_link = "https://{}/reservation-bff/auth".format(shore)
        self.couch_time = "http://{}/syncgatewayfeedersadmin/prodappdocstore/ShipTime::{}".format(ship, 'DX')

        # Get Token
        self._get_token()

        # Make db Connection
        self.connection = self._db_connection()

        # Get embark and debark dates
        self.embark_date = None
        self.debark_date = None
        self.get_embark_debark_date()

        # Calculate Offset
        self.offset = self.get_offset()

        # Get Next Voyage ID
        self.next_voyage_id = self._get_next_voyage_id()

        # Run Voyage Roll Over
        self.execute_voyage_rollover()

        # Verify Voyage Rollover
        self.verify_voyage_roll_over()

        # Update Ship Time
        self.update_ship_time()

        assert self.verify_ship_time(), "ERROR: Ship time not updated !!"

    def _get_token(self):
        headers = {
            "Authorization": "Basic {}".format(self.token),
            "correlationId": str(uuid.uuid4())
        }
        response = requests.get(self.token_link, headers=headers)
        self.token = response.json()["accessToken"]

    def _db_connection(self):
        """
        Connect to DB and send connection object back to user
        :param db_server:
        :param db_user:
        :param db_pass:
        :return:
        """
        connection = psycopg2.connect(
            host=self.db_host,
            port=self.db_port,
            user=self.db_user,
            password=self.db_pass,
            database="ship"
        )
        return connection

    def _db_run_query(self, query, all=False):
        """
        Run a query on Database Connection
        :param query:
        :return:
        """
        cursor = self.connection.cursor()
        cursor.execute(query)
        # for row in CursorByName(cursor):
        #     print(row)
        if all:
            data = cursor.fetchall()
        else:
            data = cursor.fetchone()
        return data

    def _get_next_voyage_id(self):
        """
        Get Next Voyage ID from Database
        :return:
        """
        query = "select voyageid from voyage where embarkdate = (select debarkdate from voyage where isactive=true)"
        data = self._db_run_query(query, all=True)
        return data[0]

    def _get_next_voyage_details(self):
        url = urljoin(self.ship_service, self.next_voyage_id)
        headers = {
            "Authorization": "bearer {}".format(self.token),
            "correlationId": str(uuid.uuid4())
        }
        response = requests.get(url, headers=headers)
        return response.json()

    def _prepare_payload(self):
        modified_voyage_details = dict()
        voyage_details = self._get_next_voyage_details()
        for _data in voyage_details:
            if _data not in ["createdByUser", "creationTime", "modificationTime", "_links"]:
                modified_voyage_details[_data] = voyage_details[_data]

        for _count, _data in enumerate(voyage_details['voyageItineraries']):
            for _to_remove in ["createdByUser", "creationTime", "modificationTime"]:
                del modified_voyage_details['voyageItineraries'][_count][_to_remove]

        modified_voyage_details['isActive'] = True
        modified_voyage_details['isVoyageRollover'] = True

        return modified_voyage_details

    def execute_voyage_rollover(self):
        """
        Execute Voyage Roll Over
        :return:
        """
        postHeader = {
            "Authorization": "bearer {}".format(self.token),
            "Content-Type": "application/json"
        }
        data = self._prepare_payload()
        print("Executing Voyage Rollover ...")
        response = requests.post(self.ship_service, data=json.dumps(data), headers=postHeader)
        return response

    def verify_voyage_roll_over(self):
        """
        Verify Voyage Roll Over
        :return:
        """
        while True:
            url = urljoin(self.ship_service, self.next_voyage_id)
            header = {
                "Authorization": "bearer {}".format(self.token),
                "correlationId": str(uuid.uuid4())
            }
            response = requests.get(url, headers=header)
            if response.json()['isActive']:
                print("Voyage Rollover Successful ...")
                break
            else:
                print("Waiting for Voyage Rollover ...")
                time.sleep(10)

    def get_embark_debark_date(self):
        """
        Get Embark Debark Date
        :return:
        """

        window = [str(datetime.utcnow().date() + timedelta(days=x)) for x in range(0, 5)]
        # query = "select * from voyage"
        query = "select embarkdate,debarkdate,voyageid,shipid,isactive,shipcode from voyage where isdeleted=false"
        data = self._db_run_query(query, all=True)
        for _data in data:
            embarkDate = _data[0].strftime("%Y-%m-%d")
            debarkDate = _data[1].strftime("%Y-%m-%d")
            voyageid = _data[2]
            shipid = _data[3]
            isactive = _data[4]
            shipcode = _data[5]
            for _win in window:
                if embarkDate == _win:
                    print("isactive: {} | voyageid: {} | shipid: {}".format(isactive, voyageid, shipid))
        self.embark_date = data[0]
        self.debark_date = data[1]

    def get_offset(self):
        """
        Get Time Offset from Current Time
        :return:
        """
        utcNowTZ = timezone('UTC').localize(datetime.strptime(str(self.embark_date), "%Y-%m-%d %H:%M:%S"))
        usDate = utcNowTZ.astimezone(timezone('US/Eastern')).replace(tzinfo=None)
        utcNow = datetime.utcnow().date()
        offset = relativedelta(usDate, utcNow)
        return (offset.days * 24 * 60) + (offset.hours * 60) + offset.minutes

    def update_ship_time(self):
        """
        Update Ship Time
        :return:
        """
        data = {
            "shipTimeId": str(uuid.uuid4()),
            "shipId": str(uuid.uuid4()),
            "fromUtcDate": "{}T{}.000+0000".format(str(datetime.utcnow().date()), str(datetime.utcnow().time())[0:8]),
            "fromDateOffset": str(self.offset),
            "isDeleted": False,
            "shipCode": self.ship
        }

        header = {
            "Authorization": "bearer {}".format(self.token),
            "Content-Type": "application/json"
        }
        print("Updating Ship Time ...")
        response = requests.put(self.ship_time, data=json.dumps(data), headers=header)
        return response

    def verify_offset_in_core(self):
        query = "select fromdateoffset from shiptime"
        data = self._db_run_query(query)
        return data[0] == self.offset

    def verify_offset_in_couch(self):
        response = requests.get(self.couch_time)
        return response.json()["FromDateOffset"] == self.offset

    def verify_ship_time(self):
        while True:
            if self.verify_offset_in_core() and self.verify_offset_in_couch():
                return True
            else:
                print("Waiting for Ship Time to update ...")
                time.sleep(5)


if __name__ == "__main__":
    GetActiveVoyage(
        ship="anshumangoyal-sagar.developer.dxp-decurtis.com",
        shore="anshumangoyal-shore.developer.dxp-decurtis.com"
    )
