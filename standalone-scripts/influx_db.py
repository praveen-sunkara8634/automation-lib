import uuid
from influxdb import InfluxDBClient
from datetime import datetime, timezone


class InfluxDB:
    """
    Class to interact with Influx DB
    """

    def __init__(self, host='localhost', port=8086, username='root', password='root'):
        """
        Init function to make a connection with influx db
        :param host:
        :param port:
        :param username:
        :param password:
        """
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.client = InfluxDBClient(
            host=self.host,
            port=self.port,
            username=self.username,
            password=self.password,
            # ssl=True, verify_ssl=True
        )

    def get_list_of_dbs(self):
        """
        Get list of all DB's
        :return:
        """
        return self.client.get_list_database()

    def switch_database(self, db_name):
        """
        Switch to database, create if it is not there
        :param db_name:
        :return:
        """
        for db_already_present in self.get_list_of_dbs():
            if db_already_present['name'] == db_name:
                print("DB {} already present, switching".format(db_name))
                self.client.switch_database(db_name)
                break
        else:
            self.client.create_database(db_name)
            self.client.switch_database(db_name)

    def insert_json_data_in_database(self, json_data):
        """
        Insert Json Data in Database Table
        :param json_data:
        :return:
        """
        if isinstance(json_data, list):
            self.client.write_points(json_data)

    def delete_series(self, measurement):
        """
        Delete Series of Database
        :param measurement:
        :return:
        """
        self.client.delete_series(measurement=measurement)


if __name__ == "__main__":
    db = InfluxDB()
    db.switch_database('_internal')
    # db.delete_series(measurement='Job-Status')
    # db.delete_series(measurement='PR-Status')
    data = [
        {
            "measurement": "PR-Status",
            "tags": {
                "Component": 'dxp-fnb-venuemanager',
                "status": "Success",
                "uuid": str(uuid.uuid4()),
            },
            "time": datetime.now(timezone.utc).isoformat(),
            "fields": {
                "Composer": 'sudeep.pal',
                "Reason": 'Build passed against test cases',
                "Time-Taken": '25 minutes',
            }
        }
    ]

    db.insert_json_data_in_database(data)
