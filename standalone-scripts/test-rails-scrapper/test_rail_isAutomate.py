__author__ = 'vishesh.teng'
__maintainer__ = 'anshuman.goyal'

from decurtis.test_rails import Testrail
from decurtis.slack import SlackNotification
from decurtis.common import *
from datetime import datetime
from decurtis.bitbucket import BitBucketApi

# To get all the test cases id in repo
test_cases_repo = []
directory = 'test_scripts'
repos = ['dxp-e2e-automation', 'vv-e2e-automation']
for repo in repos:
    bb = BitBucketApi()
    for _file in bb.get_list_of_files(client='decurtis', repo=repo, path='/src/master/test_scripts'):
        if re.match(r'^test_', str(_file['path']).split('/')[1], re.I | re.M):
            data = bb.read_remote_file(client='decurtis', repo=repo, file_path=_file['links']['self']['href'])
            test_cases_repo.extend(re.findall(r'@pytestrail.case\(\'(\d+)\'\)', data))

# To get all the test cases in test Rail
slack = SlackNotification()
creds = read_creds_file()

test_rail = Testrail(url=creds.test_rails.url, username=creds.test_rails.email, password=creds.test_rails.api_key)
all_users = test_rail.get_users()
all_cases = test_rail.get_cases_in_project()
for case in all_cases:
    if str(case['id']) in test_cases_repo:
        url = f"https://decurtis.testrail.io/index.php?/cases/view/{case['id']}"
        user = [x for x in all_users if x['id'] == case['created_by']][0]
        if case['custom_isautomated'] == 2:
            current_date = datetime.strptime(str(datetime.today().date()), '%Y-%m-%d').strftime('%m/%d/%Y')
            _body = {
                'custom_isautomated': 1,
                'custom_automateddate': current_date
            }
            test_rail.update_test_case(case_id=case['id'], data=_body)
            # Send Slack Notification to user who made this test-case
            _id = slack.get_id(user['email'])
            slack.send_message(message=f"Hey !! {_id} <{url}|{case['id']}> automation flag updated ...",
                               recipients='testrail-notification')
