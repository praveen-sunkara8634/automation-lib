__author__ = 'vishesh.teng'
__maintainer__ = 'anshuman.goyal'

from decurtis.test_rails import Testrail
from decurtis.common import read_creds_file

creds = read_creds_file()
test_rail = Testrail(url=creds.test_rails.url, username=creds.test_rails.email, password=creds.test_rails.api_key)

priority_map = test_rail.get_priorities(True)
type_map = test_rail.get_types(False)
all_cases = test_rail.get_cases_in_project()
for _case in all_cases:
    """
    # Set Priority for test-cases
    if priority_map[_case['priority_id']] == 'Critical':
        if _case['type_id'] != type_map['Smoke & Sanity']:
            print("{:6d}: Critical -> Smoke & Sanity".format(_case['id']))
            test_rail.update_test_case(case_id=_case['id'], type_id=type_map['Smoke & Sanity'])
    else:
        if _case['type_id'] != type_map['Regression']:
            print("{:6d}: Others -> Regression".format(_case['id']))
            test_rail.update_test_case(case_id=_case['id'], type_id=type_map['Regression'])
    """

    """
    # Set CI/CD Running Date for test-cases
    if _case['custom_isautomated'] == 1 and _case['custom_cicdrunning'] == 1 and _case['custom_cidate'] is None:
        print("case: {:4d}".format(_case['id']))
        test_rail.update_test_case(case_id=_case['id'], custom_cidate='10/18/2019')
    elif _case['custom_isautomated'] != 1 and _case['custom_cicdrunning'] != 1 and _case['custom_cidate'] is not None:
        # print("case: {:4d}".format(_case['id']))
        test_rail.update_test_case(case_id=_case['id'], custom_cidate=None)
    """
