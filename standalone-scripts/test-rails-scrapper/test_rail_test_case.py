__author__ = 'vishesh.teng'
__maintainer__ = 'anshuman.goyal'

from decurtis.test_rails import Testrail
from decurtis.slack import SlackNotification
from decurtis.common import *
from datetime import datetime, timedelta

slack = SlackNotification()
creds = read_creds_file()

test_rail = Testrail(url=creds.test_rails.url, username=creds.test_rails.email, password=creds.test_rails.api_key)
db = Database(
    host=creds.qa_postgres.host,
    username=creds.qa_postgres.username,
    password=creds.qa_postgres.password,
    database=creds.qa_postgres.database
)
table = 'testraildata'
all_users = test_rail.get_users()
for _count, _user in enumerate(all_users):
    if '@decurtis.com' in _user['email'] and 'test@decurtis.com' not in _user['email']:
        _id = slack.get_user_by_email(_user['email'])
        _user = {**_user, **{'slack': f"<@{_id}>"}}
        all_users[_count] = _user

db.truncate_data(table=table)
all_cases = sorted(test_rail.get_cases_in_project(), key=lambda x: x['updated_on'], reverse=True)
all_suites = test_rail.get_test_suits(project_id=1)
dateTimeObj = datetime.now() - timedelta(days=7)
duration = dateTimeObj.strftime("%Y-%m-%d %H:%M:%S")
condition = f"WHERE 'time' < '{duration}';"
db.delete_data_query(table='component_report', condition=condition)

for _count, case in enumerate(all_cases, start=1):
    print(f"Processing {_count} of {len(all_cases)}")
    in_dxp = '0'
    in_vv = '0'
    name = 'testrail-notification'
    suite_id = str(case['suite_id'])
    user_id = case['updated_by']
    case_id = str(case['id'])
    for suite in all_suites:
        if str(suite['id']) == suite_id:
            suite_name = suite['name']
            break
    else:
        raise Exception("Suite name not available !!")

    priority = str(case['priority_id'])
    created = case['created_on']
    section = case['section_id']
    created_on = str(datetime.fromtimestamp(created))
    if case['custom_cidate'] is None:
        ci_date = None
    else:
        ci_date = f"{case['custom_cidate']} T00:00:00"
    if case['custom_automateddate'] is None:
        automated_date = None
    else:
        automated_date = f"{case['custom_automateddate']} T00:00:00"
    if case['custom_cicdrunning'] is None:
        in_ci = '0'
    else:
        in_ci = str(case['custom_cicdrunning'])
    if case['custom_isautomated'] is None:
        automated = '0'
    else:
        automated = str(case['custom_isautomated'])

    if 'custom_customer' not in case or len(case['custom_customer']) == 0:
        for user in all_users:
            if user['id'] == user_id:
                _case = f"<https://decurtis.testrail.io/index.php?/cases/view/{case['id']}|{case['id']}>"
                _suite = f"<https://decurtis.testrail.io/index.php?/suites/view/{suite['id']}|{suite_name}>"
                message = f"{user['slack']} Customer Missing | Case: {_case} | Suite: {_suite}"
                slack.send_message(message, name)
    else:
        if 4 in case['custom_customer']:
            in_dxp = '1'
        if 6 in case['custom_customer']:
            in_vv = '1'
    if in_ci == '1' and automated == '0':
        for user in all_users:
            if user['id'] == user_id:
                _case = f"<https://decurtis.testrail.io/index.php?/cases/view/{case['id']}|{case['id']}>"
                _suite = f"<https://decurtis.testrail.io/index.php?/suites/view/{suite['id']}|{suite_name}>"
                message = f"{user['slack']} Automated flag Missing | Case: {_case} | Suite: {_suite}"
                slack.send_message(message, name)
    if automated == '1' and automated_date is None:
        for user in all_users:
            if user['id'] == user_id:
                _case = f"<https://decurtis.testrail.io/index.php?/cases/view/{case['id']}|{case['id']}>"
                _suite = f"<https://decurtis.testrail.io/index.php?/suites/view/{suite['id']}|{suite_name}>"
                message = f"{user['slack']} Automated date Missing | Case: {_case} | Suite: {_suite}"
                slack.send_message(message, name)
    if in_ci == '1' and ci_date is None:
        for user in all_users:
            if user['id'] == user_id:
                _case = f"<https://decurtis.testrail.io/index.php?/cases/view/{case['id']}|{case['id']}>"
                _suite = f"<https://decurtis.testrail.io/index.php?/suites/view/{suite['id']}|{suite_name}>"
                message = f"{user['slack']} CI date Missing | Case: {_case} | Suite: {_suite}"
                slack.send_message(message, name)
    query = {
        'case_id': case_id, 'priority': priority, 'created': created_on, 'suite': suite_id, 'section': section,
        'automated': automated, 'in_ci': in_ci, 'ci_date': ci_date, 'suite_name': suite_name,
        'automation_date': automated_date, 'in_dxp': in_dxp, 'in_vv': in_vv
    }
    db.insert_test_rail_data(query=query)
