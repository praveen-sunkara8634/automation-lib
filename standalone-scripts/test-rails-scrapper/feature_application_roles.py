import xlrd
import os

# Give the location of the file
loc = os.path.abspath('test_data/ACI + Gangway ApplicationFeature and ApplicationRoles.xlsx')

# To open Workbook
wb = xlrd.open_workbook(loc)
sheet = wb.sheet_by_index(0)
data = []
for row in range(sheet.nrows):
    for column in range(sheet.ncols):
        if sheet.cell_value(row, column) is 'x':
            text = f"Union all Select '{sheet.cell_value(0, column)}', '{sheet.cell_value(row, 6)}' "
            data.append(text)
with open('query.txt', 'w') as _fp:
    for item in data:
        _fp.write(f"{item}\n")
