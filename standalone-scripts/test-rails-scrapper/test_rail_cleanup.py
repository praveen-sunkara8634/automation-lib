__author__ = 'vishesh.teng'
__maintainer__ = 'anshuman.goyal'

from datetime import datetime, timedelta
from decurtis.test_rails import Testrail
from decurtis.slack import SlackNotification
from decurtis.common import read_creds_file

creds = read_creds_file()
test_rail = Testrail(url=creds.test_rails.url, username=creds.test_rails.email, password=creds.test_rails.api_key)

slack = SlackNotification()

users = [x for x in test_rail.get_users() if x['email'] == 'test@decurtis.com'][0]['id']
to_delete = dict()
to_close = dict()
done = False
offset = 0
while done is False:
    from_date = datetime.utcnow().date() - timedelta(days=2)
    content = test_rail.get_plans(project_id=1, offset=offset)
    if len(content) >= 250:
        offset += len(content)
    else:
        done = True
    for plan in [x for x in content if x['is_completed'] is False and x['created_by'] == users]:
        passed = plan['passed_count']
        blocked = plan['blocked_count']
        retest = plan['retest_count']
        untested = plan['untested_count']
        failed = plan['failed_count']
        created_on = datetime.fromtimestamp(plan['created_on']).date()

        if str(plan['name']).startswith('Sprint'):
            from_date -= timedelta(days=12)

        if created_on > from_date:
            continue
        else:
            if passed == 0:
                to_delete[plan['id']] = plan

            if failed == 1:
                to_close[plan['id']] = plan

            # Close test plans which have zero untested test-cases
            if untested == 0:
                to_close[plan['id']] = plan

            if (untested + passed) > 0 and int(passed / (untested + passed)) < 25:
                to_close[plan['id']] = plan

for _, _close in to_close.items():
    if _close['id'] not in to_delete.keys():
        print(f"Closing Test Plan: {_close['id']} => {_close['name']}")
        test_rail.close_test_plan(plan_id=_close['id'])
        slack.send_message(message=f"Test-Rails-Cleanup: <{_close['url']}|{_close['name']}> Closed ...",
                           recipients='testrail-notification')
    else:
        print(f"Not Closing Test Plan: {_close['id']} as it is set to delete ...")

for _, _delete in to_delete.items():
    print(f"Deleting Test Plan: {_delete['id']} => {_delete['name']}")
    test_rail.delete_test_plan(plan_id=_delete['id'])
    slack.send_message(message=f"Test-Rails-Cleanup: <{_delete['url']}|{_delete['name']}> Deleted ...",
                       recipients='testrail-notification')
