__author__ = 'vishesh.teng'
__maintainer__ = 'anshuman.goyal'

import pytz
from datetime import datetime
from decurtis.test_rails import Testrail
from decurtis.common import read_creds_file

creds = read_creds_file()
test_rail = Testrail(url=creds.test_rails.url, username=creds.test_rails.email, password=creds.test_rails.api_key)

now = int(datetime.now(pytz.timezone(pytz.utc.zone)).replace(hour=0, minute=0, second=0, microsecond=0).astimezone(
    pytz.utc).timestamp())

_one_day = 3600 * 24
_one_week = 3600 * 24 * 7
_one_month = 3600 * 24 * 30

_window = []
for x in range(1, 30):
    automated_count = 0
    ci_cd_count = 0
    start_time = now - (_one_day * x)
    end_time = now - (_one_day * (x - 1))
    _from = datetime.fromtimestamp(start_time).strftime("%a %d-%b-%Y")
    _to = datetime.fromtimestamp(end_time).strftime("%a %d-%b-%Y")
    date_range = "{} to {}".format(_from, _to)
    all_cases = test_rail.get_filtered_cases_in_project(start_time, end_time)
    for _case in all_cases:
        if _case['custom_isautomated'] == 1:
            automated_count += 1
        if _case['custom_cicdrunning']:
            ci_cd_count += 1

    print("{} | total: {:4d} | automated: {:4d} | ci_cd_count: {:4d}".format(date_range, len(all_cases),
                                                                             automated_count, ci_cd_count))
