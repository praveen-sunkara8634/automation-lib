__author__ = 'vishesh.teng'
__maintainer__ = 'anshuman.goyal'

from decurtis.test_rails import Testrail
from decurtis.database import Database
from decurtis.common import read_creds_file

creds = read_creds_file()
test_rail = Testrail(url=creds.test_rails.url, username=creds.test_rails.email, password=creds.test_rails.api_key)

type_map = test_rail.get_types(True)
suits = test_rail.get_test_suits(project_id=test_rail.project_id)
data = dict()
for suit in suits:
    if suit['name'] not in data:
        data[suit['name']] = {"Smoke & Sanity": {"API": 0, "UI": 0}, "Regression": {"API": 0, "UI": 0}}
    sections = test_rail.get_sections(suit['id'])
    data[suit['name']]['url'] = suit['url']
    for section in sections:
        for case in test_rail.get_section_cases(suite_id=suit['id'], section_id=section['id']):
            if section['parent_id']:
                for _section in sections:
                    if _section['id'] == section['parent_id']:
                        if _section['name'] == 'API Tests':
                            data[suit['name']][type_map[case['type_id']]]["API"] += 1
                        elif _section['name'] == 'UI Tests':
                            data[suit['name']][type_map[case['type_id']]]["UI"] += 1
            else:
                if section['name'] == 'API Tests':
                    data[suit['name']][type_map[case['type_id']]]["API"] += 1
                elif section['name'] == 'UI Tests':
                    data[suit['name']][type_map[case['type_id']]]["UI"] += 1

print(data)

db = Database(
    host=creds.qa_postgres.host,
    username=creds.qa_postgres.username,
    password=creds.qa_postgres.password,
    database=creds.qa_postgres.database
)

for _data in data.keys():
    query = {
        'component': _data,
        'api_smoke': data[_data]['Smoke & Sanity']['API'],
        'api_regression': data[_data]['Regression']['API'],
        'ui_regression': data[_data]['Regression']['UI'],
        'ui_smoke': data[_data]['Smoke & Sanity']['UI'],
        'test_plan': data[_data]['url']
    }
    db.insert_test_cases_data(query=query)

# cases = test_rail.get_section_cases(section_id=section['id'])
# print()
# test_cases = test_rail.get_cases_in_project()
# for _count, _case in enumerate(test_cases):
#     sections = test_rail.get_sections()
