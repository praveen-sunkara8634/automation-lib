__author__ = 'anshuman.goyal'

import requests
import json
from functools import wraps
from decurtis.common import process_response


def refresh_token(method):
    """
    Decorator function to refresh Token
    :param method:
    :return:
    """

    @wraps(method)
    def wrapper_function(self, *method_args, **method_kwargs):
        """
        Wrapper Function to refresh token
        :param args:
        :param kwargs:
        :return:
        """
        data = {
            'grant_type': 'refresh_token',
            'refresh_token': self.refresh_token
        }
        response = process_response(
            self.session.post(url=self.token_api, data=data, auth=(self.client_id, self.secret)))
        self.access_token = response.content['access_token']
        self.header = {
            'Authorization': "Bearer {}".format(self.access_token)
        }
        self.session.headers.update(self.header)
        return method(self, *method_args, **method_kwargs)

    return wrapper_function


class BitBucketScrubber:
    """
    Class to Scrub Data from Bit Bucket
    """

    def __init__(self):
        """
        Init Function which will generate tokens etc.
        """
        self.client_id = 'bP8nwZamdpTJdpb5y6'
        self.secret = 'QzZpAYpxyD5cPtBqKPczBsdfFXeH6TcW'
        self.token_api = 'https://bitbucket.org/site/oauth2/access_token'
        self.session = requests.Session()
        self.session.verify = False
        self.access_token = None
        self.refresh_token = None
        self.get_token()

    def get_token(self):
        """
        Function to get Bit Bucket Token
        :return:
        """
        data = {
            'grant_type': 'client_credentials'
        }
        response = self.session.post(url=self.token_api, data=data,
                                     auth=(self.client_id, self.secret))
        response = process_response(response)
        self.access_token = response.content['access_token']
        self.refresh_token = response.content['refresh_token']
        self.header = {
            'Authorization': "Bearer {}".format(self.access_token)
        }
        self.session.headers.update(self.header)

    # @refresh_token
    def send_get_request(self, url, query=None):
        """
        Function to send get request
        :return:
        """
        params = (('q', query),)
        data = []
        while True:
            if query:
                response = process_response(self.session.get(url=url, params=params))
            else:
                response = process_response(self.session.get(url=url))
            data.extend(response.content['values'])
            if 'next' in response.content:
                url = response.content['next']
            else:
                break
        return data

    # @refresh_token
    def send_post_request(self, url, data):
        """
        Function to send get request
        :return:
        """
        response = process_response(self.session.post(url=url, data=data))
        return response


if __name__ == "__main__":
    bb = BitBucketScrubber()
    repositories = bb.send_request(method="GET", url='https://api.bitbucket.org/2.0/repositories/decurtis',
                                   query='project.key="DXPDEVOPS"')
    for repo in repositories:
        full_name = repo['full_name']
        pull_requests = bb.send_request(method="GET", url=repo['links']['pullrequests']['href'], query='state="MERGED"')
        commits = bb.send_request(method="GET", url=repo['links']['commits']['href'], query='state="MERGED"')
        for commit in commits:
            print()
