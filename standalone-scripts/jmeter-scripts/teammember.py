from common import *

common_requests = CommonRequest()
for root, dirs, files in os.walk("standalone-scripts/jmeter-scripts/"):
    for dir in dirs:
        if "teammember" in dir:
            filepath = f'standalone-scripts/jmeter-scripts/{dir}/'
            dir = dir
            break
service = 'teammember'
script_name = []
reports = []
environment_url = dict()
list = os.sys.argv
properties = dict()
for argument in list:
    if 'ship_url' in argument:
        client = re.search('--ship_url=(.+)', argument).group(1)
        environment_url.update({
            "ship": client
        })
    if 'shore_url' in argument:
        client = re.search('--shore_url=(.+)', argument).group(1)
        environment_url.update({
            "shore": client
        })
    if 'thread' in argument:
        thread = re.search('--thread=(.+)', argument).group(1)
        properties.update({
            "thread": thread
        })
    if 'ramp_time' in argument:
        ramp_time = re.search('--ramp_time=(.+)', argument).group(1)
        properties.update({
            "ramp_time": ramp_time
        })
for key, url in environment_url.items():
    common_requests.create_jmx_files(filepath=filepath, service=service, key=key, url=url, properties=properties,
                                     replacement_path="/dxpcore", context_path="/dxpcore")
    # CSV creation and insertion
    csv_data = common_requests.get_teammember_details(url)
    csv_data_code_types = common_requests.get_code_types(url)
    csv_data_code_values = common_requests.get_code_values(url)
    csv_data_code_duties = common_requests.get_code_duties(url)
    csv_data_code_attributes = common_requests.get_code_attributes(url)
    columns = [['codeTypeId', 'codeValueId', 'dutyId', 'teamMemberId', 'teammembertypesId', 'roleId',
                'attributeId', 'name', 'teamMemberNumber', 'stateroom', 'safetyNumbers', 'dutyTypeId']]
    rows_value = []
    for teammember_array in range(len(csv_data)):
        infirstLoop = False
        if len(csv_data[teammember_array]['teamMemberDuties']) != 0 and 'stateroom' in csv_data[teammember_array]:
            infirstLoop = True
            rows_value = [csv_data_code_types[teammember_array]['codeTypeId'],
                          csv_data_code_values[teammember_array]['codeValueId'],
                          csv_data[teammember_array]['teamMemberDuties'][0]['dutyId'],
                          csv_data[teammember_array]['teamMemberId'],
                          csv_data[teammember_array]['teamMemberTypeId'],
                          csv_data[teammember_array]['teamMemberRoles'][0]['roleId'],
                          '036533f2-ba9a-e611-80c2-00155df80332', csv_data[teammember_array]['firstName'],
                          csv_data[teammember_array]['teamMemberNumber'],
                          csv_data[teammember_array]['stateroom'],
                          csv_data[teammember_array]['safetyDuties'][0]['safetyNumber']]
        if (infirstLoop):
            rows_value.append(csv_data_code_duties[teammember_array]['dutyTypeId'])
        columns.append(rows_value)
        if len(columns) == 10:
            break
    data = common_requests.csv_for_service(columns, key, filepath=filepath,
                                           service=service)
    script_name.append(f'{dir}/{service}_{key}.jmx')
    reports.append(f'JMETER_{service}_{key}')
add_data_to_json(script_name, reports)
