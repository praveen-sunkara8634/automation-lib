from decurtis.common import *
from os import path


class CommonRequest:
    def get_iam_token(self, environment_url):
        data = read_creds_file()
        token = f"Basic {data.auth.oci}"  # TODO: @vishesh this has to be fixed
        url = urljoin(environment_url, "identityaccessmanagement-service/oauth/token?grant_type=client_credentials")
        header = {"authorization": token}
        _content = requests.post(url, data=None, headers=header).json()
        self.iam_token = f"{_content['token_type']} {_content['access_token']}"
        return self.iam_token

    def get_teammember_details(self, environment_url):
        bearer_token = self.iam_token
        url = urljoin(environment_url, "dxpcore/teammembers")
        header = {"authorization": bearer_token}
        _content = requests.get(url, headers=header).json()
        csv_data = []
        for codes in _content['_embedded']['teammembers']:
            csv_data.append(codes)
        return csv_data

    def get_id(self, environment_url, end_point):
        bearer_token = self.get_iam_token(environment_url)
        url = urljoin(environment_url, f"dxpcore/{end_point}/")
        header = {"authorization": bearer_token}
        _content = requests.get(url, headers=header).json()
        id_key = ""
        if '_embedded' in _content.keys():
            context_path = _content['_embedded']
        else:
            context_path = _content
        if len(context_path[end_point]) == 0:
            value_id = ""
            body = ""
        else:
            for key in context_path[end_point][0].keys():
                if f"{end_point}id" in key.lower():
                    id_key = key
                    break
                elif f"{end_point[:-1]}id" in key.lower():
                    id_key = key
            if id_key == "":
                id_key = 'Id'
            value_id = context_path[end_point][0][id_key]
            body = context_path[end_point][0]
        return value_id, body

    def get_code_types(self, environment_url):
        bearer_token = self.iam_token
        url = urljoin(environment_url, "dxpcore/codetypes")
        header = {"authorization": bearer_token}
        _content = requests.get(url, headers=header).json()
        csv_data = []
        for codes in _content['_embedded']['codetypes']:
            csv_data.append(codes)
        return csv_data

    def get_code_values(self, environment_url):
        bearer_token = self.iam_token
        url = urljoin(environment_url, "dxpcore/codevalues")
        header = {"authorization": bearer_token}
        _content = requests.get(url, headers=header).json()
        csv_data = []
        for codes in _content['_embedded']['codevalues']:
            csv_data.append(codes)
        return csv_data

    def get_code_duties(self, environment_url):
        bearer_token = self.iam_token
        url = urljoin(environment_url, "dxpcore/duties")
        header = {"authorization": bearer_token}
        _content = requests.get(url, headers=header).json()
        csv_data = []
        for codes in _content['_embedded']['duties']:
            csv_data.append(codes)
        return csv_data

    def get_code_attributes(self, environment_url):
        bearer_token = self.iam_token
        url = urljoin(environment_url, "dxpcore/attributes")
        header = {"authorization": bearer_token}
        _content = requests.get(url, headers=header).json()
        csv_data = []
        for codes in _content['_embedded']['attributes']:
            csv_data.append(codes)
        return csv_data

    def create_jmx_files(self, filepath, service, key, url, properties, replacement_path, context_path):
        # Reading JMX File
        with open(f'{filepath}{service}.jmx', 'r+') as _fp:
            contents = _fp.read()
            soup = BeautifulSoup(contents, 'xml')
            titles = soup.find_all('stringProp')
            csv_name = f'{service}_test_{key}'
            iam_token = self.get_iam_token(url)
            # XML PARSING
            for title in titles:
                if 'bearer' in title.get_text():
                    title.string = iam_token
                if '.com' in title.get_text():
                    title.string = url.strip("https://")
                if '.csv' in title.get_text():
                    title.string = f'{service}_{key}.csv'
                if title.attrs['name'] == 'ThreadGroup.num_threads':
                    title.string = properties['thread']
                if title.attrs['name'] == 'ThreadGroup.ramp_time':
                    title.string = properties['ramp_time']
                if title.get_text() == context_path:
                    title.string = replacement_path

        with open(f'{filepath}{service}_{key}.jmx', 'w') as _fd:
            _fd.write(str(soup))

    def csv_for_service(self, columns, key, filepath, service):
        with open(f'{filepath}{service}_{key}.csv', 'w+', newline='') as file:
            csv_writer(file, columns)


def csv_writer(file, content):
    writer = csv.writer(file)
    writer.writerows(content)


def add_data_to_json(script_name, reports):
    if path.exists("standalone-scripts/jmeter-scripts/jmx_Scripts.json"):
        with open(f'standalone-scripts/jmeter-scripts/jmx_Scripts.json', 'r+') as _jmx_json:
            data = json.load(_jmx_json)
            for scripts in script_name:
                data['scripts'].append(scripts)
            for report in reports:
                data['reports'].append(report)
            with open(f'standalone-scripts/jmeter-scripts/jmx_Scripts.json', 'r+') as _json:
                json.dump(data, _json)
    else:
        with open(f'standalone-scripts/jmeter-scripts/jmx_Scripts.json', 'w+') as _jmx_json:
            data = dict()
            data.update({
                "scripts": script_name,
                "reports": reports
            })
            json.dump(data, _jmx_json)
