from common import *
import re

common_requests = CommonRequest()
for root, dirs, files in os.walk("standalone-scripts/jmeter-scripts/"):
    for dir in dirs:
        if "ship" in dir:
            filepath = f'standalone-scripts/jmeter-scripts/{dir}/'
            dir = dir
            break
service = 'ship'
script_name = []
reports = []
list = os.sys.argv
environment_url = dict()
properties = dict()
for argument in list:
    if 'ship_url' in argument:
        client = re.search('--ship_url=(.+)', argument).group(1)
        environment_url.update({
            "ship": client
        })
    if 'shore_url' in argument:
        client = re.search('--shore_url=(.+)', argument).group(1)
        environment_url.update({
            "shore": client
        })
    if 'thread' in argument:
        thread = re.search('--thread=(.+)', argument).group(1)
        properties.update({
            "thread": thread
        })
    if 'ramp_time' in argument:
        ramp_time = re.search('--ramp_time=(.+)', argument).group(1)
        properties.update({
            "ramp_time": ramp_time
        })

for key, url in environment_url.items():
    with open(f'{filepath}{service}.jmx', 'r+') as _fp:
        csv_name = f'{service}_test_{key}'
        iam_token = common_requests.get_iam_token(url)
        contents = _fp.read()
        soup = BeautifulSoup(contents, 'xml')
        titles = soup.find_all('stringProp')
        list_id = []
        id_dict = {}
        for title in titles:
            if '${contextPath}' in title.text:
                if re.search('(\${contextPath}/(.+\/(.+)))', title.parent.text) is None and 'ERROR' not in \
                        title.parent.text and 'GET' in title.parent.text:
                    end_point = title.text.strip('${contextPath}')
                    end_point = end_point.strip('/')
                    value_id = common_requests.get_id(url, end_point)
                    id_dict.update({
                        end_point: value_id[0],
                        f'{end_point}_body': value_id[1]
                    })
                    list_id.append((id_dict))
                if re.search('(\${contextPath}/(.+\/(.+)))', title.parent.text) is not None and 'ERROR' not in \
                        title.parent.text and 'POST' not in title.parent.text:
                    end_point = title.text.strip('${contextPath}')
                    _result = re.search('/(.+?)/', end_point).group(1)
                    if _result != 'loggingmode':
                        contextpath_string = '${contextPath}'
                        title.string = f'{contextpath_string}/{_result}/{id_dict[_result]}'
                if 'PUT' in title.parent.text and 'ERROR' not in title.parent.text:
                    end_point = title.text.strip('${contextPath}')
                    _result = re.search('/(.+?)/', end_point).group(1)
                    _result = f'{_result}_body'
                    if _result != 'loggingmode_body':
                        elementProp = title.parent.find('elementProp')
                        elementProp.find_all('stringProp')[0].string = json.dumps(id_dict[_result])
            if 'bearer' in title.get_text():
                title.string = iam_token
            if '.com' in title.get_text() and 'http' not in title.get_text():
                title.string = url.strip("https://")
            if '.csv' in title.get_text():
                title.string = f'{service}_{key}.csv'
            if title.attrs['name'] == 'ThreadGroup.num_threads':
                title.string = properties['thread']
            if title.attrs['name'] == 'ThreadGroup.ramp_time':
                title.string = properties['ramp_time']
        with open(f'{filepath}{service}_{key}.jmx', 'w') as _fd:
            _fd.write(str(soup))
    # CSV creation and insertion
    script_name.append(f'{dir}/{service}_{key}.jmx')
    reports.append(f'JMETER_{service}_{key}')
add_data_to_json(script_name, reports)
