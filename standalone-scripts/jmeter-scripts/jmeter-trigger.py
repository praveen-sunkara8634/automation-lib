from common import *

list = os.sys.argv
list_services = []
data = dict()
scripts = []
with open('standalone-scripts/jmeter-scripts/python_Scripts.json', 'w+') as python_Scripts:
    for load_service in list:
        if '.py' not in load_service:
            scripts.append(f'{load_service}.py')
            data.update({
                "scripts": scripts
            })
    json.dump(data, python_Scripts)
