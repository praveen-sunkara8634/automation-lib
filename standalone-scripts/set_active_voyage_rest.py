import psycopg2
import requests
import uuid
import json
import re
import urllib.parse as urlparse
from datetime import datetime
from datetime import timedelta
from pytz import timezone
from dateutil.relativedelta import relativedelta
import time
from decurtis.common import process_response


class SetActiveVoyage:
    def __init__(self, ship, shore):
        """
        Init the Environment where this has to be used
        :param environment:
        """
        self.ship = ship
        self.token_link = urljoin(shore, 'reservation-bff/auth')
        self.token = "NDllMTQ2NzEtMzJkMy00MWU4LWI1NzktZDc0N2I1NGZmZDIzOnNlY3JldA=="

        # Get Token
        self._get_token()
        self._set_active_voyage()

    def _get_token(self):
        headers = {
            "Authorization": "Basic {}".format(self.token),
            "correlationId": str(uuid.uuid4())
        }
        response = requests.get(self.token_link, headers=headers)
        self.token = response.json()["accessToken"]

    def _set_active_voyage(self):
        startDate = str(datetime.utcnow().date())
        end_date = str(datetime.utcnow().date() + timedelta(days=5))
        _url = "dxpcore/voyages/search/findbyembarkdate?startdate={}&enddate={}".format(startDate, end_date)
        url = urljoin(self.ship, _url)
        headers = {
            "Authorization": "bearer {}".format(self.token),
            "correlationId": str(uuid.uuid4())
        }

        response = process_response(requests.get(url, headers=headers))
        headers['Content-Type'] = "application/json"
        to_remove = ['createdByUser', 'creationTime', 'modificationTime', 'modifiedByUser']
        to_set_true = ['isActive', 'isVoyageRollover']
        for window in [datetime.utcnow().date() + timedelta(days=x) for x in range(0, 4)]:
            for voyage in response.content['_embedded']['voyages']:
                embarkDate = datetime.strptime(voyage['embarkDate'], "%Y-%m-%dT%H:%M:%S").date()
                if embarkDate == window and not voyage['isActive']:
                    # Remove Fields
                    for _to_remove in to_remove:
                        if _to_remove in voyage:
                            del voyage[_to_remove]

                    # Set to True
                    for _to_set_true in to_set_true:
                        if _to_set_true in voyage:
                            voyage[_to_set_true] = True

                    # Remove Fields from voyageItineraries
                    for _count in range(0, len(voyage['voyageItineraries'])):
                        for _to_remove in to_remove:
                            if _to_remove in voyage['voyageItineraries'][_count]:
                                del voyage['voyageItineraries'][_count][_to_remove]
                    url = urljoin(self.ship, 'dxpcore/voyages')
                    process_response(requests.post(url, data=json.dumps(voyage), headers=headers))
                    return 0


if __name__ == "__main__":
    SetActiveVoyage(
        ship="https://dev-kraken.dxp-decurtis.com",
        shore="https://dev-shore.dxp-decurtis.com",
    )
