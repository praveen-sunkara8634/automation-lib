from decurtis.common import retry_when_fails


class FailMe:
    def __init__(self):
        """
        Init Function
        """

    @retry_when_fails((10, 10))
    def fail_me(self):
        raise Exception("I have failed !!")


FailMe().fail_me()
