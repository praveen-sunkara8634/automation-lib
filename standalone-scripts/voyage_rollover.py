import psycopg2
import requests
import uuid
import json
import re
from datetime import datetime
from pytz import timezone
from dateutil.relativedelta import relativedelta
from decurtis.common import urljoin
import time


class VoyageRollOver:
    def __init__(self, environment, ship):
        """
        Init the Environment where this has to be used
        :param environment:
        """
        roll_over_creds = {
            "dev": {
                "dbServer": "postgresql.dev-{}.dxp-decurtis.com",
                "dbUser": "pgreadonlyuser",
                "dbPassword": "QHY!uxX8pwM4sgkG29Jz",
                "token": "NDllMTQ2NzEtMzJkMy00MWU4LWI1NzktZDc0N2I1NGZmZDIzOnNlY3JldA=="
            },
            "qa1": {
                "dbServer": "postgresql.qa1-{}.dxp-decurtis.com",
                "dbUser": "pgreadonlyuser",
                "dbPassword": "TdhVyLmjMGgp2IK7MbHRTm7Z",
                "token": "NDllMTQ2NzEtMzJkMy00MWU4LWI1NzktZDc0N2I1NGZmZDIzOnNlY3JldA=="
            }
        }

        if environment == 'dc':
            raise Exception("Sorry !! Cannot Run this on DC !!")

        if environment in ['qa1', 'dev']:
            _environment = environment
        else:
            _environment = 'dev'

        qa_dev_token = 'dxp-decurtis'
        pr_token = 'pullrequest.dxp-decurtis'
        dev_token = 'developer.dxp-decurtis'
        self.ship = ship
        self.ship_code = {
            'sagar': "DX",
            'kraken': "KR"
        }[self.ship]

        self.db_server = roll_over_creds[_environment]['dbServer'].format(self.ship)
        self.db_user = roll_over_creds[_environment]['dbUser']
        self.db_pass = roll_over_creds[_environment]['dbPassword']
        self.token = roll_over_creds[_environment]['token']

        # Generate Token Link
        self.ship_time = "https://{}-{}.{}.com/dxpcore/shiptimes/7ab60bb8-db1f-48fa-8033-be8fd52798ad"
        self.ship_service = "https://{}-{}.{}.com/dxpcore/voyages/"
        self.token_link = "https://{}-shore.{}.com/reservation-bff/auth"
        self.couch_time = "http://{}-{}.{}.com/syncgatewayfeedersadmin/prodappdocstore/ShipTime::{}"

        # For QA1 and DEV Environments
        if environment in ['qa1', 'dev']:
            self.ship_time = self.ship_time.format(environment, self.ship, qa_dev_token)
            self.ship_service = self.ship_service.format(environment, self.ship, qa_dev_token)
            self.token_link = self.token_link.format(environment, qa_dev_token)
            self.couch_time = self.couch_time.format(environment, self.ship, qa_dev_token, self.ship_code)

        # For PR Environments
        elif re.search(r'^pr(\d*)', environment, re.I | re.M):
            self.ship_time = self.ship_time.format(environment, self.ship, pr_token)
            self.ship_service = self.ship_service.format(environment, self.ship, pr_token)
            self.token_link = self.token_link.format(environment, pr_token)
            self.couch_time = self.couch_time.format(environment, self.ship, pr_token, self.ship_code)

        # For Developer Environments
        else:
            self.ship_time = self.ship_time.format(environment, self.ship, dev_token)
            self.ship_service = self.ship_service.format(environment, self.ship, dev_token)
            self.token_link = self.token_link.format(environment, dev_token)
            self.couch_time = self.couch_time.format(environment, self.ship, dev_token, self.ship_code)

        # Get Token
        self._get_token()

        # Make db Connection
        self.connection = self._db_connection()

        # Get embark and debark dates
        self.embark_date = None
        self.debark_date = None
        self.get_embark_debark_date()

        # Calculate Offset
        self.offset = self.get_offset()

        # Get Next Voyage ID
        self.next_voyage_id = self._get_next_voyage_id()

        # Run Voyage Roll Over
        self.execute_voyage_rollover()

        # Verify Voyage Rollover
        self.verify_voyage_roll_over()

        # Update Ship Time
        self.update_ship_time()

        assert self.verify_ship_time(), "ERROR: Ship time not updated !!"

    def _get_token(self):
        headers = {
            "Authorization": "Basic {}".format(self.token),
            "correlationId": str(uuid.uuid4())
        }
        response = requests.get(self.token_link, headers=headers)
        self.token = response.json()["accessToken"]

    def _db_connection(self):
        """
        Connect to DB and send connection object back to user
        :param db_server:
        :param db_user:
        :param db_pass:
        :return:
        """
        connection = psycopg2.connect(
            user=self.db_user,
            password=self.db_pass,
            host=self.db_server,
            port="5432",
            database="ship"
        )
        return connection

    def _db_run_query(self, query, all=False):
        """
        Run a query on Database Connection
        :param query:
        :return:
        """
        cursor = self.connection.cursor()
        cursor.execute(query)
        if all:
            data = cursor.fetchall()
        else:
            data = cursor.fetchone()
        return data

    def _get_next_voyage_id(self):
        """
        Get Next Voyage ID from Database
        :return:
        """
        query = "select voyageid from voyage where embarkdate = (select debarkdate from voyage where isactive=true)"
        data = self._db_run_query(query, all=True)
        return data[0]

    def _get_next_voyage_details(self):
        url = urljoin(self.ship_service, self.next_voyage_id)
        headers = {
            "Authorization": "bearer {}".format(self.token),
            "correlationId": str(uuid.uuid4())
        }
        response = requests.get(url, headers=headers)
        return response.json()

    def _prepare_payload(self):
        modified_voyage_details = dict()
        voyage_details = self._get_next_voyage_details()
        for _data in voyage_details:
            if _data not in ["createdByUser", "creationTime", "modificationTime", "_links"]:
                modified_voyage_details[_data] = voyage_details[_data]

        for _count, _data in enumerate(voyage_details['voyageItineraries']):
            for _to_remove in ["createdByUser", "creationTime", "modificationTime"]:
                del modified_voyage_details['voyageItineraries'][_count][_to_remove]

        modified_voyage_details['isActive'] = True
        modified_voyage_details['isVoyageRollover'] = True

        return modified_voyage_details

    def execute_voyage_rollover(self):
        """
        Execute Voyage Roll Over
        :return:
        """
        postHeader = {
            "Authorization": "bearer {}".format(self.token),
            "Content-Type": "application/json"
        }
        data = self._prepare_payload()
        print("Executing Voyage Rollover ...")
        response = requests.post(self.ship_service, data=json.dumps(data), headers=postHeader)
        return response

    def verify_voyage_roll_over(self):
        """
        Verify Voyage Roll Over
        :return:
        """
        while True:
            url = urljoin(self.ship_service, self.next_voyage_id)
            header = {
                "Authorization": "bearer {}".format(self.token),
                "correlationId": str(uuid.uuid4())
            }
            response = requests.get(url, headers=header)
            if response.json()['isActive']:
                print("Voyage Rollover Successful ...")
                break
            else:
                print("Waiting for Voyage Rollover ...")
                time.sleep(10)

    def get_embark_debark_date(self):
        """
        Get Embark Debark Date
        :return:
        """
        query = "select embarkdate,debarkdate from voyage where isactive=true"
        data = self._db_run_query(query)
        self.embark_date = data[0]
        self.debark_date = data[1]

    def get_offset(self):
        """
        Get Time Offset from Current Time
        :return:
        """
        utcNowTZ = timezone('UTC').localize(datetime.strptime(str(self.embark_date), "%Y-%m-%d %H:%M:%S"))
        usDate = utcNowTZ.astimezone(timezone('US/Eastern')).replace(tzinfo=None)
        utcNow = datetime.utcnow().date()
        offset = relativedelta(usDate, utcNow)
        return (offset.days * 24 * 60) + (offset.hours * 60) + offset.minutes

    def update_ship_time(self):
        """
        Update Ship Time
        :return:
        """
        data = {
            "shipTimeId": str(uuid.uuid4()),
            "shipId": str(uuid.uuid4()),
            "fromUtcDate": "{}T{}.000+0000".format(str(datetime.utcnow().date()), str(datetime.utcnow().time())[0:8]),
            "fromDateOffset": str(self.offset),
            "isDeleted": False,
            "shipCode": self.ship
        }

        header = {
            "Authorization": "bearer {}".format(self.token),
            "Content-Type": "application/json"
        }
        print("Updating Ship Time ...")
        response = requests.put(self.ship_time, data=json.dumps(data), headers=header)
        return response

    def verify_offset_in_core(self):
        query = "select fromdateoffset from shiptime"
        data = self._db_run_query(query)
        return data[0] == self.offset

    def verify_offset_in_couch(self):
        response = requests.get(self.couch_time)
        return response.json()["FromDateOffset"] == self.offset

    def verify_ship_time(self):
        while True:
            if self.verify_offset_in_core() and self.verify_offset_in_couch():
                return True
            else:
                print("Waiting for Ship Time to update ...")
                time.sleep(5)


if __name__ == "__main__":
    voyageRollOver('qa1', 'sagar')
