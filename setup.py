import setuptools
import json
import os

if not os.path.isfile('version.json'):
    data = {
        "version": "0.0.1"
    }
else:
    with open('version.json', 'r') as _fp:
        data = json.load(_fp)

version = data['version']

with open("README.md", "r") as fh:
    long_description = fh.read()

with open("requirements.txt", "r") as fh:
    requirements = fh.read().split('\n')

setuptools.setup(
    name="decurtis",
    version=version,
    author="Anshuman Goyal",
    author_email="anshuman.goyal@decurtis.com",
    description="Python Library to be used by all Decurtis Automation Projects",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/anshumangoyal/python-lib",
    packages=setuptools.find_packages(),
    install_requires=requirements,
    include_package_data=True,
    license='MIT',
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
