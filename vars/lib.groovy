#!/bin/groovy

import groovy.json.*
import groovy.io.*

def getUser() {
    userEmail = currentBuild.rawBuild.getCause(Cause.UserIdCause).getUserId()
    return userEmail
}

def sendEmail(notifyEmails, testBed, gitBranch, tcName, testPlan, failReason, correlationId, curlCommand) {
    /*
    slackChannel: Channel to which notification to be sent
    testBed: Test Bed Name
    gitBranch: Git Branch
    tcName: Test Case name
    testPlan: Test Plan Link
    reason: Reason for Failure
    correlationId: Correlation ID
    curlCmd: Curl Command
    */
    emailext attachmentsPattern: '',
    mimeType: 'text/html',
    body: """<p>Hello,<br><br><b>E2E Test-Case ${tcName} on ${testBed.toUpperCase()} Has Failed !!</b></p>
    <p>Git Branch: ${gitBranch}</p>
    <p><a href='${env.BUILD_URL}allure/#categories'>Allure-Report</a></p>
    <p><a href='${testPlan}'>Test-Plan</a></p>
    <p>Fail-Reason <pre>${failReason}</pre></p>
    <p>Correlation-Id <pre>${correlationId}</pre></p>
    <p>Curl-Command <pre>${curlCommand}</pre></p>
    <p>Thanks,<br>Team QA</p>""",
    subject: "E2E On ${testBed.toUpperCase()} Failed !!",to: notifyEmails.unique().sort().join(',')
}

// Send Slack notification (message) to given channel
def sendE2ESlackNotification(channel, toTag, gitHash, tcName, failReason) {
    /*
    slackChannel: Channel to which notification to be sent
    testBed: Test Bed Name
    gitBranch: Git Branch
    tcName: Test Case name
    testPlan: Test Plan Link
    failReason: Reason for Failure
    correlationId: Correlation ID
    curlCommand: Curl Command
    */
    colorCode = '#FF0000'
    Date now = new Date();
    time_now = ((now.getTime())/1000).toInteger()
//    message = """${slackToTag} E2E Job# <${env.BUILD_URL}allure/#categories|${env.BUILD_DISPLAY_NAME}> on *${testBed.toUpperCase()}* Failed !!
//Triggered @ <!date^${time_now}^ {date_short} {time_secs}|MY-Date>
//*Branch*: `${gitBranch}`
//*Test-Plan*: ${testPlan}
//*Correlation-ID*: `${correlationId}`
    message = """${toTag} Job# <${env.BUILD_URL}allure/#categories|${env.BUILD_DISPLAY_NAME}> ${gitHash} Failed !!
`${tcName.split("::")[-2, -1].join(" :: ")}`
```${failReason}```
"""
    slackSend(
        channel: channel,
        color: colorCode,
        tokenCredentialId: 'slack-token',
        message: message,
        sendAsText: true
    )
}

/*
Trigger a remote build and as per parameters, wait for it to complete or not
*/
def triggerRemoteBuild(buildName, remoteServer, parameters=null, enhancedLogs = false, block = true, creds = 'ciduser-decurtis') {
    if (parameters) {
        params = []
        for (_key in parameters.keySet()) {
            params.add("${_key}=${parameters[_key]}")
        }
        params = params.join('\n')
    } else {
        params = null
    }
    def handle = triggerRemoteJob(
        remoteJenkinsName: remoteServer,
        job: buildName,
        blockBuildUntilComplete: block.toBoolean(),
        parameters: params,
        enhancedLogging: enhancedLogs.toBoolean(),
        auth: CredentialsAuth(credentials: creds)
    )
    def status = handle.getBuildStatus().toString()
    def buildUrl = handle.getBuildUrl().toString()
    echo buildUrl + " finished with " + status

    // Read Test Data From Remote Build
    testPlan = null
    if (params) {
        if (params.contains("testRails=true")) {
            testData = handle.readJsonFileFromBuildArchive('test_data.json')
            if (testData.containsKey('test_plan')) {
                testPlan = testData['test_plan']
            }
        }
    }

    if (!(status in ['SUCCESS', 'FINISHED', 'RUNNING'])) {
        return [testPlan, false, buildUrl]
    } else {
        return [testPlan, true, buildUrl]
    }
}

/*
Get Test Bed Name, based on Shore Link both for DXP and VV Environments
*/
def getTestBed(shoreLink) {
    def vvMatcher = shoreLink =~ /https:\/\/(.*?)\.virginvoyages/
    def dxpMatcher = shoreLink =~ /https:\/\/(.*?)-shore/
    def dclMatcher = shoreLink =~ /https:\/\/(.*?)\.dxpcore/

    if (vvMatcher.find()) {
        return "vv-${vvMatcher[0][1].toString()}"
    } else if (dxpMatcher.find()) {
        return "dxp-${dxpMatcher[0][1].toString()}"
    } else if (dclMatcher.find()) {
        return "dcl-${dclMatcher[0][1].toString()}"
    } else {
        // TODO: Handle DCL, NCL and other environments here
        log.error('Invalid Shore Link ${shoreLink}')
        error("ERROR Invalid Shore Link ${shoreLink}")
    }
}

/*
Send an email to a user with subject and a message
*/
def sendMail(recipients, subject, message) {
    /*
    recipients: Send to whom, expecting a list here
    subject: Subject Line
    message: Text that has to be sent, expecting it is HTML Formatted.
    */
    emailext attachmentsPattern: '',
    mimeType: 'text/html',
    body: """<p><a href='${env.BUILD_URL}consoleFull'>Job # ${env.BUILD_NUMBER}</a></p><p>${message}</p>""",
    subject: "${env.JOB_BASE_NAME} #${env.BUILD_NUMBER} - ${subject}",
    to: recipients.unique().sort().join(',')
}

def sendGetRequest(url) {
    /*
    Send a get request to a remote server
    */
    def response = null
    try {
        response = httpRequest ignoreSslErrors: true, quiet: true, timeout: 5, url: url
        return response
    } catch (exp) {
        println("ERROR: ${url} | Get-Response: ${response.content} ${exp} !!")
        return response.content
    }
}

def getVersionFromResponse(response) {
    /*
    Get Component Version from Info Call Response
    */
    def toReturn = null
    try {
        def data = readJSON text: response.content
        if ('version' in data.keySet()) {
            toReturn = data['version']
        } else if ('Version' in data.keySet()) {
            toReturn = data['Version']
        } else {
            toReturn = data
        }
    } catch (net.sf.json.JSONException exp) {
        toReturn = response.content
    }
    toReturn = toReturn.replaceAll('’', '')
    toReturn = toReturn.replaceAll('‘', '')
    toReturn = toReturn.replaceAll('"', '')
    toReturn = toReturn.tokenize(' ')[0]
    return toReturn
}

def getTaggedUsers(users) {
    /*
    Get user-id of a user who has to be tagged using slack's in built library
    */
    def toTag = users.replaceAll('\\s', '').split(',')
    data = readJSON file: "test_data/slack_data.json"
    final_data = []
    for (_toTag in toTag) {
        for (_user in data['users']) {
            if (_user['email'] == _toTag){
                final_data.add("<@${_user['id']}>")
            }
        }
        for (_group in data['groups']) {
            if (_group['handle'] == _toTag){
                final_data.add("<!subteam^${_group['id']}>")
            }
        }
        for (_channel in data['channels']) {
            if (_channel['name'] == _toTag){
                final_data.add("<#${_channel['id']}")
            }
        }
    }
    return final_data.unique().join(" ")
}

def sendSlackMessage(users, message) {
    /*
    Send Slack message to any user from Jenkins, this uses Python Library to perform the operation
    */
    sh "python3 -u standalone-scripts/send-slack-message.py --users=\"${users}\" --message=\"${message}\""
}

def getBuildUser() {
    /*
    Get User who has started this build, else it returns vertical-qa when it is auto triggered
    */
    if (currentBuild.rawBuild.getCauses()[0].toString().contains('UserIdCause')){
        node {
            wrap([$class: 'BuildUser']) {
                return ["user": "${BUILD_USER}", "id": "${BUILD_USER_ID}", "email": "${BUILD_USER_EMAIL}"]
            }
        }
    } else {
        return ["user": "vertical-qa", "id": "vertical-qa", "email": "vertical-qa@decurtis.com"]
    }
}

def getBitbucketToken() {
    /*
    Get Bit Bucket Token to read something from Bit Bucket
    */
    sh 'curl -X POST -u "bP8nwZamdpTJdpb5y6:QzZpAYpxyD5cPtBqKPczBsdfFXeH6TcW" https://bitbucket.org/site/oauth2/access_token -d grant_type=client_credentials > bitbucket_token.json'
    tokenJson = readJSON file: 'bitbucket_token.json'
    token = tokenJson.access_token
    return token
}

def getJenkinsCreds() {
    /*
    Get Credential ID's of all saved credentials in Jenkins
    */
    creds_list = []
    node('master') {
        def creds = com.cloudbees.plugins.credentials.CredentialsProvider.lookupCredentials(
            com.cloudbees.plugins.credentials.common.StandardUsernameCredentials.class,
            Jenkins.instance,
            null,
            null
        );
        for (c in creds) {
            creds_list.add(c.id)
        }
    }
    return creds_list
}

def compareTwoMaps(oldVersions, newVersions) {
    /*
    Compare two maps and return the difference
    */
    diff = [:]
    for (_key in oldVersions.keySet()) {
        if (! (_key in newVersions.keySet())) {
            diff[_key] = oldVersions[_key]
        }
    }
    for (_key in newVersions.keySet()) {
        if (! (_key in oldVersions.keySet())) {
            diff[_key] = newVersions[_key]
        }
    }

    for (_key in oldVersions.keySet()) {
        if (_key in newVersions.keySet()) {
            if (oldVersions[_key] != newVersions[_key]) {
                diff[_key] = [oldVersions[_key], newVersions[_key]]
            }
        }
    }

    if (diff.size() == 0) {
        return null
    } else {
        return JsonOutput.prettyPrint(JsonOutput.toJson(diff))
    }
}

def readJsonFromOtherJob(fileName, jobName) {
    /*
    This function can read a file from artifacts of any job and return back it's Json
    Make sure you read Json from this as that makes more use of this function
    */
    def data = [:]
    node('master') {
        deleteDir()
        try {
            copyArtifacts(projectName: jobName)
        } catch (exc) {
            log.info('There is no artifact !!')
            return data
        }

        if (fileExists(fileName)) {
            data = readJSON file: fileName, pretty: 2
        }
    }
    return data
}

def getEmbarkDates(file, job) {
    /*
    This function will read Json from 'dxp-embark-dates' Job and return back embark dates
    */
    def embarkDates = readJsonFromOtherJob(file, job)
    embarkDates = embarkDates.unique().join('\n')
    return embarkDates.split("\n")
}

def getGitBranch() {
    /*
    Get Git Branch from which this code has been triggered
    */
    def gitBranch = 'master'
    try {
        gitBranch = scm.branches[0].name.toString().replace("*/","")
    } catch (exc) {
        print "Pipeline doesn't know SCM, setting git-branch to default to master"
    }
    return gitBranch
}

def getUserToDisplay(toNotify = null) {
    /*
    This function can take String or Array as input and process the data to give back a String (commas separated) of all
    users who triggered this job and/or who were involved to be notified
    */
    def result = []

    if (toNotify == null) {
        toNotify = []
    } else if (toNotify.class == String) {
        toNotify = [toNotify]
    }

    try {
        def whoTriggered = currentBuild.rawBuild.getCause(Cause.UserIdCause).getUserId()
        toNotify.add(whoTriggered)
    } catch (exc) {
        print("Unable to get who triggered this Job !!")
    }

    toNotify = toNotify.unique().sort()

    if (toNotify.size() == 0) {
        result.add('ciuser'.toUpperCase())
    } else if (toNotify.size() == 1) {
        def _name = toNotify[0].split("@")[0]
        result.add(_name.toUpperCase())
    } else {
        for (_data in toNotify) {
            def _name = _data.split("@")[0]
            if (_name == 'ciuser') {
                continue
            } else {
                result.add(_name.toUpperCase())
            }
        }
    }

    return result.join(", ")
}

def getConsoleLogs(name = null, number = null) {
    /*
    This function will send back the console logs for name and number
    env.JOB_NAME Holds Job Name
    env.BUILD_NUMBER Holds Job NUmber
    */
    if (! name) {
        name = env.JOB_NAME
    }
    if (! number) {
        number = env.BUILD_NUMBER.toInteger()
    } else {
        number = number.toInteger()
    }
    def logContent = Jenkins.getInstance().getItemByFullName(name).getBuildByNumber(number).logFile.text
    return logContent.toString()
}

def getLastTestCase(name = null, number = null) {
    /*
    This function will send back the latest test case that has been executed by a job
    */
    if (! name) {
        name = env.JOB_NAME
    }
    if (! number) {
        number = env.BUILD_NUMBER.toInteger()
    } else {
        number = number.toInteger()
    }
    def logContent = getConsoleLogs(name, number)
    def matcher = logContent =~ /test_scripts\/test_(\d+.*?)\s/
    if (matcher.find() && matcher.size() > 1) {
        return matcher[-1][0].toString()
    }
}

def getChannelName(shipLink) {
    /*
    Get Channel Name based on test Test Bed
    */
    def channel = null

    // For DXP
    if (shipLink =~ /https:\/\/(master\d+)-sagar/) {
        channel = 'e2e-ci-failures'
    } else if (shipLink =~ /https:\/\/(pr\d+)-sagar/) {
        channel = 'e2e-pr-failures'
    } else if (shipLink =~ /https:\/\/(qa1)-sagar/) {
        channel = 'e2e-qa1-failures'
    } else if (shipLink =~ /https:\/\/(dc)-sagar/) {
        channel = 'sre-alerts'
    } else {
        channel = 'e2e-dev-failures'  // Developer
    }

    // For VV's
    if ('virginvoyages' in shipLink) {
        channel = 'e2e-vv-failures'
    }

    // Branch for E2E Testing
    if (env.JOB_BASE_NAME.toLowerCase().contains('branch')) {
        channel = 'e2e-branch-failures'
    }

    // if no channel defined, send it to automation-alerts
    if (channel == null) {
        channel = 'automation-alerts'
    }

    return channel
}

def get_slack_user_id_from_email(emailId) {
    /*
    Get Slack User ID from given email address
    */
    withCredentials([string(credentialsId: 'slack-api-token', variable: 'SLACK_TOKEN')]) {
        def url = "https://slack.com/api/users.lookupByEmail?email=${emailId}"
        def response = httpRequest ignoreSslErrors: true,
            httpMode: 'GET',
            contentType: 'APPLICATION_JSON',
            quiet: true,
            customHeaders: [[name: 'Authorization', value: "Bearer ${SLACK_TOKEN}"]],
            url: url
        def respContent = readJSON text: response.content
        if ('user' in respContent && 'id' in respContent['user']) {
            def userId = respContent['user']['id']
            return "<@${userId}>"
        } else {
            return null
        }
    }
}

def readJsonData(fileName) {
    /*
    Read Json Data from a file
    */
    if (fileExists(fileName)) {
        data = readJSON file: fileName, pretty: 2
    }
    return data
}

def getFoldersWithPattern(path, pattern) {
    /*
    Searches 'path' recursively for folder with 'pattern'
    Returns list of matching folders
    */
    def folderList = []
    def dir = new File(path)
    dir.eachFile (FileType.DIRECTORIES) { folder ->
        def matcher = folder =~ /(.*)${pattern}(.*)$/
        if (matcher) {
            folderList.add(folder)
        }
    }
    return folderList
}

def getFilesWithPattern(path, pattern) {
    /*
    Searches 'path' recursively for files with 'pattern'
    Returns list of matching files
    */
    def fileList = []
    def dir = new File(path)
    dir.eachFile (FileType.FILES) { file ->
        def matcher = file =~ /(.*)${pattern}(.*)$/
        if (matcher) {
            fileList.add(file)
        }
    }
    return fileList
}

def getLastTestCaseName(logContent) {
    /*
    This function will send back the latest test case that has been executed by a job (available in content)
    */
    def matcher = logContent =~ /test_scripts\/test_(\d+.*?)\s/
    if (matcher.find() && matcher.size() > 1) {
        return matcher[-1][0].toString()
    }
}

@NonCPS
def getJenkinsName() {
    /*
    Get Jenkins Name
    */
    def nameMatcher = ("${env.JENKINS_URL}" =~ /https:\/\/(.*?)\..*$/)[0..-1]
    def name = nameMatcher[0][1].toString().toUpperCase()
    return name
}

return this
