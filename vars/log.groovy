#!/bin/groovy

// Log Debug
def debug(String text) {
    //Magenta Color
    ansiColor('xterm') {
        echo "\u001B[35m${text}\n\u001B[m\n"
    }
}

// Log Info
def info(String text) {
    // Cyan Color
    ansiColor('xterm') {
        echo "\u001B[36m${text}\n\u001B[m\n"
    }
}

// Log Error
def error(String text) {
    // Red Color
    ansiColor('xterm') {
        echo "\u001B[31m${text}\n\u001B[m\n"
    }
}

// Log Success
def success(String text) {
    // Green Color
    ansiColor('xterm') {
        echo "\u001B[32m${text}\n\u001B[m\n"
    }
}

return this